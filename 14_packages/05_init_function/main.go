package main

import "fmt"

var myList = [10]int{}

func init() {
	// INIT FUNNCTION
	// init function does not take any argument
	// executes autmatically like the main
	// init cannot be called manually

	fmt.Println("This is init function")

	// can have multiple init function in a package or file
	// the order of calling will be in the order they appear
	// the purpose of init is to initialize global variables
	//

	for i, v := range myList {
		fmt.Printf("Your index is %d and the value is %d", i, v)
		myList[i] = i * 2

	}
}

func main() {
	fmt.Printf("%#v\n", myList)
	// will print my list array with the numbers
}
