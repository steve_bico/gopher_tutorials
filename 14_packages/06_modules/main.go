package main

import (
	"fmt"

	"github.com/ddadumitrescu/hellomod"
)

func main() {
	fmt.Println("This is package import from github example")
	hellomod.Salut()
	hellomod.SayHello()
}
