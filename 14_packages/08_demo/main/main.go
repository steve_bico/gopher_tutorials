package main

import (
	"course/display"
	msg "course/mgs"
)

func main() {
	msg.Hi()
	display.Display("Hello from display")
	msg.Exiting("An Exciting message")
}
