package main

import (
	"fmt"

	email "bix.com/packages/test2/emails"
	invoice "bix.com/packages/test2/invoice"
)

func main() {
	fmt.Println("This is main func")

	email.Send("This is test", "bix.bix@gmail.com")
	invoice.Create("Jey", 5, 8976.89)

	// to initialize go mod use go mod init <module_name>
}
