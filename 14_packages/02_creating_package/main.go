package main

import (
	"fmt"
	"numbers/numbers"
)

func main() {
	var x uint = 40
	var y int = 50
	fmt.Println("This is number bank package section example")
	fmt.Printf("%d is even : %t \n", x, numbers.Even(x))

	fmt.Printf("%d is prime : %t \n", y, numbers.IsPrime(y))

	// use go run *.go - used to run multiple files in the same go directory
	// imports are filed scoped- visible only to the file they were used
	// constants, varables and functions when declared in a package, they are visible to other files in the same package
	// main function belongs to package
	// it is the entry point of any program
	// does not take any arguments

}
