package rooms

import "fmt"

func GetRoomDetails(roomNumber, size, nights int) {
	fmt.Println("Room number ", roomNumber, ":", size, "people", nights, " nights")
}
