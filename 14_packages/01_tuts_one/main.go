package main

import (
	"fmt"
	"test/rooms"
)

func main() {
	fmt.Println("This is the main file")

	rooms.GetRoomDetails(400, 3, 4)
}

/*
package is a project directory containing .go files wiht the same package statement at the beginning.
A package contains many source files each ending in .go extension and belonging to a single directory.
There are two types of package
1. Executable packages - that generate executable files which can be run.  The name of executable package is predefined and is called main.
2. Non Executable Packages - Libraries/dependancies that are used by other packages and can have any name.  They cannot be executed, only IMPORTED.

*/
