package main

import (
	"encoding/json"
	"fmt"
)

type People struct {
	Firstname string
	Lastname  string
}

func main() {

	// 1. Decoding JSON to struct

	var person People

	jsonString := `{
        "firstName":"Dan",
        "lastName":"Mike",
    }`

	err := json.Unmarshal([]byte(jsonString), &person)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(person)

	// 2. Decoding JSON to array
	var persons []People

	jsonArray := `[
        {
            "Firstname":"Kim",
            "Lastname":"Possible"
        },
        {
            "Firstname":"Bob",
            "Lastname":"Colt"
        },
        {
            "Firstname":"Rose",
            "Lastname":"Kim"
        }
    ]`

	err = json.Unmarshal([]byte(jsonArray), &persons)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(persons)

	for _, person := range persons {
		fmt.Println(person)
	}

}
