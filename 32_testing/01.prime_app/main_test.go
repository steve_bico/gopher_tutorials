package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"strings"
	"testing"
)

func Test_isPrime(t *testing.T) {
	// using test table to avoid repetetion
	primeTests := []struct {
		name       string
		testNumber int
		expected   bool
		message    string
	}{
		{"prime", 7, true, "7 is prime number"},
		{"not prime", 8, false, "8 is not prime number since it's divisible by 2"},
		{"zero", 0, false, "0 is not prime, by definition"},
		{"one", 1, false, "1 is not prime, by definition"},
		{"negative number", -1, false, "negative numbers are not prime, by definition"},
	}

	for _, test := range primeTests {
		result, message := isPrime(test.testNumber)
		if test.expected && !result {
			t.Errorf("%s: expected true but got false", test.name)
		}

		if !test.expected && result {
			t.Errorf("%s: expected false but got true", test.name)
		}

		if test.message != message {
			t.Errorf("%s: expected %s but got %s", test.name, test.message, message)
		}
	}

	// Check test coverage by using go test -cover .
	// go test -coverprofile=coverage.out -> the coverage.out will have the report of the test
	// go tool cover -html=coverage.out will open the browser and show the code covered
	// combine the test coverage with go test -coverprofile=coverage.out && go tool cover -html=coverage.out
	// check test coverage short hand go test cover .
}

// Test Prompt

func Test_prompt(t *testing.T) {
	// Testing things from the ternmial
	// Save a copy of os.Stdout
	oldOut := os.Stdout

	// Create a read and write pipe

	r, w, _ := os.Pipe()

	// set os.Stdout to write pipe
	os.Stdout = w

	// run prompt
	prompt()

	// close the writer
	_ = w.Close()

	// reset os.Stdout to what it was before
	os.Stdout = oldOut

	// read the out put of prompt func from read pipe
	out, _ := io.ReadAll(r)

	if string(out) != "--> " {
		t.Errorf("incorrect prompt: expected --> but got %s", string(out))
	}

}

func Test_intro(t *testing.T) {
	// Testing things from the ternmial
	// Save a copy of os.Stdout
	oldOut := os.Stdout

	// Create a read and write pipe
	r, w, _ := os.Pipe()

	// set os.Stdout to write pipe
	os.Stdout = w

	// run intro
	intro()

	// close the writer
	_ = w.Close()

	// reset os.Stdout to what it was before
	os.Stdout = oldOut

	// read the out put of prompt func from read pipe
	out, _ := io.ReadAll(r)

	if !strings.Contains(string(out), "Enter a whole number") {
		t.Errorf("intro text not correct; got %s", string(out))
	}

}

func Test_checkNumbers(t *testing.T) {

	//input := strings.NewReader("7")
	// use this to simulate user input
	// strings.NewReader() returns *strings.Reader which is r io.Reader
	// prepopulate a simulation of user input with strings.NewReader("")

	// input := strings.NewReader(test.input)
	// reader := bufio.NewScanner(input) // returns *bufio.Scanner which is user input

	// check for resposne with
	// res, _ := checkNumbers(reader)

	// if !strings.EqualFold(res, "7 is prime number") {
	// 	t.Error("incorrect value returned")
	// }

	// Test Table
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{name: "empty", input: "", expected: "Please enter a whole number!"},
		{name: "zero", input: "0", expected: "0 is not prime, by definition"},
		{name: "one", input: "1", expected: "1 is not prime, by definition"},
		{name: "negative", input: "-1", expected: "negative numbers are not prime, by definition"},
		{name: "two", input: "2", expected: "2 is prime number"},
		{name: "three", input: "3", expected: "3 is prime number"},
		{name: "four", input: "4", expected: "4 is not prime number since it's divisible by 2"},
		{name: "quit", input: "q", expected: ""},
		{name: "QUIT", input: "Q", expected: ""},
	}

	// Loop through the tests table to check name, input, expected

	for _, test := range tests {
		input := strings.NewReader(test.input)
		reader := bufio.NewScanner(input)
		res, _ := checkNumbers(reader)
		if !strings.EqualFold(res, test.expected) {
			t.Errorf("%s: expected %s, but got %s", test.name, test.expected, res)
		}

	}

}

func Test_readUserInput(t *testing.T) {
	// this test makes sure goroutine fires, takes some values and done something then return the value then quits
	// to test this function, we need a channel, and instance of an io.Reader
	doneChan := make(chan bool)

	// Create a reference of bytes.Buffer
	var stdin bytes.Buffer
	stdin.Write([]byte("1\nq\n")) // simulates user typing 1 then enter or q then enter
	go readUserInput(&stdin, doneChan)
	<-doneChan

	close(doneChan)

}

// Writing Individual tests

// 1. Running a single test -> go test -run <name to run>
//  go test -run Test_readUserInput can use -v for verbose go test -v -run <test>

// 2. Running Group of tests/test suite
// gives test a unique name eg _alpha_
// go test -v -run Test_alpha. alpha can be anything eg auth
