package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	// Print Welcoming Message
	intro()

	// Create a channel -> concurrently running things in the background
	doneChan := make(chan bool)

	// Start a goroutine running on the backgroudn to read user input and run program
	go readUserInput(os.Stdin, doneChan) // os.Stdin satisfies the io.Reader interface

	// block untill a channel gets a value
	<-doneChan

	// close channel
	close(doneChan)
	// say goodbye
	fmt.Println("Good bye")
}

func intro() {
	fmt.Println("Is it Prime?")
	fmt.Println("_____________")
	fmt.Println("Enter a whole number and we will tell you if it is prime number. Enter 'q' to quit")
	prompt()
}

func prompt() {
	fmt.Print("--> ")
}

func readUserInput(in io.Reader, doneChan chan bool) {
	// scanner
	// scanner := bufio.NewScanner(os.Stdin)
	scanner := bufio.NewScanner(in)

	for {
		res, done := checkNumbers(scanner)

		if done {
			doneChan <- true
			return
		}

		fmt.Println(res)
		prompt()
	}
}

func checkNumbers(scanner *bufio.Scanner) (string, bool) {
	// Read user input
	scanner.Scan()

	// Check to see if user want to qu
	if strings.EqualFold(scanner.Text(), "q") {
		return "", true
	}

	// try to convert what the user typed into an int
	numberToCheck, err := strconv.Atoi(scanner.Text())
	if err != nil {
		return "Please enter a whole number!", false
	}

	_, message := isPrime(numberToCheck)

	return message, false

}

func isPrime(number int) (bool, string) {
	// Zero and 1 are not prime

	if number == 0 || number == 1 {
		return false, fmt.Sprintf("%d is not prime, by definition", number)

	}

	// Negative numbers are not prime
	if number < 0 {
		return false, "negative numbers are not prime, by definition"
	}

	// Use modulos operator repeatedly to see if we have a prime number
	for i := 2; i <= number/2; i++ {
		if number%i == 0 {
			// not a prime number
			return false, fmt.Sprintf("%d is not prime number since it's divisible by %d", number, i)
		}
	}

	return true, fmt.Sprintf("%d is prime number", number)
	// {"prime", 7, true, "7 is a prime number!"},
}
