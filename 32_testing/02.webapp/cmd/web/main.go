package main

import (
	"encoding/gob"
	"flag"
	"log"
	"net/http"
	"webapp/pkg/data"
	"webapp/pkg/repository"
	"webapp/pkg/repository/dbrepo"

	"github.com/alexedwards/scs/v2"
)

type application struct {
	DSN string
	//DB      db.PostgresConn
	DB      repository.DatabaseRepo
	Session *scs.SessionManager
}

func main() {

	// register User type for the session
	gob.Register(data.User{})

	// set up app config for things shared around application
	app := application{}

	// reading the data source string from the terminal
	flag.StringVar(&app.DSN, "dsn", "host=localhost port=5430 user=postgres password=postgres dbname=users sslmode=disable timezone=UTC connect_timeout=5", "Posgres Connection")

	flag.Parse() // reads the value into where it has to be

	conn, err := app.connectToDB()

	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	//app.DB = conn
	//app.DB = db.PostgresConn{DB: conn}

	// Using repository pattern
	app.DB = &dbrepo.PostgresDBRepo{DB: conn}

	// get session manager
	app.Session = getSession()

	// get application roots
	mux := app.routes()

	// print out a message to show the app is runnign

	log.Println("Starting server on port 9000...")

	// start the server
	err = http.ListenAndServe(":9000", mux)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("server running on port 9000 ....")

}
