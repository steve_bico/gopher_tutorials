package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"
	"webapp/pkg/data"
)

type TemplateData struct {
	IPAddress string
	Data      map[string]interface{}
	Error     string
	Flash     string
	User      data.User
}

var templatesPath = "./templates/"
var uploadPath = "./static/img"

func (app *application) Home(w http.ResponseWriter, r *http.Request) {
	// pass data from session to template
	var templateData = make(map[string]interface{})

	// check if something in session
	if app.Session.Exists(r.Context(), "test") {
		msg := app.Session.GetString(r.Context(), "test")
		templateData["test"] = msg
	} else {
		app.Session.Put(r.Context(), "test", "Hit this page at "+time.Now().UTC().String())
	}

	_ = app.render(w, r, "home.page.html", &TemplateData{Data: templateData})
}

func (app *application) Profile(w http.ResponseWriter, r *http.Request) {
	_ = app.render(w, r, "profile.page.html", &TemplateData{})
}

func (app *application) Login(w http.ResponseWriter, r *http.Request) {
	// 1. parse form data
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	// Validate Data
	form := NewForm(r.PostForm)
	form.RequiredFields("email", "password")

	// check if form is valid
	if !form.Valid() {
		// redirect with error message
		app.Session.Put(r.Context(), "error", "Invalid login credentials")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	// get form values
	email := r.Form.Get("email")
	password := r.Form.Get("password")
	_ = password

	user, err := app.DB.GetUserByEmail(email)
	if err != nil {
		// redirect with error message
		app.Session.Put(r.Context(), "error", "Invalid logins")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	_ = user

	// Authenticate user
	// if not authenticated redirect with error
	if !app.authenticate(r, user, password) {
		app.Session.Put(r.Context(), "error", "Invalid login")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	// prevent session fixation attack
	_ = app.Session.RenewToken(r.Context())

	//Redirect to some page
	app.Session.Put(r.Context(), "flash", "successfully logged in")
	http.Redirect(w, r, "/user/profile", http.StatusSeeOther)

}

func (app *application) UploadProfilePicture(w http.ResponseWriter, r *http.Request) {
	// 1. Call a function that extracts a file from an upload.
	files, err := app.UploadFiles(r, uploadPath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// 2. Get the user from the session
	user := app.Session.Get(r.Context(), "user").(data.User)

	// 3. Create a var of type data.UserImage
	var i = data.UserImage{
		UserID:   user.ID,
		FileName: files[0].OriginalFileName,
	}

	// 4. Insert the user image into user_images
	_, err = app.DB.InsertUserImage(i)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// 5. Refresh sessional variable "user" for right user info
	updatedUser, err := app.DB.GetUser(user.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	app.Session.Put(r.Context(), "user", updatedUser)

	// 6. Redirect to profile page
	http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
}

func (app *application) render(w http.ResponseWriter, r *http.Request, t string, td *TemplateData) error {

	//1. parse the template from disk
	// parsedTemplate, err := template.ParseFiles("./templates/" + t)
	parsedTemplate, err := template.ParseFiles(path.Join(templatesPath, t), path.Join(templatesPath, "base.layout.html"))
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)
		return err
	}

	// getting the ip from context and assigning it to data.IPAddress
	td.IPAddress = app.ipFromContext(r.Context())
	td.Error = app.Session.PopString(r.Context(), "error")
	td.Flash = app.Session.PopString(r.Context(), "flash")

	// Populate data being passed to the template when a a file is uploaded
	if app.Session.Exists(r.Context(), "user") {
		td.User = app.Session.Get(r.Context(), "user").(data.User)
	}

	// 2. execute template passing data if any
	err = parsedTemplate.Execute(w, td)
	if err != nil {
		http.Error(w, "internal server error ", http.StatusInternalServerError)
		return err
	}

	return nil
}

func (app *application) authenticate(r *http.Request, user *data.User, password string) bool {
	if valid, err := user.PasswordMatches(password); err != nil || !valid {
		return false
	}

	app.Session.Put(r.Context(), "user", user)

	return true
}

// Type uploaded file

type UploadedFile struct {
	OriginalFileName string
	FileSize         int64
}

// file upload function for extracting files and storing in the uploadDir
func (app *application) UploadFiles(r *http.Request, uploadDir string) ([]*UploadedFile, error) {

	var uploadedFiles []*UploadedFile

	err := r.ParseMultipartForm(int64(1024 * 1024 * 5))
	if err != nil {
		return nil, fmt.Errorf("file uploaded is too big. user 5mb")
	}

	// get file
	for _, fileHeaders := range r.MultipartForm.File {
		for _, header := range fileHeaders {
			uploadedFiles, err = func(uploadedFiles []*UploadedFile) ([]*UploadedFile, error) {
				var uploadedFile UploadedFile
				infile, err := header.Open()
				if err != nil {
					return nil, err
				}

				defer infile.Close()

				uploadedFile.OriginalFileName = header.Filename

				var outfile *os.File

				defer outfile.Close()

				if outfile, err = os.Create(filepath.Join(uploadDir, uploadedFile.OriginalFileName)); err != nil {
					return nil, err
				} else {
					fileSize, err := io.Copy(outfile, infile)
					if err != nil {
						return nil, err
					}

					uploadedFile.FileSize = fileSize
				}

				uploadedFiles = append(uploadedFiles, &uploadedFile)

				return uploadedFiles, nil

			}(uploadedFiles)

			if err != nil {
				return uploadedFiles, err
			}
		}
	}

	return uploadedFiles, nil
}
