package main

import (
	"net/url"
	"strings"
)

type errors map[string][]string

// Get error on field if exists
func (e errors) GetError(field string) string {
	errorSlice := e[field]

	if len(errorSlice) == 0 {
		return ""
	}

	return errorSlice[0]
}

func (e errors) AddError(field, message string) {
	e[field] = append(e[field], message)
}

type Form struct {
	Data   url.Values
	Errors errors
}

// Creating new form
func NewForm(data url.Values) *Form {
	return &Form{
		Data:   data,
		Errors: map[string][]string{},
	}
}

// Check if form has a field
func (f *Form) HasField(field string) bool {
	x := f.Data.Get(field)

	if x == "" {
		return false
	}

	return true // the started field exists
}

// Require a certain field
func (f *Form) RequiredFields(fields ...string) {
	for _, field := range fields {
		value := f.Data.Get(field)
		if strings.TrimSpace(value) == "" {
			f.Errors.AddError(field, "This field cannot be empty")
		}
	}
}

// Generic for checking anything
func (f *Form) Check(ok bool, key, message string) {
	if !ok {
		f.Errors.AddError(key, message)
	}
}

// Check if form is valid
func (f *Form) Valid() bool {
	return len(f.Errors) == 0
	// returns true if valid or false if there are errors in the form
}
