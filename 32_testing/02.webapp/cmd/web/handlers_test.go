package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"image"
	"image/png"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path"
	"strings"
	"sync"
	"testing"
	"webapp/pkg/data"
)

func Test_application_handlers(t *testing.T) {

	tests := []struct {
		name                    string
		url                     string
		expectedStatusCode      int
		expectedURL             string
		expectedFirstStatusCode int
	}{
		{"home", "/", http.StatusOK, "/", http.StatusOK},
		{"404", "/fish", http.StatusNotFound, "/fish", http.StatusNotFound},
		{"profile", "/user/profile", http.StatusOK, "/", http.StatusTemporaryRedirect},
	}

	//
	// var app application
	routes := app.routes()

	testServer := httptest.NewTLSServer(routes)
	// built in test server which accepts routes
	// httptest.NewTLSServer is built in webserver on httptest

	defer testServer.Close() // close test server when done

	transport := &http.Transport{
		// Maximum of 10 redirects and returns the last one
		// InsecureSkipVerify to avoid unknown tls provider
		// This custom setting will return the first response code
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	// Client will accept invalid certificates
	// From the transport var because InsecureSkipVerify is set to true
	// The client will be tested to check the response statuses
	client := &http.Client{
		Transport: transport,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	for _, test := range tests {
		res, err := testServer.Client().Get(testServer.URL + test.url)
		if err != nil {
			t.Log(err)
			t.Fatal(err)
		}

		// Testing server status code
		if res.StatusCode != test.expectedStatusCode {
			t.Errorf("for %s: expected status %d, but got %d", test.name, test.expectedStatusCode, res.StatusCode)
		}

		// Testing server request url path
		if res.Request.URL.Path != test.expectedURL {
			t.Errorf("%s: expected final url of %s but got %s", test.name, test.expectedURL, res.Request.URL.Path)
		}

		// This is the test for page redirect
		// To test client response
		resTwo, _ := client.Get(testServer.URL + test.url)
		if resTwo.StatusCode != test.expectedFirstStatusCode {
			t.Errorf("%s: expected first status code of %d but got %d", test.name, test.expectedFirstStatusCode, resTwo.StatusCode)
		}
	}

}

// func TestAppHomeOld(t *testing.T) {
// 	// create a request
// 	req, _ := http.NewRequest("GET", "/", nil)

// 	req = addContextAndSessionToRequest(req, app)

// 	res := httptest.NewRecorder()

// 	handler := http.HandlerFunc(app.Home)

// 	handler.ServeHTTP(res, req)

// 	// Check status code
// 	if res.Code != http.StatusOK {
// 		t.Errorf("TestAppHome returned wrong status code: expected 200 but got %d", res.Code)
// 	}

// 	// check the body of html if exists
// 	body, _ := io.ReadAll(res.Body)

// 	if !strings.Contains((string(body)), `<p>From Session:`) {
// 		t.Error("did not find correct text in html")
// 	}
// }

func TestAppHome(t *testing.T) {
	var tests = []struct {
		name         string
		putInSession string
		expectedHTML string
	}{
		{"first visit", "", "<p>From Session:"},
		{"second visit", "hello world", "<p>From Session:  hello world"},
	}

	for _, test := range tests {
		// create a request
		req, _ := http.NewRequest("GET", "/", nil)

		req = addContextAndSessionToRequest(req, app)

		_ = app.Session.Destroy(req.Context())
		// makes sure there is nothing in the session

		if test.putInSession != "" {
			app.Session.Put(req.Context(), "test", test.putInSession)
		}

		responseRecorder := httptest.NewRecorder()

		handler := http.HandlerFunc(app.Home)

		handler.ServeHTTP(responseRecorder, req)

		// Check status code
		if responseRecorder.Code != http.StatusOK {
			t.Errorf("TestAppHome: expected 200 but got %d", responseRecorder.Code)
		}

		// check the body of html if exists
		body, _ := io.ReadAll(responseRecorder.Body)
		if !strings.Contains(string(body), test.expectedHTML) {
			t.Errorf("%s: did not find %s in response body", test.name, test.expectedHTML)
			// t.Errorf("%s: expected %s; got %s", test.name, test.expectedHTML, string(body))
		}

	}
}

func TestApp_renderWithBadTemplate(t *testing.T) {
	// set template path with location to a bad template
	templatesPath = "./testdata/"

	req, _ := http.NewRequest("GET", "/", nil)
	req = addContextAndSessionToRequest(req, app)

	responseRecorder := httptest.NewRecorder()

	err := app.render(responseRecorder, req, "bad.page.html", &TemplateData{})
	if err == nil {
		t.Error("expected error from bad template, but did not get any")
	}

	templatesPath = "./../../templates"

}

func Test_app_Login(t *testing.T) {
	var tests = []struct {
		name               string
		postedData         url.Values
		expectedStatusCode int
		expectedLocation   string
	}{
		{
			name: "valid login",
			postedData: url.Values{
				"email":    {"admin@example.com"},
				"password": {"secret"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/user/profile",
		},
		{
			name: "missing form data",
			postedData: url.Values{
				"email":    {""},
				"password": {""},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
		{
			name: "user not found",
			postedData: url.Values{
				"email":    {"admin@exxample.com"},
				"password": {"secret"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
		{
			name: "bad credentials",
			postedData: url.Values{
				"email":    {"admin@example.com"},
				"password": {"secret123"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
	}

	for _, test := range tests {
		req, _ := http.NewRequest("POST", "/login", strings.NewReader(test.postedData.Encode()))
		req = addContextAndSessionToRequest(req, app)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		responseRecorder := httptest.NewRecorder()
		handler := http.HandlerFunc(app.Login)

		handler.ServeHTTP(responseRecorder, req)

		// Testing the status code
		if responseRecorder.Code != test.expectedStatusCode {
			t.Errorf("%s: retured wrong status code; expected %d but got %d", test.name, test.expectedStatusCode, responseRecorder.Code)
		}

		// Testing the location redirect *** further studies
		actualLocation, err := responseRecorder.Result().Location()
		if err == nil {
			if actualLocation.String() != test.expectedLocation {
				t.Errorf("%s: expected location %s but got %s", test.name, test.expectedLocation, actualLocation.String())
			}
		} else {
			t.Errorf("%s: no location header set", test.name)
		}

	}
}

func Test_app_UploadFiles(t *testing.T) {
	// 1. Set up a pipe.
	pipeReader, pipeWriter := io.Pipe()

	// 2. Create a new writer of type *io.Writer.
	writer := multipart.NewWriter(pipeWriter)

	// Create a waitgroup and add 1 to id
	wg := sync.WaitGroup{}
	wg.Add(1)

	// 3.  Simulate uploading a file using a goroutine and created writer.
	go simulatePNGUpload("./../testdata/img.png", writer, t, &wg)

	// 4. Read from the pipe which receives data.
	request := httptest.NewRequest("POST", "/", pipeReader)
	request.Header.Add("Content-Type", writer.FormDataContentType())

	// 5. Call app.UploadFiles.
	uploadedFiles, err := app.UploadFiles(request, "./../testdata/uploads/")
	if err != nil {
		t.Error(err)
	}

	// 6. Perform Tests.
	if _, err := os.Stat(fmt.Sprintf("./../testdata/uploads/%s", uploadedFiles[0].OriginalFileName)); os.IsNotExist(err) {
		t.Errorf("expected to exist: %s ", err.Error())
	}

	// 7. Clean up file resources that were created.
	_ = os.Remove(fmt.Sprintf("./../testdata/uploads/%s", uploadedFiles[0].OriginalFileName))

	// 8. Use wait group to wait
	wg.Wait()

}

func Test_app_UploadProfilePic(t *testing.T) {
	uploadPath = "./../testdata/uploads"
	filePath := "./../testdata/img.png"

	//1. Specify field name for the form
	fieldName := "file"

	// 2. Create bytes.Buffer to act as the request body
	body := new(bytes.Buffer)

	// 3. Create a new writer
	multipartWriter := multipart.NewWriter(body)

	// 4. Open file you want to upload
	file, err := os.Open(filePath)
	if err != nil {
		t.Fatal(err)
	}

	// 5. Create form file
	w, err := multipartWriter.CreateFormFile(fieldName, filePath)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := io.Copy(w, file); err != nil {
		t.Fatal(err)
	}

	multipartWriter.Close()

	// 6. Build new request
	req := httptest.NewRequest(http.MethodPost, "/upload", body)
	req = addContextAndSessionToRequest(req, app)
	app.Session.Put(req.Context(), "user", data.User{ID: 1})
	req.Header.Add("Content-Type", multipartWriter.FormDataContentType())

	resRecorder := httptest.NewRecorder()

	handler := http.HandlerFunc(app.UploadProfilePicture)

	handler.ServeHTTP(resRecorder, req)

	if resRecorder.Code != http.StatusSeeOther {
		t.Errorf("wrong status code")
	}

	// 7. Clean up the upload folder
	_ = os.Remove("./../testdata/uploads/img.png")

}

// adding value to the context to avoid the error of missing value in the session
func getCtx(req *http.Request) context.Context {
	ctx := context.WithValue(req.Context(), contextUserKey, "unknown")
	return ctx
}

// calls the getCtx
func addContextAndSessionToRequest(req *http.Request, app application) *http.Request {
	// adding value to context
	req = req.WithContext(getCtx(req))

	//add information for session
	ctx, _ := app.Session.Load(req.Context(), req.Header.Get("X-Session"))

	return req.WithContext(ctx)
}

func simulatePNGUpload(fileToUpload string, writer *multipart.Writer, t *testing.T, wg *sync.WaitGroup) {
	defer writer.Close()
	defer wg.Done()

	// Create form data fields called file with value being filename
	part, err := writer.CreateFormFile("file", path.Base(fileToUpload))
	if err != nil {
		t.Error(err)
	}

	// Open the actual file which we will simulate uploading
	file, err := os.Open(fileToUpload)
	if err != nil {
		t.Error(err)
	}

	defer file.Close()

	// decode the PNG file
	img, _, err := image.Decode(file)
	if err != nil {
		t.Error("error decoding image")
	}

	// Write the PNG image to our io.Writer
	err = png.Encode(part, img)
	if err != nil {
		t.Error(err)
	}

}
