package main

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"webapp/pkg/data"
)

func Test_application_addIPToContext(t *testing.T) {
	tests := []struct {
		headerName   string
		HeaderValue  string
		address      string
		emptyAddress bool
	}{
		{"", "", "", false},
		{"", "", "", true},
		{"X-Forwarded-For", "192.3.2.1", "", false},
		{"", "", "hello:world", false},
	}

	// Create dummy handle to use to check the context
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// get the value of contextKey from the context with

		value := r.Context().Value(contextUserKey)
		// to get the contextUserKey
		// context should have that value

		if value == nil {
			t.Error(string(contextUserKey), "not present")
		}

		// make sure it is a string
		ip, ok := value.(string)
		if !ok {
			t.Error("not string")
		}

		t.Log(ip)

	})

	for _, test := range tests {
		// create the handle to test using custom handler
		handlerToTest := app.addIPToContext(nextHandler)

		req := httptest.NewRequest("GET", "http://testing", nil) // mock request

		if test.emptyAddress {
			req.RemoteAddr = ""
		}

		if len(test.headerName) > 0 {
			// add header to request before executing the test
			req.Header.Add(test.headerName, test.HeaderValue)
		}

		if len(test.address) > 0 {
			req.RemoteAddr = test.address
		}

		handlerToTest.ServeHTTP(httptest.NewRecorder(), req)

	}
}

func Test_application_ipFromContext(t *testing.T) {
	// create an app var from type application
	// var app application package level var app application in setup_test.go

	// get context
	ctx := context.Background()

	// put something in the context
	ctx = context.WithValue(ctx, contextUserKey, "anything")

	// call the function
	ip := app.ipFromContext(ctx)

	// perform the test
	if !strings.EqualFold("anything", ip) {
		t.Error("wrong value returned from the context")
	}

}

func Test_app_auth(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	var tests = []struct {
		name   string
		isAuth bool
	}{
		{"logged in", true},
		{"not logged in", false},
	}

	for _, entry := range tests {
		handlerToTest := app.auth(nextHandler)

		req := httptest.NewRequest("GET", "http://testing", nil)
		req = addContextAndSessionToRequest(req, app)

		// tests
		if entry.isAuth {
			// if authenticated put user into session
			app.Session.Put(req.Context(), "user", data.User{ID: 1})
		}

		responseRecorder := httptest.NewRecorder()
		handlerToTest.ServeHTTP(responseRecorder, req)

		// Test Status Code
		if entry.isAuth && responseRecorder.Code != http.StatusOK {
			t.Errorf("%s: expected status code of 200 but got %d", entry.name, responseRecorder.Code)
		}

		// Test Temporary Redirect
		if !entry.isAuth && responseRecorder.Code != http.StatusTemporaryRedirect {
			t.Errorf("%s: expected status code 307 but got %d", entry.name, responseRecorder.Code)
		}
	}
}
