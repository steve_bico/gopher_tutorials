package main

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestForm_HasField(t *testing.T) {
	form := NewForm(nil) // form with no fields

	// 1. Check if the form which is supposed to have no field has field
	has := form.HasField("whatever") // check if this form has whatever field
	if has {
		t.Error("form shows has field when it should not")
	}

	// 2. Create fiels in the form then check if they exists
	postedData := url.Values{}
	postedData.Add("username", "username")
	form = NewForm(postedData)

	has = form.HasField("username")
	if !has {
		t.Error("should have username field but non exists")
	}
}

func TestForm_RequiredFields(t *testing.T) {
	// 1. Create a request
	req := httptest.NewRequest("POST", "/home", nil)

	form := NewForm(req.PostForm)
	// req.PostForm is used for posting data using POST, PUT
	form.RequiredFields("a", "b", "c")

	// Form should not be valid since it should not have fields
	if form.Valid() {
		t.Error("forms shows valid when required fields are missing")
	}

	// 2. Test for a form which has fields
	// create formToPost and add the required fields
	formToPost := url.Values{}
	formToPost.Add("username", "username")
	formToPost.Add("email", "email")
	formToPost.Add("password", "password")

	// mock a post request with nil values
	req, _ = http.NewRequest("POST", "/home", nil)
	req.PostForm = formToPost

	form = NewForm(req.PostForm)
	form.RequiredFields("username", "email", "password")
	if !form.Valid() {
		t.Error("shows form does not have required fields when it does")
	}
}

func TestForm_Check(t *testing.T) {
	form := NewForm(nil)

	form.Check(false, "password", "password is required")
	if form.Valid() {
		t.Error("Valid() returns false, and it should return true when calling Check()")
	}
}

func TestForm_GetError(t *testing.T) {
	form := NewForm(nil)

	form.Check(false, "password", "password is required")
	string := form.Errors.GetError("password")

	// since the password is not there because of false in form.Check()
	// should return an error and if the string len is 0 then something went wrong
	if len(string) == 0 {
		t.Error("should have an error returned for Get(), but non is returned")
	}

	string = form.Errors.GetError("whatever")
	if len(string) != 0 {
		t.Error("should not have an error but do")
	}
}
