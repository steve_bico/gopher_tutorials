package main

import (
	"os"
	"testing"
	"webapp/pkg/repository/dbrepo"
)

// special file for setting up and must have setup_test

var app application

func TestMain(m *testing.M) {
	//NB: Will always be executed before any test run

	// load template for all the handles
	templatesPath = "./../../templates/"

	// make session available to apps during testing
	app.Session = getSession()

	//app.DB = db.PostgresConn{DB: conn}

	app.DB = &dbrepo.TestDBRepo{}

	// runs all the tests
	os.Exit(m.Run())
}

//NB:2 force a test to run with go test -count=1 .testlocations -> avoids cached images
