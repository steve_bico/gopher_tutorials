package main

import "net/http"

var allowedMethods = "GET,POST,PUT,PATCH,DELETE,OPTIONS"
var allowedHeaders = "Accept,Content-Type,X-CSRF-Token,Authorization"

func (app *application) enableCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-allow-Origin", "http://localhost:9001")
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", allowedMethods)
			w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func (app *application) authRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Get token from the header and determine if it is valid
		_, _, err := app.getTokenFromHeaderAndVerify(w, r)
		if err != nil {
			// this request is not authorized
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}
