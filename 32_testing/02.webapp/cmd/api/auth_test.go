package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"webapp/pkg/data"
)

func Test_app_getTokenFromHeaderAndVerify(t *testing.T) {
	// 1. Set up test user
	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	// 2. Generate token pairs
	tokens, _ := app.generateTokenPair(&testUser)

	// 3. Create table test
	var tests = []struct {
		name          string
		token         string
		errorExpected bool
		setHeader     bool
		issuer        string
	}{
		{"valid", fmt.Sprintf("Bearer %s", tokens.Token), false, true, app.Domain},
		{"valid but expired", fmt.Sprintf("Bearer %s", expiredToken), true, true, app.Domain},
		{"no header", "", true, false, app.Domain},
		{"invalid token", fmt.Sprintf("Bearer %s1", tokens.Token), true, true, app.Domain},
		{"no bearer", fmt.Sprintf("Bear %s", tokens.Token), true, true, app.Domain},
		{"three header parts", fmt.Sprintf("Bearer %s 1", tokens.Token), true, true, app.Domain},
		// make sure this is the last test to run
		{"wrong issuer", fmt.Sprintf("Bearer %s", tokens.Token), true, true, "anotherdomain.com"},
	}

	// 4. Loop through table test
	for _, test := range tests {

		// generating token for domain issuer if not app.Domain
		if test.issuer != app.Domain {
			app.Domain = test.issuer
			tokens, _ = app.generateTokenPair(&testUser)
		}

		// 5. Create a request object for GET
		request, _ := http.NewRequest("GET", "/", nil)

		// 6. Set Header if test.setHeader is true
		if test.setHeader {
			request.Header.Set("Authorization", test.token)
		}

		// 7. Create responseWriter
		responseWriter := httptest.NewRecorder()

		// 8. Get token from header and verify
		_, _, err := app.getTokenFromHeaderAndVerify(responseWriter, request)
		if err != nil && !test.errorExpected {
			t.Errorf("%s: did not expected error, but got one -%s", test.name, err.Error())
		}

		if err == nil && test.errorExpected {
			t.Errorf("%s: expected error,but did not get one", test.name)
		}

		app.Domain = "example.com"

	}
}
