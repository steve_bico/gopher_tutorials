package main

import (
	"net/http"
	"strings"
	"testing"

	"github.com/go-chi/chi/v5"
)

func Test_api_routes(t *testing.T) {

	// 1. Create a table test

	var registered = []struct {
		route  string
		method string
	}{
		// {"/", "GET"},
		{"/test", "GET"},
		{"/auth", "POST"},
		{"/refresh-token", "POST"},
		{"/users/all", "GET"},
		{"/users/{userID}", "GET"},
		{"/users/", "PUT"},
		{"/users/", "PATCH"},
		{"/users/{userID}", "DELETE"},
	}

	mux := app.routes()

	//
	chiRoutes := mux.(chi.Routes)

	for _, route := range registered {
		// Check to see if the route exists
		if !routeExist(route.route, route.method, chiRoutes) {
			t.Errorf("route %s is not registered", route.route)
		}

	}

}

func routeExist(testRoute, testMethod string, chiRoutes chi.Routes) bool {
	found := false
	_ = chi.Walk(chiRoutes, func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		if strings.EqualFold(method, testMethod) && strings.EqualFold(route, testRoute) {
			found = true
		}

		return nil
	})

	return found

}
