package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"webapp/pkg/data"
)

// Test enable cors
func Test_app_enableCORS(t *testing.T) {
	// Testing middleware requires

	// 1. Dummy HandlerFunc
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	// 2. Table tests
	var tests = []struct {
		name         string
		method       string
		expectHeader bool
	}{
		{"preflight", "OPTIONS", true},
		{"get", "GET", false},
	}

	for _, test := range tests {
		// 3. Handler to test
		handlerToTest := app.enableCORS(nextHandler)

		// 4. Mock request
		request := httptest.NewRequest(test.method, "http://testing", nil)
		responseRecorder := httptest.NewRecorder()

		handlerToTest.ServeHTTP(responseRecorder, request)

		// First test
		if test.expectHeader && responseRecorder.Header().Get("Access-Control-Allow-Credentials") == "" {
			t.Errorf("%s: expected header, but didn't get any", test.name)
		}

		// Second test
		if !test.expectHeader && responseRecorder.Header().Get("Access-Control-Allow-Credentials") != "" {
			t.Errorf("%s: expected no header but got one", test.name)
		}
	}

}

// test auth required
func Test_app_authRequired(t *testing.T) {
	// dummy handler
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	// test user for generating token
	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	tokens, _ := app.generateTokenPair(&testUser)

	var tests = []struct {
		name             string
		token            string
		expectAuthorized bool
		setHeader        bool
	}{
		{"valid token", fmt.Sprintf("Bearer %s", tokens.Token), true, true},
		{"no token", "", false, false},
		{"invalid token", fmt.Sprintf("Bearer %s", expiredToken), false, true},
	}

	for _, test := range tests {
		req, _ := http.NewRequest("GET", "/", nil)
		if test.setHeader {
			req.Header.Set("Authorization", test.token)
		}
		rr := httptest.NewRecorder()

		handlerToTest := app.authRequired(nextHandler)

		handlerToTest.ServeHTTP(rr, req)

		if test.expectAuthorized && rr.Code == http.StatusUnauthorized {
			t.Errorf("%s: got code 401 and should not have ", test.name)
		}

		if !test.expectAuthorized && rr.Code != http.StatusUnauthorized {
			t.Errorf("%s: did not get 401, and should have ", test.name)
		}
	}

}
