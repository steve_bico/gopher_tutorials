package main

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"
	"webapp/pkg/data"

	"github.com/go-chi/chi/v5"
)

func Test_api_authenticate(t *testing.T) {
	var tests = []struct {
		name               string
		requestBody        string
		expectedStatusCode int
	}{
		{"valid user", `{"email":"admin@example.com","password":"secret"}`, http.StatusOK},
		{"not json", `not json`, http.StatusBadRequest},
		{"invalid user", `{"email":"adminn@example.com","password":"secret"}`, http.StatusUnauthorized},
		{"empty json", `{}`, http.StatusUnauthorized},
		{"empty email", `{"email":"","password":"secret"}`, http.StatusUnauthorized},
		{"empty password", `{"email":"adminn@example.com","password":""}`, http.StatusUnauthorized},
	}

	for _, test := range tests {
		// 1. Get the reader
		var reader io.Reader

		// 2. Read request body
		reader = strings.NewReader(test.requestBody)

		// 3. Make request with reader being payload
		request, _ := http.NewRequest("POST", "/auth", reader)

		// 4. Get responseRecorder
		responseRecorder := httptest.NewRecorder()

		// 5. Create handlerFunc
		handler := http.HandlerFunc(app.authenticate)

		// 6. Serve request
		handler.ServeHTTP(responseRecorder, request)

		// 7. Carry out the test
		if test.expectedStatusCode != responseRecorder.Code {
			t.Errorf("%s: retured wrong status code; expected %d but got %d", test.name, test.expectedStatusCode, responseRecorder.Code)
		}
	}
}

func Test_api_refresh(t *testing.T) {
	tests := []struct {
		name               string
		token              string
		expectedStatusCode int
		resetRefreshTime   bool
	}{
		{"valid", "", http.StatusOK, true},
		{"not yet expired valid", "", http.StatusTooEarly, false},
		{"expired token", expiredToken, http.StatusBadRequest, false},
	}

	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	oldRefreshTime := refreshTokenExpiry

	for _, test := range tests {
		var token string
		if test.token == "" {
			if test.resetRefreshTime {
				refreshTokenExpiry = time.Second * 1
			}
			tokens, _ := app.generateTokenPair(&testUser)
			token = tokens.RefreshToken
		} else {
			token = test.token
		}

		postedData := url.Values{
			"refresh_token": {token},
		}

		request, _ := http.NewRequest("POST", "/refresh-token", strings.NewReader(postedData.Encode()))
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		responseRecorder := httptest.NewRecorder()

		handler := http.HandlerFunc(app.refresh)

		handler.ServeHTTP(responseRecorder, request)

		if responseRecorder.Code != test.expectedStatusCode {
			t.Errorf("%s: expected status of %d but got %d", test.name, test.expectedStatusCode, responseRecorder.Code)
		}

		refreshTokenExpiry = oldRefreshTime

	}

}

func Test_api_userHandlers(t *testing.T) {
	var tests = []struct {
		name           string
		method         string
		json           string
		paramID        string
		handler        http.HandlerFunc
		expectedStatus int
	}{
		{"allUsers", "GET", "", "", app.allUsers, http.StatusOK},
		{"deleteUser", "DELETE", "", "1", app.deleteUser, http.StatusNoContent},
		{"deleteUser bad url", "DELETE", "", "a", app.deleteUser, http.StatusBadRequest},
		{"getUser valid", "GET", "", "1", app.getUser, http.StatusOK},
		{"getUser invalid", "GET", "", "10", app.getUser, http.StatusBadRequest},
		{"getUser bad url param", "GET", "", "b", app.getUser, http.StatusBadRequest},
		{"updateUser valid",
			"PATCH",
			`{"id":1, "first_name":"Administrator","last_name":"User","email":"admin@example.com"}`,
			"",
			app.updateUser,
			http.StatusNoContent,
		},
		{"updateUser invalid",
			"PATCH",
			`{"id":100, "first_name":"Administrator","last_name":"User","email":"admin@example.com"}`,
			"",
			app.updateUser,
			http.StatusBadRequest,
		},
		{"updateUser invalid json",
			"PATCH",
			`{"id":1, first_name:"Administrator","last_name":"User","email":"admin@example.com"}`,
			"",
			app.updateUser,
			http.StatusBadRequest,
		},
		{"inserUser valid",
			"PUT",
			`{"first_name":"Jack","last_name":"Smith","email":"jack@example.com"}`,
			"",
			app.insertUser,
			http.StatusNoContent,
		},
		{"inserUser invalid",
			"PUT",
			`{"foo":"bar","first_name":"Jack","last_name":"Smith","email":"jack@example.com"}`,
			"",
			app.insertUser,
			http.StatusBadRequest,
		},
		{"inserUser invalid json",
			"PUT",
			`{first_name:"Jack","last_name":"Smith","email":"jack@example.com"}`,
			"",
			app.insertUser,
			http.StatusBadRequest,
		},
	}

	for _, test := range tests {
		var req *http.Request
		if test.json == "" {
			// This test does not require a json payload
			req, _ = http.NewRequest(test.method, "/", nil)
		} else {
			// This test has a json request body payload
			req, _ = http.NewRequest(test.method, "/", strings.NewReader(test.json))
		}

		// Check if there is param id
		if test.paramID != "" {
			chiContext := chi.NewRouteContext()
			chiContext.URLParams.Add("userID", test.paramID)
			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, chiContext))

		}

		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(test.handler)

		handler.ServeHTTP(rr, req)

		if rr.Code != test.expectedStatus {
			t.Errorf("%s: wrong status returned expected %d but got %d", test.name, test.expectedStatus, rr.Code)
		}
		// https://portal.dev01.int.betika.com/site/reset-password?token=SE9OnOCE-XGu78MJKWL1xPop9XyejrB4_1725348856
		

	}
}
