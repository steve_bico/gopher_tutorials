package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func (app *application) routes() http.Handler {
	mux := chi.NewRouter()

	//1. Register middleware for panick recovere
	mux.Use(middleware.Recoverer)
	mux.Use(app.enableCORS)

	//2. Enable CORS

	//3. Authentication routes - auth handler, refresh
	mux.Post("/auth", app.authenticate)
	mux.Post("/refresh-token", app.refresh)

	//4. Test Handler for dev
	mux.Get("/test", func(w http.ResponseWriter, r *http.Request) {
		var payload = struct {
			Message string `json:"message"`
		}{
			Message: "Hello World",
		}

		_ = app.writeJSON(w, http.StatusOK, payload)
	})

	//5. Protected routes
	mux.Route("/users", func(mux chi.Router) {
		mux.Use(app.authRequired)
		// use auth middleware
		mux.Get("/all", app.allUsers)
		mux.Get("/{userID}", app.getUser)
		mux.Delete("/{userID}", app.deleteUser)
		mux.Put("/", app.insertUser)
		mux.Patch("/", app.updateUser)

	})

	return mux

}
