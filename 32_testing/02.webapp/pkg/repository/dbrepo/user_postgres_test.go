//go:build integration

package dbrepo

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
	"webapp/pkg/data"
	"webapp/pkg/repository"

	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	// by using go:build integration, we have tagged our tests which makes it run fast
	// use go test -v -tag=integration . where . is the context
	// run all the test with go test -v -tags=integration ./...
)

var (
	host     = "localhost"
	user     = "postgres"
	password = "postgres"
	dbName   = "users_test"
	port     = "5435"
	dsn      = "host=%s port=%s user=%s password=%s dbname=%s sslmode=disable timezone=UTC connect_timeout=5"
)

var resource *dockertest.Resource
var pool *dockertest.Pool
var testDB *sql.DB
var testRepo repository.DatabaseRepo

// Utility function to create tables
func createTables() error {
	tableSQL, err := os.ReadFile("./testdata/users.sql")
	if err != nil {
		fmt.Println(err)
		return err
	}

	_, err = testDB.Exec(string(tableSQL))
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

// Only for integration test
func TestMain(m *testing.M) {

	// 1. Connect dot docker and fail if docker is not runnning
	p, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("could not connect to docker %s", err)
	}

	pool = p

	// 2. Set up docker options, specifying image and others
	options := dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "14.5",
		Env: []string{
			"POSTGRES_USER=" + user,
			"POSTGRES_PASSWORD=" + password,
			"POSTGRES_DB=" + dbName,
		},
		ExposedPorts: []string{"5432"},
		PortBindings: map[docker.Port][]docker.PortBinding{
			"5432": {
				{HostIP: "0.0.0.0", HostPort: port},
			},
		},
	}

	// 3. Get a resource (instance of docker image)
	resource, err = pool.RunWithOptions(&options)
	if err != nil {
		_ = pool.Purge(resource)
		log.Fatalf("Could not start resource: %s", err)
	}

	// 4. Start image and wait till it's ready
	if err := pool.Retry(func() error {
		var err error
		testDB, err = sql.Open("pgx", fmt.Sprintf(dsn, host, port, user, password, dbName))

		if err != nil {
			log.Println("Error: ", err)
			return err
		}

		return testDB.Ping()
	}); err != nil {
		_ = pool.Purge(resource)
		log.Fatalf("could not connect to database: %s", err)
	}

	// 5. Populate the db with empty tables
	err = createTables()
	if err != nil {
		log.Fatalf("error creating tables %s", err)
	}

	// Creating a pool of database connection
	testRepo = &PostgresDBRepo{DB: testDB}

	// 6. run the test
	code := m.Run()

	// 7. Clean up and start the test in known state
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("coult not purge resource %s ", err)
	}

	os.Exit(code)
}

func Test_pingDB(t *testing.T) {
	err := testDB.Ping()
	if err != nil {
		t.Error("cannot ping database")
	}
}

func TestDBInsertUser(t *testing.T) {
	testUser := data.User{
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
		Password:  "secret",
		IsAdmin:   1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	id, err := testRepo.InsertUser(testUser)
	if err != nil {
		t.Errorf("insert user returned an error: %s ", err)

	}

	if id != 1 {
		t.Errorf("insert user retured wrong id; expected 1, got %d", id)
	}
}

func TestDBAllUsers(t *testing.T) {
	users, err := testRepo.AllUsers()
	if err != nil {
		t.Errorf("all users reports and error %s", err)
	}

	if len(users) != 1 {
		t.Errorf("all user report wrong size expected 1, got %d", len(users))
	}

	testUser := data.User{
		FirstName: "John",
		LastName:  "Doe",
		Email:     "doe@example.com",
		Password:  "secret",
		IsAdmin:   1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	_, _ = testRepo.InsertUser(testUser)

	users, err = testRepo.AllUsers()
	if err != nil {
		t.Errorf("all users reports and error %s", err)
	}

	if len(users) != 2 {
		t.Errorf("all user report wrong size after insert. expected 2, got %d", len(users))
	}

}

func TestDBGetUser(t *testing.T) {
	user, err := testRepo.GetUser(1)
	if err != nil {
		t.Errorf("error getting user by id: %s", err)
	}

	if user.Email != "admin@example.com" {
		t.Errorf("wrong email retuned by GetUser; exptected admin@example.com but got %s ", user.Email)
	}

	_, err = testRepo.GetUser(5) // user who does not exist
	if err == nil {
		t.Error("no error reported for getting non existence user by id")
	}
}

func TestDBGetUserByEmail(t *testing.T) {
	user, err := testRepo.GetUserByEmail("doe@example.com")
	if err != nil {
		t.Errorf("error getting user by id: %s", err)
	}

	if user.ID != 2 {
		t.Errorf("wrong id retuned; exptected 2 but got %d ", user.ID)
	}
}

func TestDBUpdateUser(t *testing.T) {
	user, _ := testRepo.GetUser(2)
	user.FirstName = "Jane"
	user.Email = "jane@example.com"

	err := testRepo.UpdateUser(*user)
	if err != nil {
		t.Errorf("error updating user %d: %s", 2, err)
	}

	user, _ = testRepo.GetUser(2)
	if user.FirstName != "Jane" || user.Email != "jane@example.com" {
		t.Errorf("expected Jane & jane@example.com but got %s %s", user.FirstName, user.Email)
	}
}

func TestDBDeleteUser(t *testing.T) {
	err := testRepo.DeleteUser(2)
	if err != nil {
		t.Errorf("error deleting user by id: %s", err)
	}

	user, err := testRepo.GetUser(2)
	if err == nil {
		t.Errorf("delete user: expected no user but found %d ", user.ID)
	}

}

func TestDBResetPassword(t *testing.T) {
	err := testRepo.ResetPassword(1, "secret2")
	if err != nil {
		t.Errorf("update password test failed %s", err)
	}

	user, err := testRepo.GetUserByEmail("admin@example.com")
	if err != nil {
		t.Errorf("update password: error updating user %s", err)
	}

	matches, err := user.PasswordMatches("secret2")
	if err != nil {
		t.Error(err)
	}

	if !matches {
		t.Errorf("password should match 'secret2' but got %t", matches)
	}

}

func TestDBInsertImage(t *testing.T) {
	var image data.UserImage
	image.UserID = 1
	image.FileName = "testimage.jpg"
	image.CreatedAt = time.Now()
	image.UpdatedAt = time.Now()

	newId, err := testRepo.InsertUserImage(image)
	if err != nil {
		t.Error("inserting user image failed", err)
	}

	if newId != 1 {
		t.Error("got wrong id id for image should be 1 but got ", newId)
	}

	image.UserID = 100 // user who does not exist
	_, err = testRepo.InsertUserImage(image)
	if err == nil {
		t.Error("inserting user with non existing user id", err)
	}

}
