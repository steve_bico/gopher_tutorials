package main

import (
	"fmt"
	"sync"
)

// Using mutex, waitgroup and data race
// Check for race condition with go run --race main.go

func thisIsMutex() {

	wg := &sync.WaitGroup{}
	mut := &sync.Mutex{} // mutex is used during write or read operations to lock memory

	var score = []int{0}

	wg.Add(3) // where 3 is the number of goroutines
	go func(wg *sync.WaitGroup, mut *sync.Mutex) {
		fmt.Println("One Routine 1")
		mut.Lock() // lock the memory
		score = append(score, 1)
		mut.Unlock() // unlock the memory after operation
		wg.Done()    // Notifies the main goroutine that this goroutine is done
	}(wg, mut)

	go func(wg *sync.WaitGroup, mut *sync.Mutex) {
		fmt.Println("One Routine 2")
		mut.Lock() // lock the memory
		score = append(score, 2)
		mut.Unlock() // unlock the memory after operation
		wg.Done()
	}(wg, mut)

	go func(wg *sync.WaitGroup, mut *sync.Mutex) {
		fmt.Println("One Routine 3")
		mut.Lock() // lock the memory
		score = append(score, 3)
		mut.Unlock() // unlock the memory after operation
		wg.Done()
	}(wg, mut)

	wg.Wait() //wait till all the goroutines perform their jobs

	fmt.Println(score)

	// You can also use RWMutex which locks a resource while reading and writing
}
