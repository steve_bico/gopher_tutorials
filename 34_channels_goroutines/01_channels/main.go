package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://amazon.com",
		"http://golang.org",
		"http://stackoverflow.com",
	}

	// creating a channel
	c := make(chan string)

	for _, link := range links {
		go checkLink(link, c)

	}

	// fmt.Println(<-c) // get value from channel and print
	// fmt.Println(<-c)
	// fmt.Println(<-c)
	// fmt.Println(<-c)
	// fmt.Println(<-c)

	// for {
	// 	//fmt.Println(<-c)
	// 	go checkLink(<-c, c)
	// }

	for l := range c {
		go func(link string) {
			time.Sleep(time.Second)
			checkLink(link, c)
		}(l)
	}

}

func checkLink(link string, c chan string) {
	// to use channel, you have to pass it into a function
	// it has a type chan and the type of data which it holds
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, " might be down")
		c <- link
		return
	}

	fmt.Println(link, " is up")
	c <- link
}

/*
Concurrency - we can have multiple threads executing code.
If one thread blocks, another on is picked up and worked on.
This is done by Go scheduler.
This can be done when only one CPU core is being used.

use keyword go which will be used to run the code concurrently
when go schedular sees a non blocking code, it passes the command
to the main routine

Parallelism - multiple threads executed at the exact same time.
Requires MULTIPLE CPUs.



Channels
with goroutines, if child routines have some work still being done and the schedular
passes control to the main routine, if there is no other code, the main routine exits
- this is not ideal and that is where 'channels' come in.
- channels are used to communicate between different goroutines
- that is the only way to communicate between goroutines
- data passed into the channel for different goroutines must be typed just like any other variable.
-

Passing Data To and From channel
channel <- 5 sending value '5' into this channel
myNumber <- channel Wait for a value to be sent into a channel
			when we get one, assign it into myNumber variable
fmt.Println(<- channel) wait for a value to be sent into the channel.
						When we get one, log it out immediately.


NB: Receiving messages from a channel is a blocking operation


*/
