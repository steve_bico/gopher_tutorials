package main

import (
	"fmt"
	"sync"
)

// Using mutex, waitgroup and data race

func thisIsRace() {

	wg := &sync.WaitGroup{}

	var score = []int{0}

	wg.Add(3) // where 3 is the number of goroutines
	go func(wg *sync.WaitGroup) {
		fmt.Println("One Routine 1")
		score = append(score, 1)
		wg.Done() // Notifies the main goroutine that this goroutine is done
	}(wg)

	go func(wg *sync.WaitGroup) {
		fmt.Println("One Routine 2")
		score = append(score, 2)
		wg.Done()
	}(wg)

	go func(wg *sync.WaitGroup) {
		fmt.Println("One Routine 3")
		score = append(score, 3)

		wg.Done()
	}(wg)

	wg.Wait() //wait till all the goroutines perform their jobs

	fmt.Println(score)
}
