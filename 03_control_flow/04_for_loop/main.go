package main

import "fmt"

func main() {
	// For loop is used to perform an operation repeatedly
	// Go only has for loop
	// printing number 0 - 9

	for i := 0; i < 10; i++ {
		fmt.Println(i)
		//prints number from zero to 9
		// can also use i++ in the for loop instead of outside
	}

	//while loop is not in go

	j := 10

	for j >= 0 {
		fmt.Println(j)
		j--
		// this code as same effect as while loop in other programming languages
		// will reduce j by 1 till the condition is false
	}

	// infinite loop

	// k := 0

	// for {
	// 	k++
	// 	fmt.Println(k)
	// 	// infinite loop

	// }

	// continue statement - returns control to the beginning of the for loop and reject all the statements
	// used to skip remaining code in the loop

	for d := 0; d < 10; d++ {
		if d%2 != 0 {
			continue
		}

		fmt.Println(d)

	}

	// Break statement
	// used to terminate a loop
	//

	count := 0

	for i := 0; true; i++ {
		if i%13 == 0 {
			fmt.Printf("%d is divisible by 13 \n", i)
			count++
		}

		if count == 10 {
			break
			// after getting 10 numbers divisible by 13 break out of the loop
		}
	}

	fmt.Println("Just a message after the for loop")

}
