package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	// getting user input
	fmt.Println("os.Args", os.Args)
	// returns the value of os.Args
	fmt.Println("Path ", os.Args[0])
	fmt.Println("First argument ", os.Args[1])    // returns first args
	fmt.Println("Second argument ", os.Args[2])   // second arg
	fmt.Println("Number of items ", len(os.Args)) //total number of items

	var result, err = strconv.ParseFloat(os.Args[1], 64)
	_ = err
	fmt.Println(result)

}
