package main

import "fmt"

func main() {
	// LABEL STATEMENTS
	// - are used in break, continue, and goto statements
	// - it is illegal to define a label that is never used
	// - labels are not block scope and do not conflict with identifiers that are not labels. They live in another space
	// - the scope of a label is the body of the function in which it is declared and excludes the body of any nested function
	// - most of the time, labels are used to terminate outer enclosing loops

	outer := 20
	_ = outer

	people := [5]string{"Hellen", "Mark", "Brenda", "Antonio", "Michael"}
	friends := [2]string{"Mark", "Mary"}

outer:
	for index, name := range people {
		for _, friend := range friends {
			if name == friend {
				fmt.Printf("Found a friend %q at index %d", friend, index)
				break outer
			}
		}
	}

	fmt.Println("Next instructions after the break")
}
