package main

import "fmt"

func main() {
	i := 0

loop:
	if i < 5 {
		fmt.Println(i)
		i++
		goto loop
		// will transfer execution of the program to loop
	}

	// goto todo
	x := 9

	// this is not aloud

todo:
	fmt.Println("Something here")
	fmt.Printf(`This is my something test for %v`, x)
	goto todo

	// goto is highly discourage because it makes it hard to understand the program
}
