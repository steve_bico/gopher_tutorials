package main

import "fmt"

func main() {
	fmt.Println("if,else if, else statements")
	// are used for logic and decision making if certain creteria are met
	// if some condition is true, execute this code
	// if the second condition is true execute this code
	// can only one if statement
	// can be in bracket or not

	price, inStock := 1000, true

	if price > 100 {
		fmt.Printf("The %v is too expensive", price)
	}

	if price <= 1000 && inStock == true {
		fmt.Printf("The price %v is affordable and we can take it", price)
	} else {
		fmt.Printf("The price %v is too expensive for us ", price)
	}

	// non boolean types cannot be used for comparison
	/*

		if price{
			Println("This is a bad code")
		}

		// will return compile type error
		// cannot be used in go

	*/

	if price < 1000 {
		fmt.Printf("This price %v is cheap", price)
	} else if price == 1000 {
		fmt.Printf("The price %d is affordable", price)
	} else {
		fmt.Printf("The price %d is expensive", price)
	}

	age := 50

	if age >= 0 && age < 18 {
		fmt.Printf("You cannot vote.  Please return in %d years", 18-age)
	} else {
		fmt.Printf("Welcome to the booth. Act your age (%d) and make it count", age)
	}

}
