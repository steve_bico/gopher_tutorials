package main

func main() {
	// scope means visibility
	// scope or the lifetime of a variable is the interval of time during which it exists as the program executes
	// a name cannot be declared again in the same scope eg function but can be declared in another scope
	// there are 3 scopes in Go
	// 1. File Scope - import statement are file scoped. Cannot import fmt twice
	// 2. Package Scope - const done = false, func main this are package scoped
	// 3. Block(Local) scope - local scoped are variables in a block
}
