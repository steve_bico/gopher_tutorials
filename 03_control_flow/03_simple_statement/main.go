package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	// simple statetement
	i, err := strconv.Atoi("55")

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(i)
	}

	// using simple statement

	if i, err := strconv.Atoi("30"); err == nil {
		// first part of if is called initialization statement
		// the second part is boolean expression return true or false
		//
		fmt.Printf("There is no error , i is %v", i)
	} else {
		fmt.Println(err)
	}

	if args := os.Args; len(args) != 2 {
		// go run main.go <without args>
		fmt.Println("One argument is required")
	} else if km, err := strconv.Atoi(args[1]); err != nil {
		// go run main.go <with args which has alphabet>
		fmt.Printf("The argument must be an integer! Error: %v", err)
	} else {
		// go run main.go with <number>
		// converts to mile and prints miles
		fmt.Printf("%d km in miles is %v \n", km, float64(km)*0.621)
	}
}
