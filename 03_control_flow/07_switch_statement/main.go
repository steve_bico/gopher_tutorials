package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("This is SWITCH SECTION")
	// go uses switch statements
	// switch statements are used where there are long if statements
	// values must be comparable in switch statement and case
	// cannot compare ints to strings
	// break statement is added automatically by go unlike other programing languages
	// cases are evaluated from top to bottom

	language := "golang"

	switch language {
	case "python":
		fmt.Println("You are learning python")
	case "js":
		fmt.Println("You are studying javascript")

	case "Go", "golang":
		//represents OR with the , compares Go with golang
		fmt.Println("Good go for Go, you are using curly braces {}")

	default:
		// default gets executed if no testing condition matches
		// equivalent of else
		// executed if non of the statement is true
		// cases are
		fmt.Println("Any other language is a good start")

	}

	number := 5

	switch true {
	case number%2 == 0:
		fmt.Printf(" %d is an even number\n", number)
	case number%2 != 0:
		fmt.Printf("%d is an odd number \n", number)

	default:
		fmt.Println("Never run")

	}

	hour := time.Now().Hour()
	//fmt.Println(hour)

	switch {
	// switch without statement means true
	case hour < 12:
		fmt.Println("Good morning")
	case hour < 17:
		fmt.Println("Good afternoon")
	default:
		fmt.Println("Good evening")

	}

}
