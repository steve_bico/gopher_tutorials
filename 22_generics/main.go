package main

import "fmt"

func main() {
	fmt.Println("This is generics section")

	result := add(1, 3)
	fmt.Println(result)
}

// to declare generics in a function, supply [T] placeholder before params
// this avoids the situation of empty interface
// by adding [T interface{}] and return type of T,
// we are letting the compiler know that the types of the params and return type
// will only be known when the function is called
// can use T int | string | float64
func add[T int | string | float64](a, b T) T {
	return a + b

}
