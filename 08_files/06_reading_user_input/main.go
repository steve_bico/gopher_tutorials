package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	// scan from the console
	// input string comes from command line
	scanner := bufio.NewScanner(os.Stdin)

	//fmt.Printf("%T\n", scanner)

	scanner.Scan()

	text := scanner.Text()
	bytes := scanner.Bytes()

	fmt.Println("Input text is ", text)
	fmt.Println("Input bytes is ", bytes) // returns the ascii code for the input
	// use < for file input

	// Read input contiously

	for scanner.Scan() {
		text = scanner.Text()
		fmt.Println("You have entered ", text)

		if text == "exit" {
			fmt.Println("Exisitn the scanning ...")
			break
		}
	}

	fmt.Println("Just a message after the for loop")

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
