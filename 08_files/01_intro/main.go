package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	// creating a new file

	var newFile *os.File
	fmt.Printf("%T\n", newFile)
	// *os.File

	// no exception in go
	// creating a new file
	var err error

	newFile, err = os.Create("app.txt")
	// Create creates a file if it does not exist
	// will create a file in the current working dir of the main.go
	//

	if err != nil {
		// fmt.Println("An error occured ")
		// os.Exit(1)
		//

		log.Fatal(err) //exits the program and logs the error
	}

	// os.Trancate('File',numeric_value) numeric value trancates the file in bytes

	err = os.Truncate("app.txt", 0)

	if err != nil {
		log.Fatal(err)
	}

	// close file when done
	newFile.Close()

	// Opening file
	// file is opened in read only mode

	file, err := os.Open("app.txt")
	fileOne, err := os.OpenFile("jam.txt", os.O_CREATE|os.O_APPEND, 0644)
	// os.O_CREATE|os.O_APPEND - create if does not exist and append
	// append information if it exists

	if err != nil {
		log.Fatal(err)
	}

	// closing the file after using
	file.Close()
	fileOne.Close()

	// getting file information

	var fileInfo os.FileInfo

	fileInfo, err = os.Stat("app.txt")

	fmt.Println("File name: ", fileInfo.Name())           //returns file name
	fmt.Println("File size in bytes ", fileInfo.Size())   // returns file size in bytes
	fmt.Println("File last modified", fileInfo.ModTime()) // returns last modified time
	fmt.Println("Is directory ?", fileInfo.IsDir())       // returns if the file is directory
	fmt.Println("Check permissions", fileInfo.Mode())     // returns the permissions

	// checking if the file exists

	fileInfo, err = os.Stat("b.txt")

	if err != nil {
		if os.IsNotExist(err) {
			//log.Fatal("File does not exist")
			// terminates the program and the program will never reach the end
			fmt.Println("File does not exist")
		}
	}

	// Renaming a file
	oldPath := "app.txt"
	newPath := "new_app.txt"

	err = os.Rename(oldPath, newPath)
	// changed the name of the old app.txt to new_app.txt

	if err != nil {
		log.Fatal(err)
		// ternimates the app and the other code will never run
	}

	// Removing a File
	err = os.Remove("new_app.txt")
	if err != nil {
		log.Fatal(err)
	}

}
