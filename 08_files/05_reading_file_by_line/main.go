package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	//use buffio.Scanner

	file, err := os.Open("myfile.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	success := scanner.Scan()

	if success == false {
		err = scanner.Err()

		if err == nil {
			log.Println("Scan was completed and it reached end of file")
		} else {
			log.Fatal(err)
		}
	}

	// getting data from scan

	fmt.Println("First lin found ", scanner.Text())

	// reading the whole part
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
