package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	// Reading files

	//open the file
	file, err := os.Open("text.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// reading part of file in by silce

	byteSlice := make([]byte, 2)

	numberBytesRead, err := io.ReadFull(file, byteSlice)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Number of bytes read %d\n", numberBytesRead)
	log.Printf("Data read %s\n", byteSlice)

	// reading the entire file
	file, err = os.Open("text.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	data, err := ioutil.ReadAll(file)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Data as string %s\n", data)             // returns the entire content of the data
	fmt.Printf("Number of bytes read %v \n", len(data)) // returns the number of bytes in the file

	//Reading file with ioutils
	// handles reading and closing of file

	data, err = ioutil.ReadFile("text.txt")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Data read is %s\n", data)

}
