package main

import (
	"bufio"
	"log"
	"os"
)

func main() {
	//
	file, err := os.OpenFile("myfile.txt", os.O_WRONLY|os.O_CREATE, 0644)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// writing buffer writer
	bufferedWriter := bufio.NewWriter(file)

	bs := []byte{97, 98, 99}

	bytesWritten, err := bufferedWriter.Write(bs)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Bytes written to buffer (not file) %d\n", bytesWritten)

	bytesAvailable := bufferedWriter.Available()

	log.Printf("Bytes available in buffer: %d\n", bytesAvailable)

	bytesWritten, err = bufferedWriter.WriteString("\nJust random string")

	if err != nil {
		log.Fatal(err)
	}

	unflushedBufferSize := bufferedWriter.Buffered()
	log.Printf("Bytes buffered: %d\n", unflushedBufferSize)

	// writes the file to disk
	bufferedWriter.Flush()

	// reversing changes
	bufferedWriter.Reset(bufferedWriter)

}
