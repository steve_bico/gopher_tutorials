package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	file, err := os.OpenFile(
		"b.txt",
		os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
		0644,
	)

	if err != nil {
		fmt.Println("Something went wrong")
		log.Fatal(err)
	}

	//defer statement - defers execution of other functions till de-fered activity is done

	defer file.Close()

	// writing bytes to files

	byteSlice := []byte("I love golang")

	byteWritten, err := file.Write(byteSlice)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Bytes written  %d\n", byteWritten)

	// can also write to a file useing ioutil.WriteFile()
	// WriteFile() function is

	bs := []byte("Go programming is cool")

	err = ioutil.WriteFile("c.txt", bs, 0644)

	if err != nil {
		log.Fatal(err)
	}

}
