package main

import "fmt"

func main() {
	fmt.Println("This is header and cloning section")

	friends := map[string]int{"Dan": 40, "Maria": 20}

	neighbors := friends

	friends["Dan"] = 50

	fmt.Println(neighbors)
	// neighbours map has been modified
	// neighbors and friends have the same head in memory

	// cloning a map
	// create an empty map
	// loop throught the map to be cloned
	// assign key value to the new map

	people := make(map[string]int)

	for k, v := range friends {
		people[k] = v
	}

	fmt.Println(people)
	// people heaer and friends header are different in the memory
}
