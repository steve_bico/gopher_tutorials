package main

import "fmt"

func main() {
	fmt.Println("This is map section")

	// declaring a map
	var employees map[string]string
	// [string] keys of type string
	// sting values of type string

	fmt.Printf("%#v\n", employees)
	// zero value is nil
	// map[string]string(nil)

	//checking for numbers of pairs in the map
	fmt.Printf("The number of pairs is %d\n", len(employees))
	// the number of pairs is 0

	// getting the value of a key
	//fmt.Printf(`The value of "Dan" in empolyees is %v \n`, employees["Dan"])
	// if element does not exist it returns empty string for strings, zero for floats and integer etc

	var accounts map[string]float64
	fmt.Printf("%#v\n", accounts["0x234"])
	// returns 0

	// Inserting Values in Maps
	// employees["Dan"] = "Dev"
	// employees["MAx"] = "UI/UX"
	// employees["Bill"] = "Support"

	// fmt.Printf("The employee map has %v \n", employees)
	// throws an error

	// empty but initialized map
	people := map[string]float64{}

	// assigning value to map
	people["Mike"] = 21.33
	people["Kim"] = 10.50
	people["Dave"] = 90.55

	fmt.Println(people)
	// returns map[Dave:90.55 Kim:10.50 Mike:21.33]

	// initialized but empty map
	// use keyword make to create a map which is empty
	mapOne := make(map[string]int)

	mapOne["Benz"] = 20_000_000
	mapOne["Toyota"] = 1_000_000
	mapOne["BMW"] = 2_000_000
	mapOne["Honda"] = 900_000

	fmt.Println(mapOne)

	// initialize map with key value pairs
	// last comma is mandatory when maps are declared in separate lines
	balances := map[string]float64{
		"USD":  23.44,
		"EURO": 34.55,
		"KSH":  145.99,
	}

	fmt.Println(balances)

	m := map[string]int{"Mark": 20, "Jane": 30, "Steve": 40} // does not need trailing comma
	_ = m

	// if key exist it updates the value and if it does not it adds

	balances["USD"] = 25.55
	balances["GBP"] = 55.44
	fmt.Println(balances)
	// map[EURO:34.55 GBP:55.44 KSH:145.99 USD:25.55]

	// getting a value that does not exist it will return a value of zero for the type of value being searched for
	// float64 returns 0, string returns ""

	balances["RON"] = 0

	v, ok := balances["RON"]
	_ = v

	//ok will be true if the key RON exists
	// ok will be false if the key RON does not exist
	// v is the key in the map

	if ok {
		fmt.Println("The RON key exist in balances  and value is ", v)
	} else {
		fmt.Println("The RON key does not exist in balances")
	}

	// ITERATING THROUGH MAPS
	for k, v := range balances {
		fmt.Printf("Key %v - Value %v \n", k, v)
	}

	// DELETE KEY VALUE PAIRS
	// use delete(name_map,"Key")

	delete(balances, "USD")

	fmt.Println(balances)

}

/*
Map - is a collection type just like an array or slice and stores key value pairs
- main advantage of maps is that add, get, delete operations take constant expected time
- all the keys and values in a map are statically typed and must have the same type
- keys in maps must be unique but the values do not have to be unique
- maps allow us to quickly access a value using a unique key
- we can use any comparable type as a key map. Comparable type is the tiype that supports the comparing operators which is == sign
- even if it is possible, it is not recommended to use a float as key
- we cannot compare a map to another map. We can only compare a map to nil
- maps are unordered data structures in go
- maps are sort of dictionary in python or objects in javascript


*/
