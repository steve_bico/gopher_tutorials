package main

import "fmt"

func main() {
	fmt.Println("This is comparing maps")

	// maps can be compared only to nil

	a := map[string]string{"A": "X"}
	b := map[string]string{"B": "Y"}

	_ = a
	_ = b

	//fmt.Println(a == b)
	// returns invalid operation maps can only be compared to nil

	// maps can be compared with keys of type string and values of type string
	// Sprintf changes the values to string and compare them

	s1 := fmt.Sprintf("%s", a)
	s2 := fmt.Sprintf("%s", b)

	if s1 == s2 {
		fmt.Println("The maps are equal")
	} else {
		fmt.Println("Maps are not equal")
	}
}
