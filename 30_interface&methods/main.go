package main

import (
	"fmt"

	"github.com/bicosteve/interface/pkg/entities"
)

func main() {

	products := []*entities.Product{
		{"Kayak", "Watersports", 275},
		{"Life Jacket", "Watersports", 48.95},
		{"Soccer Ball", "Soccer", 19.50},
	}

	for _, p := range products {
		// fmt.Println("Name-", p.Name, "Category-", p.Category, "Price-", p.Price)
		entities.PrintDetails(p)
	}

	productsTwo := []*entities.Product{
		entities.NewProduct("Racket", "Badminto", 45.55),
		entities.NewProduct("Hockey stick", "Hockey", 55.55),
		entities.NewProduct("Gloves", "Bike Racing", 5.55),
	}

	fmt.Println("+++++++++++++++++++++++++++++++++++++++")

	for _, p := range productsTwo {
		p.ShowDetails()
	}

	var product entities.Product

	product.Name = "Bike"
	product.Category = "Bike Racing"
	product.Price = 105.55

	fmt.Println("+++++++++++++++++++++++++++++++++++++++")

	product.ShowDetails()

	costume := entities.NewProduct("Costume", "Swimming", 555.55)
	costumTwo := entities.NewProduct("Goggles", "Swimming", 45.55)

	fmt.Println("+++++++++++++++++++++++++++++++++++++++ Costume")
	costume.ShowDetails()
	fmt.Printf("The tax for %s is %.2f\n", costume.Name, costume.CalculateTax(0.15, 5))

	fmt.Println("+++++++++++++++++++++++++++++++++++++++ Costume Two")

	costumTwo.ShowDetails()
	fmt.Printf("The tax for %s is %.2f\n", costumTwo.Name, costumTwo.CalculateTax(0.15, 15))

	// Using interface
	fmt.Println("+++++++++++++++++++++++++++++++++++++++ Interface")

	expenses := []entities.Expense{
		// Can take implementing types as values
		// since CalculateTax is a pointer receiver, use &entities.Product,&entities.Service
		&entities.Product{"Kayak", "Watersports", 275},
		&entities.Service{"Boat Cover", 12, 475},
	}

	for _, e := range expenses {
		fmt.Println("Expense ", e.GetName(), "Cost ", e.GetCost(true))
	}

	// Using struct type as instead of interface
	fmt.Println("Using struct type in place of interface")
	fmt.Println(getBill(costume))
	fmt.Println(getBill(costumTwo))

}

func getBill(e entities.Expense) float64 {
	return e.CalculateTax(0.11, 20)
}
