package entities

import "fmt"

type Service struct {
	Description string
	Duration    int
	MonthlyFee  float64
}

// Methods
// PrintDetails receives a Product pointer which it uses to write out the values
// name, category and price fields.
func PrintDetail(s *Service) {
	fmt.Println("Name: ", s.Description, "Category ", s.Duration, "Price ", s.MonthlyFee)
}

// Receiver function

// Constructor-ish sort of
func NewService(description string, duration int, fee float64) *Service {
	return &Service{description, duration, fee}
}

// Receiver function.
// ShowDetails is a receiver of type Product
// the reciever is the a parameter which is a type the function/method operates on
func (s *Service) ShowDetails() {
	fmt.Println("Desc:", s.Description, "Duration:", s.Duration, "Fee:", s.MonthlyFee)

}

// Methods with parameters
func (s *Service) CalculateTax(rate, threshold float64) float64 {
	if s.MonthlyFee > threshold {
		return s.MonthlyFee + (s.MonthlyFee * rate)
	}

	return s.MonthlyFee
}

func (s Service) GetName() string {
	return s.Description
}

func (s Service) GetCost(recur bool) float64 {
	if recur {
		return s.MonthlyFee * float64(s.Duration)
	}

	return s.MonthlyFee
}
