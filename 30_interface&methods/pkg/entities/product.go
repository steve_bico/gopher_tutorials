package entities

import "fmt"

type Product struct {
	Name     string
	Category string
	Price    float64
}

// Methods
// PrintDetails receives a Product pointer which it uses to write out the values
// name, category and price fields.
func PrintDetails(p *Product) {
	fmt.Println("Name: ", p.Name, "Category ", p.Category, "Price ", p.Price)
}

// Receiver function

// Constructor-ish sort of
func NewProduct(name, category string, price float64) *Product {
	return &Product{name, category, price}
}

// Receiver function.
// ShowDetails is a receiver of type Product
// the reciever is the a parameter which is a type the function/method operates on
func (p *Product) ShowDetails() {
	fmt.Println("Name: ", p.Name, "Category ", p.Category, "Price ", p.Price)

}

// Methods with parameters
func (p *Product) CalculateTax(rate, threshold float64) float64 {
	if p.Price > threshold {
		return p.Price + (p.Price * rate)
	}

	return p.Price
}

func (p Product) GetName() string {
	return p.Name
}

func (p Product) GetCost(_ bool) float64 {
	return p.Price
}
