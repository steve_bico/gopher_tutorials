package entities

/*
Interface is set of mthods without specifying the implementation of the methods
If a type implements ALL the methods defined in the interface then the value
of that type can be used wherever the interface is permitted

NB: methods on interface contain name, parameters and return type
NB: For a type to implement an interface, it must implement all the methods specified in the interface
*/

type Expense interface {
	GetName() string
	GetCost(annual bool) float64
	CalculateTax(rate, threshold float64) float64
	ShowDetails()
}
