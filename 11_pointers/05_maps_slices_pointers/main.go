package main

import "fmt"

func main() {
	fmt.Println("This section is for maps, slices")
	quan, price, name, sold := 5, 7.5, "Book", true

	fmt.Println("BEFORE calling change variables ", quan, price, name, sold)
	changeValues(quan, price, name, sold)
	fmt.Println("AFTER calling change variables ", quan, price, name, sold)
	// values did not change
	fmt.Println("###########################")
	fmt.Println("BEFORE calling changeValuesByPointer() ", quan, price, name, sold)
	changeValuesByPointer(&quan, &price, &name, &sold)
	fmt.Println("AFTER calling changeValuesByPointer() ", quan, price, name, sold)

	gift := Product{price: 55.5, name: "Alarm"}

	fmt.Println("BEFORE calling changeProductByPointer() ", gift)

	changeProductByPointer(&gift)

	fmt.Println("AFTER calling changeProductByPointer() ", gift)

	// slices
	prices := []int{1, 2, 3}
	changeSlice(prices)
	fmt.Println("Prices after calling changeSlice()", prices)
	// increments the prices by 1 each
	// slice already has a pointer to its packing array and we do not need pointers to manipulate its values

	myMap := map[string]int{"A": 100, "B": 300}
	changeMap(myMap)
	fmt.Println("The map values after calling changeMap()", myMap)
	// the values in the map were not changed

	//NB: slices and maps are not meant to be used by pointers

}

func changeValues(quan int, price float64, name string, sold bool) {
	quan = 5
	price = 6.0
	name = "Beans"
	sold = false
}

func changeValuesByPointer(quan *int, price *float64, name *string, sold *bool) {
	// use dereferencing operator
	// modifies the value pointers point to
	// * before a type eg * int means a type declaration * int means a pointer to int
	// * before a variable means the value stored at the address of the pointer variable
	*quan = 5
	*price = 7.0
	*name = "Kitabu"
	*sold = false

}

// struct to functions

type Product struct {
	price float64
	name  string
}

// functin that takes a struct and modifies it

func changeProductByPointer(p *Product) {
	(*p).price = 400.0 // p.price = 300
	p.name = "Bicycle" // (*p).name  same thing
}

// slice
func changeSlice(s []int) {
	// for _, v := range s {
	// 	// v += 1
	// 	// fmt.Println(v)
	// 	v += 1
	// 	fmt.Println(v)
	// }

	for i := 0; i < len(s); i++ {
		s[i] += 1
	}
}

//map

func changeMap(m map[string]int) {
	m["A"] = 10
	m["B"] = 20
	m["C"] = 30

}

// It is not good practise to pass arrays in functions
