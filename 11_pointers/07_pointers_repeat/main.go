package main

import "fmt"

func main() {
	// Pointers -> are data types and stored in their own memory block and reference other variables
	// Pointers are used for to change original value and avoid reference by values

	name := "Tifa"

	myNamePtr := &name // stores the memory location

	// De-referencing pointer to get value
	fmt.Println("Value at memory address is ", *myNamePtr)

	updateName(&name)

	fmt.Println(name)
	// Will be changed to Wedge
}

func updateName(n *string) {
	*n = "Wedge"
}
