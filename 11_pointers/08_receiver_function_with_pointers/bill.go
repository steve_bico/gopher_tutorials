package main

import (
	"fmt"
	"os"
)

type Bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// This is a blue print of a Bill
// It is a custom type of a bill and all the bills will take this structure
// It is sort of object in OOP

// Make new bill function
func newBill(name string) Bill {
	var billOne = Bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	_ = billOne

	bill := Bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	return bill
}

// Receiver function
// func (bill Bill) -> is the receiver function of Bill and is limited to only Bill type
func (bill Bill) formatBill() string {
	formatedString := "Bill breakdown \n"
	total := 0.0

	// List items
	for key, value := range bill.items {
		formatedString += fmt.Sprintf(" %v ... $%v \n", key+":", value)
		total += value
	}

	// Adding totals
	formatedString += fmt.Sprintf(" %v .... $%0.2f", " total: ", total)

	// Adding tips
	formatedString += fmt.Sprintf(" %v ... $%v\n", "tip:", bill.tip)

	return formatedString
}

// Update the tip
func (b *Bill) updateTip(tip float64) {
	b.tip = tip
}

// Add item in the items map
func (b *Bill) addItem(name string, price float64) {
	b.items[name] = price
}

// NB: Whenever we are updating values, the type on receiver function should be a pointer
// NB: This will therefore update the items since go will automatically dereference the pointer
// NB: Pointers conserve the memory of computer compared to passing values
// NB: For structs, pointers are automatically de-referenced

// save bill
func (b *Bill) save() {
	data := []byte(b.formatBill())
	err := os.WriteFile("bill/"+b.name+".txt", data, 0644)

	if err != nil {
		panic(err)
	}

	fmt.Println("Your bill was saved to file")
}
