package main

import "fmt"

func main() {
	x := 9
	p := &x

	_ = p

	fmt.Println("Value of x before calling change()", x)

	change(p)

	fmt.Println("Value of x after calling change()", x)

	fmt.Println("##############")

	fmt.Println("Value of x before calling changeVar()", x)
	changeVar(x)
	fmt.Println("Value of x after calling changeVar()", x)
	// x is 100 for both

}

func change(a *int) *float64 {
	*a = 100

	b := 8.8

	return &b
}

func changeVar(a int) {
	a = 66
}
