package main

import "fmt"

func main() {
	fmt.Println("Computer Memory")
	/*
		- RAM can be sequence of boxes or cells placed one after another in a line.
		- each cell is labelled with unique number which increments sequentially
		- this number is the address of the cell or memory location
		- the numbers are in hexadecimal representation
		- each cell holds a single value
		- if you know the address of the cell, you can store or retrieve value from it
		- variable is convinient, alphanumeric label for memory location
		-

		Pointer: -
		- Pointer is a variable that stores the memory address of another variable
		- pointer 'points' to the memory address of a variable just as variable represent memory address of a value
		- pointer value is the address of a variable and nil if it has not been utilized.


	*/
}
