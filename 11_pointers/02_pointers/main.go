package main

import "fmt"

func main() {
	// declaring pointers
	// use & - address of operator
	// used in front of a variable and returns the memory address of the variable
	//

	name := "Bix"

	fmt.Println(&name)
	// prints the memory 0xc0000341d0
	//

	// initializing pointers

	var x int = 2
	ptr := &x
	// &x address
	// ptr calls the address of x
	fmt.Printf("Ptr is of type %T with a value of %v\n", ptr, ptr)
	fmt.Printf("The address of x is %p\n", &x)
	// prints the address of x in memory
	// the pointer is also stored in memory
	// can use &ptr
	//

	// Delcaring pointer without initializing it

	var ptr1 *float64
	// pointer declared to float64 and zero value is nil
	// * in front of type means type description
	//
	_ = ptr1

	// Use new function to declare pointer
	p := new(int)
	// pointer called p of int type

	x = 100
	p = &x

	// p has been initialized with the address of x
	//

	fmt.Printf("P is of type %T with value of %v\n", p, p)

	fmt.Printf("Address of x is %p\n", &x)
	// returns the same address as that of p
	//

	// Dereference Operator
	// use *
	// placed before a pointer variable
	// eg *a returns the value

	*p = 90 // set the value of x to 90
	fmt.Println(x, *p)
	// returns 90,90

	// MORE EXAMPLE
	*p = 10
	// x has been made to 10
	*p = *p / 2
	fmt.Println(x)
	// value is 5

	// &value => pointer
	// *pointer => value

	b := 90
	xy := &b

	fmt.Println(*xy)
	// xy - 90

	cd := "James"

	username := &cd

	fmt.Println(*username)
	// returns James

	*username = "Mike"
	fmt.Println(cd)
	// returns Mike

	cd = "Steve"
	fmt.Println(*username)
	// returns Steve

}
