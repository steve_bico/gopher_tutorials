package main

import "fmt"

func main() {
	a := 5.5
	p1 := &a

	b := 8.5
	p2 := &b

	pp1 := &p1
	pp2 := &p2
	_ = pp2

	fmt.Printf("The value of p1 %v, address of p1 %v\n", p1, &p1)
	fmt.Printf("The value of pp1 is %v, address of pp1 is %v\n", pp1, &pp1)

	// Dereferencing Pointer to Pointer
	fmt.Printf("*P1 is %v\n", *p1)
	fmt.Printf("*Pp1 is %v\n", *pp1)

	// to get the value stored in the address pp1 use **
	fmt.Printf("**Pp1 is %v\n", **pp1)
	// returns 5.5 the value of a

	// Manipulating the value of a with pp1

	**pp1++

	fmt.Println(a)
	// returns 6.5

	// Comparing Pointers
	// pointers are equal if they point to the same variable or if they are nil

	var p3 *int
	fmt.Printf("%#v\n", p3)
	// returns nil

	y := 5
	p4 := &y

	_ = p4

	z := 5

	p5 := &z

	_ = p5

	fmt.Println(p4 == p5)
	// returns false. Even though y and z are equal, their address are different
	//

}
