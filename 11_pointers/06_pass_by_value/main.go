package main

import "fmt"

func main() {
	name := "Tifa"

	updateName(name)

	fmt.Println(name)
	// The name here remains "Tifa"
	// This is because, when name in updateName func is changed, a new copy of name is specifically created for
	// the function in the memory while the original name := "Tifa" is not touched.
	// the new copy is what will be changed to "Wedge"

	//NB: To change the actual value with the function, have a return type specified
	// Reassign the original variable to the value returned by the function

	menu := map[string]float64{
		"pie":       67.55,
		"ice cream": 45.55,
	}

	updateMenu(menu)

	fmt.Println(menu)
	// Will add coffee as part of the items in the menu
	// It will change the original value
	// 1. Stores the underlying data on its own memory address here menu
	// 2. stores the data in the menu to other underlying blocks which is associated with the original memory block
	// NB: The variables are spread into multiple memory locations.
	// 3.
	// This happens to slices, maps, and functions
}

func updateName(s string) {
	s = "Wedge"
}

func updateMenu(y map[string]float64) {
	y["coffee"] = 25.55
}
