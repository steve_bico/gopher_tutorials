package main

import "fmt"

func main() {
	age := 40 // regular variable

	ageptr := &age
	// pointer variable -> cab also ageptr *int -> ageptr is a pointer to an integer

	fmt.Println("Your age is ", age)
	fmt.Println("The address to age is ", ageptr)
	fmt.Println("The value to age at the addres is ", *ageptr) // dereferencing

	/*
		Your age is  40
		The address to age is  0x140000a6018
		The value to age at the addres is  40
	*/

	//fmt.Println("Your adult life has been ", getAddYears(age))
	fmt.Println("Your adult years have been ", getAdultYears(&age, 18))
	fmt.Println("Your adjusted age is ", changeAge(&age, 20))
}

// func getAddYears(age int) int {
// 	return age - 18
// }

func getAdultYears(age *int, adulthood int) int {
	// get the value of age by dereferencing the age pointer then deduct 18.
	// in this case, no copy will be created when the function is running.
	// It will directly change the value of age at the 0x140000a6018 address.
	return *age - adulthood
}

// Directly mutating values
func changeAge(age *int, by int) int {
	*age = *age + by
	return *age
}
