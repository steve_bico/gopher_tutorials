package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/bixoloo/pkg/configs"
	"bitbucket.org/bixoloo/pkg/routes"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatalf("Cannot load .env because of %v", err)
	}

	port := os.Getenv("PRODUCERPORT")
	kafkaBroker := os.Getenv("BROKER")

	err = configs.SetUpKafka(kafkaBroker)
	if err != nil {
		log.Fatalf("Cannot load producer because of %v", err)
	}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: routes.Routes(),
	}

	log.Printf("Producer listening to port %v", port)

	server.ListenAndServe()

}
