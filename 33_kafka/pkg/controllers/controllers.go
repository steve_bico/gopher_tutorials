package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/bixoloo/pkg/consumer"
	"bitbucket.org/bixoloo/pkg/entities"
	"bitbucket.org/bixoloo/pkg/producer"
	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/joho/godotenv"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var payload entities.UserPayload

	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatalf("Cannot load .env because of %v", err)
	}

	topic := os.Getenv("TOPIC")
	broker := os.Getenv("BROKER")

	r.Body = http.MaxBytesReader(w, r.Body, int64(1048576))
	decode := json.NewDecoder(r.Body)
	err = decode.Decode(&payload)
	if err != nil {
		http.Error(w, "Invalid payload", http.StatusBadRequest)
		return
	}

	if payload.Message == "" {
		http.Error(w, "Payload cannot be empty", http.StatusBadRequest)
		return
	}

	notification := entities.Notification{
		From:    payload.From,
		To:      payload.To,
		Message: payload.Message,
	}

	notificationJSON, err := json.Marshal(notification)
	if err != nil {
		http.Error(w, "failed to marshal notification payload", http.StatusInternalServerError)
		return
	}

	notificationString := string(notificationJSON)

	err = producer.SendKafkaMessage(broker, topic, notificationString)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	fmt.Fprintf(w, "Payload is %+v", payload)

}

func Consume(w http.ResponseWriter, r *http.Request) {
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatalf("Cannot load .env because of %v", err)
	}

	topic := os.Getenv("TOPIC")
	broker := os.Getenv("BROKER")
	groupId := os.Getenv("GROUP")
	offset := os.Getenv("OFFSET")

	c, err := consumer.KafkaConsumer(broker, groupId, offset, topic)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer c.Close()

	// run := true

	client := &http.Client{}

	for {
		// msg, err := c.ReadMessage(time.Second)
		msg, err := c.ReadMessage(-1)
		if err == nil {
			fmt.Printf("Received message from: %s\n", string(msg.Value))
			// Can send to db here, post to another service etc
			data := []byte(msg.Value)
			req, err := http.NewRequest("POST", "http:example.com/endpont", bytes.NewBuffer(data))
			if err != nil {
				log.Printf("failed to create request because of %v", err)
				continue
			}

			res, err := client.Do(req)
			if err != nil {
				log.Printf("failed to send HTTP because of %v", err)
				continue
			}

			res.Body.Close()

		} else if !err.(kafka.Error).IsTimeout() {
			//w.Write([]byte(err.Error()))
			log.Printf("failed because of %s", err.Error())

		} else {
			log.Printf("consumer error: %v (%v)\n", err, msg)
		}
	}

}
