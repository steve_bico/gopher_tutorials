package configs

import (
	"errors"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func SetUpKafka(kafkaServer string) error {

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": kafkaServer,
		// "sasl.mechanisms":   "PLAIN",
		// "security.protocol": "SASL_SSL",
		"acks": "all",
	})

	if err != nil {
		return errors.New(err.Error())
	}

	defer p.Close()

	return nil
}
