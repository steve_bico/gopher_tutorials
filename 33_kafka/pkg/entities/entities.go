package entities

type User struct {
	ID   int    `json:"from"`
	Name string `json:"name"`
}

type Notification struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Message string `json:"message"`
}

type UserPayload struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Message string `json:"message"`
}

type UserNotifications map[string][]Notification
