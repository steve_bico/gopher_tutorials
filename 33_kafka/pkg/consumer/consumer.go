package consumer

import (
	"errors"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func KafkaConsumer(broker, groupId, offset, topic string) (*kafka.Consumer, error) {
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": broker,
		"group.id":          groupId,
		"auto.offset.reset": offset,
	})

	if err != nil {
		return nil, errors.New(err.Error())
	}

	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		return nil, errors.New(err.Error())
	}

	return c, nil
}
