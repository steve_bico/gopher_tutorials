package models

import (
	"errors"

	"bitbucket.org/bixoloo/pkg/entities"
)

func FindUserByID(id int, users []entities.User) (entities.User, error) {
	for _, user := range users {
		if user.ID == id {
			return user, nil
		}
	}

	return entities.User{}, errors.New("user not found")
}
