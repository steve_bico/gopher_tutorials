package main

import "fmt"

func maps() {
	fmt.Println("This is maps")
	// maps are used to group data together
	// map is data structure used to group data together

	// 1. Creating a map
	websites := map[string]string{"Google": "https://google.com"}
	//[string]string key - value of type string
	// keys can be of any type
	// value can be of any type
	fmt.Println(websites)

	// 2. Mutating maps
	// a) Accessing the keys in a map
	fmt.Println(websites["Google"])

	// b) Adding keys and values
	// target a key that does not exist in the map
	websites["AWS"] = "http://aws.com"
	websites["LinkedIn"] = "https://linkedin.com"

	// c) Delete key
	// target map and the key to delete
	delete(websites, "AWS")

	// 3. Maps vs Structs
	// in maps, any type can be used as a key
	// in struct, key value pairs cannot be added
	// struct are used to describe data entities not to hold data entities

	// 4. make for maps
	courseRating := make(map[string]float64, 5)
	// this means the intended length of this map

	courseRating["Go"] = 6.7
	courseRating["React"] = 10.55

	// 5. Looping maps
	// for key, value := range map{}
	for _, site := range websites {
		fmt.Println(site)
	}
}
