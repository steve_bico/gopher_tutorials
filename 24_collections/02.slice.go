package main

import "fmt"

func slice() {
	prices := [4]float64{10.99, 9.85, 45.99, 20.0}

	//1.  Slice is used to get a part of an array

	myslice := prices[0:3] // from index 0 to 3 excluding index 3
	fmt.Println(myslice)

	//2.  Slice len
	fmt.Println(len(myslice))
	// getting the length of the slice
	// shows number of items in an array

	//3.  Cap - used to get capacity of an array or slice
	fmt.Println(cap(myslice))
	// returns a number showing the elements that can be held in the slice
	// shows the number of item currently held in the slice

	//4. Adding items to slices
	updatedList := append(myslice, 55.5)
	// adds the new items at the end
	// returns a new slice
	fmt.Println(updatedList)

	// 5. Unpacking arrays and slices
	// you can append as many values as you want.
	//

	uppendedSlice := append(updatedList, myslice...)
	fmt.Println(uppendedSlice)
	// also called spread operator in javascript

	// 6. Make key word
	usernames := make([]string, 3, 5)
	// go assigns 3 empty slots in the background for the array
	// go assigns capacity of 5 and this is maximum amount of elements in the array
	// this reserves enough space for the slice
	// slices are just arrays behind the scene
	fmt.Println(usernames)

	usernames = append(usernames, "Max")
	// will add "Max" at the end of usernames but the 3 slots will still be reserved
	//

}
