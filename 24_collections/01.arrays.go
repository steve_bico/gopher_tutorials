package main

import "fmt"

func main() {
	// 1. Creating an array # 1
	prices := [4]float64{7.6, 9.0, 5.5, 7.3}
	fmt.Println(prices)
	// prints a list of the prices

	// Creating an array # 2
	var productNames [5]string
	fmt.Println(productNames) // returns empty array

}
