package main

import (
	"fmt"
	"time"
)

func main() {
	// Concurrency -> running tasks in parallel
	// Normally functions execute after each other
	// run the functions in parallel by adding go keyword before each functiongp

	go greet("James")
	go greet("How are you")
	go greet("Nice to meet you ")

	go greetSlow("How ... are ... you ")

	go greet("I hope you are liking the course")

	// Nothing will show on the console because the functions are non blocking
	// these functions have been dispatch and then we are done
	//  this is the very definition of goroutine
	// main function just dispatch the goroutines the the program exits
	// these programs are faster and that is why we do not see anything on the console
	//
}

func greet(phrase string) {
	fmt.Println("Hello ", phrase)
}

func greetSlow(phrase string) {
	time.Sleep(3 * time.Second) // simulate slow
	fmt.Println("Hello !", phrase)
}
