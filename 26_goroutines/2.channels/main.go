package main

import (
	"fmt"
	"time"
)

func main() {
	// Concurrency -> running tasks in parallel
	// Normally functions execute after each other
	// run the functions in parallel by adding go keyword before each functiongp

	done := make(chan bool)

	go greet("James", done)
	go greet("How are you", done)
	go greet("Nice to meet you ", done)
	go greetSlow("How ... are ... you ", done)
	go greet("I hope you are liking the course", done)

	<-done
	<-done
	<-done
	<-done
	<-done

	// Nothing will show on the console because the functions are non blocking
	// these functions have been dispatch and then we are done
	//  this is the very definition of goroutine
	// main function just dispatch the goroutines the the program exits
	// these programs are faster and that is why we do not see anything on the console
	// To solve this problem you can use channels
	// Channel is a communication way when working with go routine
	// to create a channel use 'make' key word followed by 'chan'
	// channels transmit some data i.e a communication device
	// should also add the type of channel which can be int,bool, string, slice etc
	// this will show us whether the operation is done or not
	// pass the channel to the function you intend to run as a goroutine
	// one channel can be used with multiple goroutines
	// if you use one channel for multiple goroutines, you have to wait for the number of multiple goroutines to finish.
	// they will also not finish in order
}

func greet(phrase string, done chan bool) {
	fmt.Println("Hello ", phrase)
	done <- true
}

func greetSlow(phrase string, done chan bool) {
	time.Sleep(3 * time.Second) // simulate slow
	fmt.Println("Hello !", phrase)

	// use the created channel in this case 'done'
	// this will send data to the position where this function is called
	// it will alert the other function is the operation in this function is done

	done <- true

	// the arrow point to the direction data should flow
	// here it means if the done is true, the function is ready
}
