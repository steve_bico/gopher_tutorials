package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

// WAIT GROUPS
// blocks the main function from terminating till all the goroutine functions have been executed.
//

func main() {

	//WAIT GROUP
	var wg sync.WaitGroup

	wg.Add(1)
	// 1 is then number of goroutine to wait for
	// function should use wait group as a parameter and must be passed as pointer

	go f1(&wg)

	fmt.Println("Number of f1() goroutines ", runtime.NumGoroutine())

	f2()

	wg.Wait() //blocks main function execution till the  gouritine function has been executed
	fmt.Println("Number of f2 goroutines ", runtime.NumGoroutine())
}

// functions
func f1(wg *sync.WaitGroup) {
	fmt.Println("F1 starts ")

	for i := 0; i < 3; i++ {
		fmt.Printf("F1 ,i = %v\n", i)
		time.Sleep(time.Second)
	}

	fmt.Println("F1 is exiting ")
	// wg.Done is called in any go routine to indicate to the wait group that a goroutine has finished excuting
	//
	wg.Done()
}

func f2() {
	fmt.Println("F2 starts ")

	for i := 5; i < 8; i++ {
		fmt.Printf("F2 ,i = %v\n", i)
	}

	fmt.Println("F2 is exiting ")
}
