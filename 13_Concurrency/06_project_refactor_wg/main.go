package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
)

func main() {

	urls := []string{"https://golang.org", "https://betika.com", "https://medium.com"}

	var wg sync.WaitGroup

	//  the len(urls) is the number of goroutines to wait for
	wg.Add(len(urls))

	for _, url := range urls {
		go checkAndSaveBody(url, &wg)
		fmt.Println(strings.Repeat("#", 10))
	}

	wg.Wait()
	// blocks the execution of main() till all goroutines are completed

}

func checkAndSaveBody(url string, wg *sync.WaitGroup) {
	res, err := http.Get(url)

	if err != nil {
		fmt.Println(err)
		fmt.Printf("%s is DOWN \n", url)
		log.Fatal(err)
	}

	defer res.Body.Close()
	fmt.Printf("%s => Status Code %d \n", url, res.StatusCode)

	if res.StatusCode == 200 {

		bodyBytes, err := ioutil.ReadAll(res.Body)
		file := strings.Split(url, "//")[1]

		file += ".txt"

		fmt.Printf("Writing response body to %s\n", file)

		err = ioutil.WriteFile(file, bodyBytes, 0664)

		if err != nil {
			log.Fatal(err)
		}

	}

	wg.Done()

}
