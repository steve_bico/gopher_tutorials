package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func main() {

	urls := []string{"https://golang.org", "https://betika.com", "https://medium.com"}

	for _, url := range urls {
		checkAndSaveBody(url)
		fmt.Println(strings.Repeat("#", 10))
	}

}

func checkAndSaveBody(url string) {
	res, err := http.Get(url)

	if err != nil {
		fmt.Println(err)
		fmt.Printf("%s is DOWN \n", url)
		log.Fatal(err)
	}

	defer res.Body.Close()
	fmt.Printf("%s => Status Code %d \n", url, res.StatusCode)

	if res.StatusCode == 200 {

		bodyBytes, err := ioutil.ReadAll(res.Body)
		file := strings.Split(url, "//")[1] // http://www.google.com -> splits on // and takes the second element.  This is slice

		// append .txt at the end of the file name www.google.com

		file += ".txt"

		fmt.Printf("Writing response body to %s\n", file)

		err = ioutil.WriteFile(file, bodyBytes, 0664)

		if err != nil {
			log.Fatal(err)
		}

	}

}
