package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int, 3) // buffered channel. Takes an optional argument to channel

	go func(c chan int) {
		for i := 1; i <= 5; i++ {
			fmt.Printf("func goroutine #%d starts sending data into the channel \n", i)
			c <- i
			fmt.Printf("func gourtine #%d after sending data into the channel \n", i)
		}
		close(c)
		// not necessary to close the channel
		// only necessary to close channel to indicate all the data has been sent
	}(c)

	fmt.Println("main goroutines sleeps for 2 seconds")
	time.Sleep(time.Second * 2)

	fmt.Println("goroutine starts reciving data")
	// iterate over channel to get values received from the channel
	for v := range c { // v := <- c
		fmt.Println("main goroutine received value from channel ", v)
	}
	// d := <-c

	// fmt.Println("Value received from channel is ", d)

	// time.Sleep(time.Second * 2)
}
