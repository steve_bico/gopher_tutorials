package main

import "fmt"

func main() {
	c := make(chan int) // unidirectional channel

	cOne := make(<-chan string) // only for receiving value of type string

	cTwo := make(chan<- string) // only for sending string data values

	fmt.Printf("%T, %T, %T\n", c, cOne, cTwo)

	go sendValueToChann(10, c)

	// receive data from channel c
	n := <-c

	// Print the received value
	fmt.Println("The received value from channel of int is ", n)

	cThree := make(chan string)

	go sendStringChan("This is test", cThree)
	// receive data from chanThree
	m := <-cThree
	fmt.Println("The received value from channel of string is ", m)

	fmt.Println("The main func has exited ... ")

}

func sendValueToChann(m int, ch chan int) {
	// sending value m to channel ch
	ch <- m
}

func sendStringChan(m string, ch chan string) {
	ch <- m
}
