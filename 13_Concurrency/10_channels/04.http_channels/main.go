package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime"
	"strings"
)

func main() {
	urls := []string{"https://golang.org", "https://betika.com", "https://medium.com"}

	// 1. Make channel
	c := make(chan string)

	for _, url := range urls {
		go checkAndSaveBody(url, c)
		fmt.Println(strings.Repeat("#", 10))
	}

	fmt.Println("Number of goroutines", runtime.NumGoroutine())

	// Receive message from channel in main goroutine
	for i := 0; i < len(urls); i++ {
		fmt.Println(<-c) // print value received from the channel
	}
}

func checkAndSaveBody(url string, c chan string) {
	res, err := http.Get(url)

	if err != nil {
		errorString := fmt.Sprintf("%s is DOWN \n", url)
		errorString += fmt.Sprintf("Error:  %v \n", err)

		c <- errorString // sending error into the channel

	}

	defer res.Body.Close()
	s := fmt.Sprintf("%s => Status Code %d \n", url, res.StatusCode)

	if res.StatusCode == 200 {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		file := strings.Split(url, "//")[1]
		// http://www.google.com -> splits on // and takes the second element.  This is slice
		// append .txt at the end of the file name www.google.com

		file += ".txt"

		s += fmt.Sprintf("Writing response body to %s\n", file)

		err = ioutil.WriteFile(file, bodyBytes, 0664)

		if err != nil {
			s += "Error writing file"
			c <- s
		}

		s += fmt.Sprintf("%s is UP\n", url)

		c <- s

	}

}
