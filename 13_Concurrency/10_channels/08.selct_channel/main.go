package main

import (
	"fmt"
	"time"
)

func main() {
	cOne := make(chan string)
	cTwo := make(chan string)

	start := time.Now().UnixNano() / 1000000

	go func() {
		time.Sleep(2 * time.Second)
		cOne <- "Hello"
	}()

	go func() {
		time.Sleep(2 * time.Second)
		cTwo <- "Salut!"
	}()

	for i := 0; i < 2; i++ {
		// select is like a switch but only used by channels
		// select statement is used when you want
		// to wait for multiple goroutines simultenously
		select {
		case msg1 := <-cOne:
			fmt.Println("Received ", msg1)
		case msg2 := <-cTwo:
			fmt.Println("Received ", msg2)
		default:
			fmt.Println("No activity")
		}
	}

	end := time.Now().UnixNano() / 1000000

	fmt.Println(end - start) // returns the time taken to run the channels
}
