package main

import "fmt"

func main() {
	// Channels

	/*
		- channels provide connection between two goroutine allowing them to communicate.
		- channels are used to communicate inbetween running goroutines.
		- data sent through or between two channels must be of the same type. ***
		- type is declared when a channel is declared.
		-
	*/

	var ch chan int
	// communicate with channel of type int
	// value of uninititialized channel is nil i.e its zero value
	fmt.Println(ch)

	ch = make(chan int)
	// initialized channel
	fmt.Println(ch)
	// returns an address since channel is like a pointer.
	// passing channels to functions is like passing pointers to functions

	// declaring and initializing new channel
	c := make(chan int)

	fmt.Println(c) // Returns a pointer

	// a channel is a two way messaging object.
	// has two operations send and receive.
	// `send` statement transmit messages from one goroutine to another goroutine executing corresponding received operation.
	// both both operations are written using the channel operator `<-`
	//

	// SEND
	c <- 10 // sends value 10 to c channel

	// RECEIVE
	num := <-c // receive a value from channel c to variable m

	fmt.Println(<-c)
	_ = num
	// channel operator <- indicates the direction of data flow.

	// CLOSE
	// indicates no more value will ever be sent to a channel
	close(c) // closes channel c

}
