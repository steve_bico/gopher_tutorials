package main

import (
	"fmt"
	"time"
)

func main() {
	c1 := make(chan int)    // unbuffered channel
	c2 := make(chan int, 3) // buffered channel
	_, _ = c1, c2

	go func(c chan int) {
		fmt.Println("func goroutines starts sending dat into the channel")
		c <- 10
		fmt.Println("func gourtine after sending dat into the channel")
	}(c1)

	fmt.Println("main goroutines sleeps for 2 seconds")

	time.Sleep(time.Second * 2)

	fmt.Println("goroutine starts reciving data")
	d := <-c1

	fmt.Println("Value received from channel is ", d)

	time.Sleep(time.Second * 2)
}
