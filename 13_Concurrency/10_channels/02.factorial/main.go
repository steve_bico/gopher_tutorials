package main

import "fmt"

func main() {
	ch := make(chan int)
	defer close(ch) // best practise to close channel before main exits.

	go factorial(5, ch)

	// main should wait for the message to come through the channel before it exits.
	//  the passing of message through the channel is a blocking call.
	// main() will go to sleep and wait for factorial() to send msg through the channel

	// message is coming from the channel ch to var f
	// this functionality is blocking in operation  and main will wait till it's done.
	f := <-ch // blocking operation

	// after main() receives the message, it wakes up and print the message then exits

	fmt.Println(f)

	for i := 1; i < 20; i++ {
		go factorial(i, ch)
		f := <-ch
		fmt.Println(f)
	}

}

func factorial(n int, c chan int) {
	f := 1
	for i := 2; i <= n; i++ {
		f *= i
	}

	// sending the value f to the channel
	c <- f

}
