package main

import (
	"fmt"
	"net/http"
	"runtime"
	"strings"
	"time"
)

func main() {
	urls := []string{"https://golang.org", "https://betika.com", "https://medium.com"}
	c := make(chan string)

	for _, url := range urls {
		go checkURL(url, c)
		fmt.Println(strings.Repeat("#", 10))
	}

	fmt.Println("Number of goroutines", runtime.NumGoroutine())

	// 1. Method One
	// for {
	// 	// infinite loop
	// 	go checkURL(<-c, c)
	// 	// <-c contains address received from previous goroutine
	// 	// it is the url value stored in channel c in line 39
	// 	fmt.Println(strings.Repeat("#", 20))

	// 	// check url for two seconds
	// 	time.Sleep(time.Second * 2)
	// }

	// 2. Method Two
	// iterating over channel like slice
	// infinitely
	// for url := range c {
	// 	time.Sleep(time.Second * 2)
	// 	go checkURL(url, c)
	// }

	// 3. Method Three
	for url := range c {
		go func(u string) {
			time.Sleep(time.Second * 2)
			checkURL(u, c)
		}(url)
	}
}

func checkURL(url string, c chan string) {
	res, err := http.Get(url)

	if err != nil {
		errorString := fmt.Sprintf("%s is DOWN \n", url)
		errorString += fmt.Sprintf("Error:  %v \n", err)

		fmt.Println(errorString)

		// send the url to the channel to be available for other goroutine
		c <- url
	}

	s := fmt.Sprintf("%s -> Status Code: %d \n", url, res.StatusCode)
	s += fmt.Sprintf("%s is UP\n", url)
	c <- s

}
