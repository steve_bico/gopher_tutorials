package main

import (
	"fmt"
	"runtime"
)

// functions
func f1() {
	fmt.Println("F1 starts ")

	for i := 0; i < 3; i++ {
		fmt.Printf("F1 ,i = %v\n", i)
	}

	fmt.Println("F1 is exiting ")
}

func f2() {
	fmt.Println("F2 starts ")

	for i := 5; i < 8; i++ {
		fmt.Printf("F2 ,i = %v\n", i)
	}

	fmt.Println("F2 is exiting ")
}

func main() {
	fmt.Println("Main execution started")
	fmt.Println("Number of CPUs is ", runtime.NumCPU())
	fmt.Println("Number of goroutines ", runtime.NumGoroutine())

	fmt.Println("The OS is ", runtime.GOOS)
	fmt.Println("Architecture ", runtime.GOARCH)

	// How many os threads can be executed simultenously
	// For 8 cpus, it will schedule 8 threads
	// maxproc is 8

	fmt.Println("Go maximum process ", runtime.GOMAXPROCS(0))

	go f1()
	//f1()
	fmt.Println("Number of f1() goroutines ", runtime.NumGoroutine())

	f2()

	fmt.Println("Number of f2 goroutines ", runtime.NumGoroutine())
}
