package main

import (
	"fmt"
	"time"
)

func main() {
	/*
		Goroutine is a light weight thread of execution. Are key ingredients to achieve concurrency in go
		Goroutine is a function that is capable of running concurrently with other functions. To create a goroutine, we use the keyword go
		followed by a function invocation.
		Goroutines are far smaller than threads, they take 2kb of stake space to initialize compared to a thread which takes a fixed size of 1-2Mb
		An OS Thread Stack is fixed size but a goroutine stack size shrinks and grows as needed.
		Scheduling a goroutine is much cheaper than scheduling a thread
		OS threads are scheduled by the OS kernel, but goroutinees are scheduled by its own Go Scheduler using a technique called m:n scheduling because
		it multiplexes (schedules) m goroutine on n OS threads
		Goroutines have no identity. There is no notion of identity that is accessible to the programmer.

	*/

	// Normal function provocation
	doSomething()

	// goroutine spawning
	// go keyword creates a goroutine
	go doSomething()

	time.Sleep(time.Second * 3)

}

func doSomething() {
	fmt.Println("Go do something....")
}
