package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	const gr = 100

	// 1. Use mutexe
	/*
		Mutex - mutual exclusion.
		- variable access is protected from access at the same time by two go methods at the same time.
		- programmer identifies the candidate for concurrent access and protects it by mutual exclusion.
		-
	*/

	var m sync.Mutex
	// two types defined on mutex type.
	// any code present between these two types will be called
	// by only one goroutine to avoid race conditon.
	// 1. Lock - `locks` access to variable till the unlock method is called.
	// 2. Unlock - ``

	var wg sync.WaitGroup

	wg.Add(gr * 2)

	var n int = 0

	//1.

	for i := 0; i < gr; i++ {
		go func() {
			time.Sleep(time.Second / 10)
			// 2. Create a lock till a goroutine finishes
			m.Lock()
			n++
			// 3. Unlock the variable after the increment is finished
			m.Unlock()
			wg.Done()
		}()

		go func() {
			time.Sleep(time.Second / 10)
			// 2. Create a lock till a goroutine finishes
			m.Lock()
			// defer m.Unlock() same thing
			n--
			// 3. Unlock the variable after the increment is finished
			m.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println("The final value of n is ", n)
	// value of n changes in the order of which go routine change
	// use -race to give data race
}
