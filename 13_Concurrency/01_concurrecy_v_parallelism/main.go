package main

func main() {
	// go has concurrency built in.
	// concurrency - means loading more goroutines at a time. If one goroutine blocks, another one is picked and started.
	// On single CPU you can run only concurrent applications but they are not parrallel.
	// PARALLELISM - means multiple goroutines executed at the same time.  It requires multiple CPUs
	// concurrency means executing processes or dealing with multiple thing at once, while parallelism is the simultaneous execution
	// of processes and require multiple core CPUs.

}
