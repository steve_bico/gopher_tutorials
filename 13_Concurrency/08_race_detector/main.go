package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	const gr = 100

	var wg sync.WaitGroup

	wg.Add(gr * 2)

	var n int = 0

	//1.

	for i := 0; i < gr; i++ {
		go func() {
			time.Sleep(time.Second / 10)
			n++
			wg.Done()
		}()

		go func() {
			time.Sleep(time.Second / 10)
			n--
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println("The final value of n is ", n)
	// value of n changes in the order of which go routine change
	// use -race to give data race
	// go run -race main.go -> will detect data race in any code
	// divided into 3 sections
	// 1. Warning for Data Race
	// 2. Simultenous write by another goroutine
	// 3. Describes the goroutine that caused the data race was created
}
