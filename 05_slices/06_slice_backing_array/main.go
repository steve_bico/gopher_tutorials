package main

import (
	"fmt"
	"unsafe"
)

func main() {
	fmt.Println("Backing array continues")

	s10 := []int{10, 20, 30, 40, 50}

	s11, s12 := s10[0:2], s10[1:3]

	s11[1] = 600 // modifies s11 and s12
	fmt.Println(s11)
	fmt.Println(s12)
	//[10 600]
	//[600 30]

	// backing arrays modifies even the original array
	// original and new arrays are still connected with backing array

	cars := []string{"Ford", "Honda", "Audi", "Range"}
	newCars := []string{}

	newCars = append(newCars, cars[0:2]...)
	// ... ellipsis act as spread operator and should never be forgotten

	cars[0] = "Nissan"

	fmt.Println(cars, newCars)
	// only cars is modified

	numbers := []int{10, 20, 30, 40, 50}
	newSlice := numbers[0:3]

	fmt.Println(len(newSlice), cap(newSlice))
	// len is 3 cap is 5
	// len()-the length of new slice
	// cap() the capacity of the newSlice in its backing array
	// slicing operation is cheaper than array operation

	// checking memory of array vs slice

	a := [5]int{1, 2, 3, 4, 5}
	b := []int{1, 2, 3, 4, 5}

	fmt.Printf("Array size in bytes is %d \n", unsafe.Sizeof(a))
	fmt.Printf("Slice size of bytes is %d \n", unsafe.Sizeof(b))

	// slice takes less memory of 24 vs arrays 40

}
