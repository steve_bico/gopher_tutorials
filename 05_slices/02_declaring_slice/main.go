package main

import "fmt"

func main() {
	fmt.Println("This is slice section")

	var cities []string
	fmt.Println("Cities is equal to nil: ", cities == nil)
	// returns true

	fmt.Printf("Cities %v \n", cities)
	// nil does not mean absence of value but no initialized value

	fmt.Println(len(cities))
	// returns 0

	numbers := []int{2, 4, 5, 6}

	fmt.Println(numbers)

	//

	nums := make([]int, 2) // make slice of type int and length of two items

	fmt.Println(nums)
	// returns [0,0]

	type names []string
	friends := names{"Dan", "Maria"}

	//getting item from the slice using index
	myFriend := friends[0]
	fmt.Printf("My friends name is %v \n", myFriend)

	// modify element using index

	friends[0] = "James"

	fmt.Println(friends)

	// iterating through a  slice

	for index, value := range numbers {
		fmt.Printf("Index %v - value %v \n", index, value)
	}

	//assigning slice to another slice

	var n []int
	n = numbers
	fmt.Println(n)
	// returns the value of n to be those in numbers.

}
