package main

import "fmt"

func main() {
	fmt.Println("This is expression section one")

	a := [5]int{1, 2, 3, 4, 5} //array
	//slicing array
	//a[start:stop] excluding stop element
	b := a[1:3]
	fmt.Println(b) //[2,3]
	// slicing an array returns slice not an array
	// []int

	s1 := []int{1, 2, 3, 4, 5, 6}
	s2 := s1[1:4]
	fmt.Println(s2)
	// returns slice of [2,3,4]

	//starting from an index to the end of the slice
	s3 := s1[2:]
	//s1[2:len(s1)]
	fmt.Println(s3)
	// [3 4 5 6]

	// missing start index to the specified index
	s4 := s1[:3]
	fmt.Println(s4)

	//[:] -> [0:len(s1)]

	//getting last element of a slice
	last := s1[len(s1)-1]
	fmt.Println(last)

	// APPENDING TO A SLICE
	s1 = append(s1, 100) //adds to the end of the slice

	fmt.Println(s1)

	s1 = append(s1[:4], 200)
	// add element after index 3 and discard the remaining elements

	fmt.Println(s1)

	// slice backing

	s10 := []int{10, 20, 30, 40, 50}

	s11, s12 := s10[0:2], s10[1:3]

	s11[1] = 600 // modifies s11 and s12
	fmt.Println(s11)
	fmt.Println(s12)
	//[10 600]
	//[600 30]

	// backing arrays modifies even the original array

}

/*
SLICE BACKING
-whenc creating a slice, behind the scenes Go creates a hidden array called Backing Array
-Backing array stores the element not arrays
-Go implements a slice as a data structure called SLICE HEADER

Slice Header contains 3 fields
1. the address of the backing array that is pointer
2. the length of the slice that is len() returns it
3. the capacity of the slice .cap() returns it

slice header is run time representation of a slice
a nil slice does have a backing array


*/
