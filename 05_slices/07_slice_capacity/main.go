package main

import "fmt"

func main() {
	var nums []int
	fmt.Printf("%#v\n", nums)
	fmt.Printf("Length of d %d and capacity %d \n", len(nums), cap(nums))

	nums = append(nums, 1, 2)
	// backing array creates new backing array is the capacity of slice is full
	// this is used to hold the excess element from the original defined slice
	//
	fmt.Printf("Length of d %d and capacity %d \n", len(nums), cap(nums))

	letters := []string{"A", "B", "C", "D", "E", "F"}

	letters = append(letters[:1], "X", "Y")
	// taking from A then appending X,Y
	// returns A, X, Y
	fmt.Printf("Length of d %d and capacity %d \n", len(letters), cap(letters))
	fmt.Println(letters)

	fmt.Println(letters[3:6])
	//returns D,E,F because slices see the backing array
}
