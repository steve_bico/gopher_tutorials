package main

import "fmt"

func main() {
	fmt.Println("Comparing slices")

	var n []int

	fmt.Println(n == nil) //return true

	m := []int{}

	fmt.Println(m == nil) // return false

	// slices cannot be compared using == operator.  Can only be compared to nil

	a, b := []int{1, 2, 3}, []int{1, 2, 3}

	//fmt.Println(a == b) // returns slice can only be compared to nil

	// use for loop to iterate over slice and compare each element
	// can only use for loop to compare the slices

	eq := true

	for i, valueA := range a {
		if valueA != b[i] {
			eq = false
			break

		}
	}

	if len(a) != len(b) {
		eq = false
	}

	if eq {
		fmt.Println(" a and b are equal")
	} else {
		fmt.Println("a and b are not equal")
	}
}
