package main

import "fmt"

func main() {
	fmt.Println("Appending to slices")

	numbers := []int{2, 3}

	// use append function to add to slice
	// does not modify initialize slice but return a new one

	numbers = append(numbers, 10)

	fmt.Println(numbers)
	// returns [2,3,10]

	// can append more elements at onces

	numbers = append(numbers, 20, 40, 30, 50)

	// appending one slice to another

	n := []int{90, 70}

	numbers = append(numbers, n...)

	// returns combination of n and numbers

	// copy a slice to another slice

	src := []int{10, 20, 30}
	dist := make([]int, len(src)) //
	distTwo := make([]int, 2)
	nn := copy(src, dist)
	nnn := copy(src, distTwo)

	fmt.Println(nn)
	// prints the number of elements in the new slice
	//

	fmt.Println(nnn)
	// returns 2 the number of elements in distTwo
}
