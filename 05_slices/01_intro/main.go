package main

import "fmt"

func main() {
	fmt.Println("This is slice section")
	// slice has dynamic lenght, that is can shrink or grow
	// the length is not part of its type and belongs to runtime
	// an uninitialized slice is equal to nil(its zero value is nil)
	// can only contain values of the same type
	// can create a keyed slice like array
	// elements are indexed from 0

}
