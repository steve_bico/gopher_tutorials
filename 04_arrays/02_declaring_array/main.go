package main

import "fmt"

func main() {
	var numbers [4]int           // inititialized by zeros
	fmt.Printf("%v\n", numbers)  //[0,0,0,0]
	fmt.Printf("%#v\n", numbers) // [4]int{0,0,0,0}

	// OTHER WAYS TO DECLARE ARRAY

	var a1 = [4]float64{} // will be inititialized by zeros
	fmt.Printf("%#v\n", a1)

	var a2 = [3]int{-10, 100, 100}
	fmt.Printf("%#v\n", a2) // will be initialized by {-10,100,100}

	//short inititialization
	a3 := [5]int{10, 200, 30, 40, 50}

	fmt.Printf("%v\n", a3)

	names := [5]string{"Jim", "Mike", "Pete", "Kariuki", "Kilunda"}
	fmt.Printf("%v\n", names)

	//inititalizing array with some values

	cars := [4]string{"BMW", "Benz"}

	fmt.Printf("%v\n", cars) // the remaining 2 values will be initialized by 0

	//elipsis operator/triple dots ...
	//finds out automatically the values an array should have
	// does not restrict the number of items that will be in an array

	a5 := [...]int{23, 45, 56, 89, 10, 58, 56, 88, 77, 99, 22, 11}

	//print the length of the array
	fmt.Printf("The length of the array is %d\n", len(a5))
	// len function returns number of element in an array

	//declariing array in nultiple lines
	a6 := [...]int{
		1,
		2,
		3,
		4,
	}

	// comma on the last element is mandatory
	fmt.Printf("%v", a6)
}
