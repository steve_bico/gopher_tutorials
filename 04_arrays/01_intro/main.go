package main

import "fmt"

/*
- an array is composite, indexible type that stores a collection of elements of the same type
- an array in go has fixed length
- every element in array/ slice must of be of the same type
- go stores teh elements on the array in contigous memory locations and this way its very efficient
- the length and the elements of type determise the type of an array.
- Lengths belongs to array type and it is determined at compile time
- eg accounts := [3]int{50,40,30,20}
- this is an array called accounts of type int with 3 integer values



*/

func main() {
	var numbers [4]int           // inititialized by zeros
	fmt.Printf("%v\n", numbers)  //[0,0,0,0]
	fmt.Printf("%#v\n", numbers) // [4]int{0,0,0,0}
}
