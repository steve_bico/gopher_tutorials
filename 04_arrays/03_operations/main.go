package main

import (
	"fmt"
	"strings"
)

func main() {
	//NB: array cannot add or remove elements because it is fixed length

	numbers := [3]int{200, 100, 400}

	// getting length
	numbersLength := len(numbers)
	fmt.Printf("The length of numbers is %v\n", numbersLength)

	// getting element at index
	firstEl := numbers[0]
	fmt.Printf("The first element in numbers is %v", firstEl)

	//modifying an element
	numbers[1] = 7
	//makes element at index 1 to 7 instead of 100

	// iterating an array

	// 1. use range

	for i, v := range numbers {
		//i is index
		//v is value

		fmt.Printf("Index %v and value is %v", i, v)

	}

	fmt.Println(strings.Repeat("#", 10))

	// 2. for loop

	for i := 0; i < len(numbers); i++ {
		fmt.Printf("Index is %v and value is %v", i, numbers[i])
	}

	// multidimensional array

	balances := [2][3]int{
		{5, 6, 9},
		[3]int{9, 10, 44},
	}

	fmt.Printf("The array of balance is %v", balances)

	// arrays equality
	// two arrays are equal if they have the same length and same order of elements
	// equal arrays are not saved on the same location in memory

	m := [3]int{1, 2, 3}
	n := m

	// m and n are two same arrays
	fmt.Println("n is eqaul to m", n == m)
}
