package entities

type Post struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
	UserId int    `json:"userid"`
}

type PostPayload struct {
	Title  string `json:"title"`
	Body   string `json:"body"`
	UserId int    `json:"userid"`
}
