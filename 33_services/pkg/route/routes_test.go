package route

import (
	"net/http"
	"strings"
	"testing"

	"github.com/go-chi/chi/v5"
)

func Test_routes(t *testing.T) {

	// Test table
	var routes = []struct {
		route  string
		method string
	}{
		{"/test", "GET"},
		{"/posts", "GET"},
		{"/posts/{postId}", "GET"},
		{"/posts/create", "POST"},
		{"/posts/{postId}", "PUT"},
		{"/posts/{postId}", "DELETE"},
	}

	mux := Routes()

	chiRoutes := mux.(chi.Routes)

	for _, route := range routes {
		if !routeExists(route.route, route.method, chiRoutes) {
			t.Errorf("route %s is not registered", route.route)
		}
	}
}

// Helper method to check if the route exists
// go test -v .

func routeExists(testRoute, testMethod string, chiRoutes chi.Routes) bool {
	found := false

	_ = chi.Walk(chiRoutes, func(method, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		if strings.EqualFold(method, testMethod) && strings.EqualFold(route, testRoute) {
			found = true
		}

		return nil
	})

	return found
}
