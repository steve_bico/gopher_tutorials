package route

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/jasigana/services/pkg/handler"
)

func Routes() http.Handler {

	mux := chi.NewRouter()
	mux.Use(middleware.Recoverer)

	mux.Get("/test", handler.Test)
	mux.Get("/posts", handler.GetPosts)
	mux.Get("/posts/{postId}", handler.GetPost)
	mux.Post("/posts/create", handler.CreatePost)
	mux.Put("/posts/{postId}", handler.UpdatePost)
	mux.Delete("/posts/{postId}", handler.DeletePost)

	return mux
}
