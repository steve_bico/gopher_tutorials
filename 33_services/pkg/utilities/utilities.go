package utilities

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

type Data struct{}

func (d *Data) ReadJSON(w http.ResponseWriter, r *http.Request, data interface{}) error {
	maxBytes := 1024 * 1024
	r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	// attempt to decode the data
	err := decoder.Decode(data)
	if err != nil {
		return err
	}

	// Allow only json valuse in the payload
	err = decoder.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New("body can only contain JSON values")

	}

	return nil
}

func (d *Data) WriteJSON(w http.ResponseWriter, status int, data any, wrap ...string) error {
	var out []byte

	// deciding to wrap the payload in json tag by checking length of wrap text
	if len(wrap) > 0 {
		wrapper := make(map[string]interface{})
		wrapper[wrap[0]] = data
		jsonBytes, err := json.Marshal(wrapper)
		if err != nil {
			return err
		}
		out = jsonBytes
	} else {
		jsonBytes, err := json.Marshal(data)
		if err != nil {
			return err
		}

		out = jsonBytes
	}

	// Set content type and status
	w.Header().Set("Content-Type", "appliction/json")
	w.WriteHeader(status)

	// write out the json value
	_, err := w.Write(out)
	if err != nil {
		return err
	}

	return nil
}

func (d *Data) ErrorJSON(w http.ResponseWriter, err error, status ...int) {
	statusCode := http.StatusBadRequest
	if len(status) > 0 {
		statusCode = status[0]
	}

	type jsonError struct {
		Message string `json:"message"`
	}

	theError := jsonError{
		Message: err.Error(),
	}

	_ = d.WriteJSON(w, statusCode, theError, "error")

}
