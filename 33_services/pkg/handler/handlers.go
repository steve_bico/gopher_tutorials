package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/jasigana/services/pkg/entities"
	"gitlab.com/jasigana/services/pkg/utilities"
)

var url = "https://jsonplaceholder.typicode.com/posts"
var data = utilities.Data{}

func Test(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("This is test handler"))

}

func GetPosts(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return

	}

	response, err := client.Do(req)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return

	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	var posts []entities.Post
	json.Unmarshal([]byte(body), &posts)

	_ = data.WriteJSON(w, http.StatusOK, posts)

}

func GetPost(w http.ResponseWriter, r *http.Request) {
	postId := strings.TrimPrefix(r.URL.Path, "/posts/")
	if postId == "" {
		data.ErrorJSON(w, errors.New("provide post id"), http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(postId)
	if postId == "" {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	client := &http.Client{}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%d", url, id), nil)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	response, err := client.Do(req)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		data.ErrorJSON(w, errors.New("error sending request"), http.StatusInternalServerError)
		return

	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	var post entities.Post
	json.Unmarshal([]byte(body), &post)

	_ = data.WriteJSON(w, http.StatusOK, post)

}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	var payload entities.PostPayload

	err := data.ReadJSON(w, r, &payload)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	jsonData, err := json.Marshal(&payload)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	client := &http.Client{}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accpet", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		data.ErrorJSON(w, fmt.Errorf("%d", resp.StatusCode), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	var post entities.Post
	json.Unmarshal([]byte(body), &post)

	_ = data.WriteJSON(w, http.StatusCreated, post)
}

func UpdatePost(w http.ResponseWriter, r *http.Request) {
	postId := strings.TrimPrefix(r.URL.Path, "/posts/")
	if postId == "" {
		data.ErrorJSON(w, errors.New("provide post id"), http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(postId)
	if postId == "" {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	var payload entities.PostPayload

	err = data.ReadJSON(w, r, &payload)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	jsonData, err := json.Marshal(&payload)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	client := &http.Client{}

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/%d", url, id), bytes.NewBuffer(jsonData))
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accpet", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		data.ErrorJSON(w, fmt.Errorf("%d", resp.StatusCode), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	var post entities.Post
	json.Unmarshal([]byte(body), &post)

	_ = data.WriteJSON(w, http.StatusOK, post)
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	postId := strings.TrimPrefix(r.URL.Path, "/posts/")
	if postId == "" {
		data.ErrorJSON(w, errors.New("provide post id"), http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(postId)
	if postId == "" {
		data.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	client := &http.Client{}

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%d", url, id), nil)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accpet", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		data.ErrorJSON(w, fmt.Errorf("%d", resp.StatusCode), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		data.ErrorJSON(w, err, http.StatusInternalServerError)
		return
	}

	var post entities.Post
	json.Unmarshal([]byte(body), &post)

	_ = data.WriteJSON(w, http.StatusOK, post)

}
