package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jasigana/services/pkg/entities"
)

func Test_GetPost(t *testing.T) {

	var req *http.Request

	req, _ = http.NewRequest("GET", "/posts", nil)
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(GetPosts)

	handler.ServeHTTP(rr, req)
	test := "Get Posts"
	expectedCode := 200

	if rr.Code != 200 {
		t.Errorf("%s: wrong status code: expected %d, got %d", test, expectedCode, rr.Code)
	}

}

func Test_GetAPost(t *testing.T) {

	req, err := http.NewRequest("GET", "/posts/1", nil)
	if err != nil {
		t.Fatalf("%s => Failed: to make request %v", "Get Post", err)
	}

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(GetPost)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("%s => Failed: expected %d but got %d", "Get Post", 200, rr.Code)
	}

}

func Test_CreatePost(t *testing.T) {
	post := entities.PostPayload{
		Title:  "Test Post",
		Body:   "Test Post Body",
		UserId: 1,
	}

	postBody, err := json.Marshal(post)
	if err != nil {
		t.Fatalf("%s => Failed to mashall post: %v", "Create Post", err)
	}

	req, err := http.NewRequest("POST", "/posts/create", bytes.NewBuffer(postBody))
	if err != nil {
		t.Fatalf("%s => Failed to crate request: %v", "Create Post", err)
	}

	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(CreatePost)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("%s => Failed: expected %v but got %v", "Create Post", http.StatusCreated, status)
	}

	var createdPost entities.Post
	err = json.NewDecoder(rr.Body).Decode(&createdPost)
	if err != nil {
		t.Errorf("%s=>Failed to decode response body: %v", "Create Post", err)
	}

	if createdPost.ID != 101 {
		t.Errorf("%s => Failed: returned unexpected ID: want %v but got %v", "Create Post", 1, createdPost.ID)
	}

	if createdPost.Title != post.Title {
		t.Errorf("%s => Failed: returned unexpected title: want %v but got %v", "Create Post", createdPost.Title, post.Title)
	}

	if createdPost.Body != post.Body {
		t.Errorf("%s => Failed: returned unexpected body: want %v but got %v", "Create Post", createdPost.Body, post.Body)
	}

	if createdPost.UserId != 1 {
		t.Errorf("%s => Failed: returned unexpected ID: want %v but got %v", "Create Post", 1, createdPost.UserId)
	}

}

func Test_UpdatePost(t *testing.T) {
	post := entities.PostPayload{
		Title:  "Test Post To Update",
		Body:   "Test Post Body To Update",
		UserId: 1,
	}

	postBody, err := json.Marshal(post)
	if err != nil {
		t.Fatalf("%s => Failed to mashall post: %v", "Update Post", err)
	}

	req, err := http.NewRequest("PUT", "/posts/1", bytes.NewBuffer(postBody))
	if err != nil {
		t.Fatalf("%s => Failed to crate request: %v", "Update Post", err)
	}

	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(UpdatePost)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("%s => Failed: expected %v but got %v", "Update Post", http.StatusOK, status)
	}

	var updatedPost entities.Post
	err = json.NewDecoder(rr.Body).Decode(&updatedPost)
	if err != nil {
		t.Errorf("%s=>Failed to decode response body: %v", "Update Post", err)
	}

	if updatedPost.ID != 1 {
		t.Errorf("%s => Failed: returned unexpected ID: want %v but got %v", "Update Post", 1, updatedPost.ID)
	}

	if updatedPost.Title != post.Title {
		t.Errorf("%s => Failed: returned unexpected title: want %v but got %v", "Update Post", updatedPost.Title, post.Title)
	}

	if updatedPost.Body != post.Body {
		t.Errorf("%s => Failed: returned unexpected body: want %v but got %v", "Update Post", updatedPost.Body, post.Body)
	}

	if updatedPost.UserId != 1 {
		t.Errorf("%s => Failed: returned unexpected ID: want %v but got %v", "Update Post", 1, updatedPost.UserId)
	}

}

func Test_DeletePost(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/posts/1", nil)
	if err != nil {
		t.Fatalf("%s => Failed to crate request: %v", "Delete Post", err)
	}

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(DeletePost)

	handler.ServeHTTP(rr, req)
	test := "Delete Post"
	expectedCode := 200

	if rr.Code != 200 {
		t.Errorf("%s: wrong status code: expected %d, got %d", test, expectedCode, rr.Code)
	}
}
