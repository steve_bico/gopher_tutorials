package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/jasigana/services/pkg/route"
)

const port = 9005

func main() {
	fmt.Println("This is main ")

	log.Printf("Starting API on port %d\n", port)

	err := http.ListenAndServe(fmt.Sprintf(":%d", port), route.Routes())
	if err != nil {
		log.Fatal(err)
	}

}
