package main

import "fmt"

func main() {
	// Functions are first class values
	// functions themselves can be parameter values for other functions
	// 1. First class citizen
	numbers := []int{1, 4, 7, 9, 10}
	_ = numbers

	trippled := transformNumbers(&numbers, tripleNumber) // don't execute tripleNumber
	fmt.Println(trippled)
	doubled := transformNumbers(&numbers, doubleNumber)
	fmt.Println(doubled)

}

func transformNumbers(numbers *[]int, transform func(int) int) []int {
	dNumbers := []int{}
	for _, value := range *numbers {
		dNumbers = append(dNumbers, transform(value))
	}

	return dNumbers
}

func tripleNumber(value int) int {
	return value * 3
}

func doubleNumber(value int) int {
	return value * 2
}
