package main

import "fmt"

func main() {
	numbers := []int{1, 2, 3, 4, 5}
	transformed := transformNumbers(&numbers, func(number int) int {
		return number * 2
		// the second param here is anonymous function
	})

	fmt.Println(transformed)
}

func transformNumbers(numbers *[]int, transform func(int) int) []int {
	dNumbers := []int{}
	for _, val := range *numbers {
		dNumbers = append(dNumbers, transform(val))
	}
	return dNumbers
}

// anonymous function is just in time function which is defined just when you need it.
//
