package main

func main() {}

// Variadic functions work with any amount of parameters
// takes an argument with ... and the type here numbers ... int
// ... creates a slice during the operation behind the scenes
// can take strings, etc

func sumUp(numbers ...int) int {

	sum := 0

	for _, number := range numbers {
		sum += number
	}

	return sum
}
