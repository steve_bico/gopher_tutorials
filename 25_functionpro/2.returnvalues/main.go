package main

import "fmt"

func myfunc() {
	// Function return as values
	// functions can return other functions
	numbers := []int{1, 2, 3, 4, 5}
	moreNumbers := []int{5, 3, 7, 8}

	doubled := getTransformer(&numbers)
	trippled := getTransformer(&moreNumbers)

	fmt.Println(doubled, trippled)

}

func getTransformer(numbers *[]int) func(int) int {
	if (*numbers)[0] == 1 {
		return double
	} else {
		return triple
	}

	// nb: it is not being executed but just being returned
	//
}

func double(number int) int {
	return number * 2
}

func triple(number int) int {
	return number * 3
}
