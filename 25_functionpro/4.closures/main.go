package main

import "fmt"

// Closures
// also use anonymous function

func main() {
	numbers := []int{1, 2, 3, 4, 5}
	transformed := transformNumbers(&numbers, func(number int) int {
		return number * 2
		// the second param here is anonymous function
	})

	double := createTransformer(2)
	triple := createTransformer(3)

	_ = triple
	_ = double

	// The above are functions created by createTransformer
	// they also return the values of the function which is returned by create transformer

	fmt.Println(triple(4))
	fmt.Println(double(5))

	doubled := transformNumbers(&numbers, double)
	fmt.Println(doubled)
	trippled := transformNumbers(&numbers, triple)
	fmt.Println(trippled)

	fmt.Println(transformed)
}

func transformNumbers(numbers *[]int, transform func(int) int) []int {
	dNumbers := []int{}
	for _, val := range *numbers {
		dNumbers = append(dNumbers, transform(val))
	}
	return dNumbers
}

func createTransformer(factor int) func(int) int {
	return func(number int) int {
		return number * factor
	}
}
