package main

import "fmt"

func main() {
	// Recursion is when a function calls itsefl
	factOne := factorialOne(5)
	fmt.Println(factOne)
	factTwo := factorial(5)
	fmt.Println(factTwo)
}

// Calculate factorial of number
// factorial of 5: 5 * 4 *3 * 2 * 1

func factorialOne(number int) int {
	result := 1
	for i := 1; i < number; i++ {
		result = result * 1
	}
	return result
}

func factorial(number int) int {
	if number == 0 {
		return 1
	}
	return number * factorial(number-1)
}
