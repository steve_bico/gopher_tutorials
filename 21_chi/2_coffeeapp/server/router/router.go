package router

import (
	"net/http"
	"server/controllers"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

// Chi Routers

func Routes() http.Handler {
	router := chi.NewRouter()

	router.Use(middleware.Recoverer)
	router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"http://*", "https://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	router.Post("/api/v1/coffees/coffee", controllers.CreateCoffee)
	router.Get("/api/v1/coffees/all", controllers.GetAllCoffees)
	router.Get("/api/v1/coffees/coffee/{id}",controllers.GetOneCoffeeByID)
	router.Put("/api/v1/coffees/coffee/{id}",controllers.UpdateCoffeeByID)
	router.Delete("/api/v1/coffees/coffee/{id}",controllers.DeleteCoffeeByID)

	
	return router

}
