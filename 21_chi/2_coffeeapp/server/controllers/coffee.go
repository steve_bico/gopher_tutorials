package controllers

import (
	"encoding/json"
	"net/http"
	"server/helpers"
	"server/services"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var coffee services.Coffee

// POST -> coffees/coffee

func CreateCoffee(w http.ResponseWriter, r *http.Request) {
	var coffeeData services.Coffee

	err := json.NewDecoder(r.Body).Decode(&coffeeData)

	if err != nil {
		helpers.MessageLogs.ErrorLog.Println("First error check", err)
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	coffee, err := coffee.CreateCoffee(coffeeData)

	// Second Error Check
	if err != nil {
		helpers.MessageLogs.ErrorLog.Println("Second error check", err)
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	helpers.WriteJSON(w, http.StatusOK, coffee)
}

// GET -> /coffees

func GetAllCoffees(w http.ResponseWriter, r *http.Request) {
	allCoffees, err := coffee.GetAllCoffees()

	if err != nil {
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	helpers.WriteJSON(w, http.StatusOK, helpers.Envelop{"Coffees": allCoffees})

}

// GET -> /coffees/coffee/{id}
func GetOneCoffeeByID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id")) // getting the id from the param and converting it to string.

	if err != nil {
		//http.Error(w, err.Error(), http.StatusBadRequest)
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err})
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	coffee, err := coffee.GetCoffeeByID(id)

	if err != nil {
		helpers.WriteJSON(w, http.StatusNotFound, helpers.Envelop{"msg": err.Error()})
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	helpers.WriteJSON(w, http.StatusOK, coffee)

}

// Update -> /coffees/coffee/{id}
func UpdateCoffeeByID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	// converting he url string to id
	if err != nil {
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err.Error()})
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	var coffeeData services.Coffee

	err = json.NewDecoder(r.Body).Decode(&coffeeData)

	if err != nil {
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err.Error()})
		helpers.MessageLogs.ErrorLog.Println(err)
		return

	}

	updatedCoffee, err := coffee.UpdateCoffeeByID(id, coffeeData)

	if err != nil {
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err.Error()})
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	helpers.WriteJSON(w, http.StatusOK, updatedCoffee)

}

// DELETE -> /coffees/coffee/{id}
func DeleteCoffeeByID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	// converting he url string to id
	if err != nil {
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err.Error()})
		//http.Error(w, err.Error(), http.StatusBadRequest)
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	err = coffee.DeleteCoffeeByID(id)

	if err != nil {
		//http.Error(w, err.Error(), http.StatusBadRequest)
		helpers.WriteJSON(w, http.StatusBadRequest, helpers.Envelop{"msg": err.Error()})
		helpers.MessageLogs.ErrorLog.Println(err)
		return
	}

	helpers.WriteJSON(w, http.StatusOK, helpers.Envelop{"msg": "Deleted successfully"})

}
