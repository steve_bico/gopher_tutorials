package services

import (
	"context"
	"log"
	"time"
)

type Coffee struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	Roast     string    `json:"roast"`
	Image     string    `json:"image"`
	Region    string    `json:"region"`
	Price     float32   `json:"price"`
	GrindUnit int16     `json:"grind_unit"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// Get all coffees
func (c *Coffee) GetAllCoffees() ([]*Coffee, error) {
	// Return type is slice [] of type Coffee and an error
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)

	defer cancel()

	//goland:noinspection SqlNoDataSourceInspection
	query := `SELECT id,name,roast,image, region, price, grind_unit,created_at,updated_at FROM coffees`
	rows, err := db.QueryContext(ctx, query)

	if err != nil {
		log.Fatal(err)
	}

	var coffees []*Coffee // coffees slice of type *Coffee

	for rows.Next() {

		var coffee Coffee

		err := rows.Scan(
			&coffee.ID,
			&coffee.Name,
			&coffee.Roast,
			&coffee.Image,
			&coffee.Region,
			&coffee.Price,
			&coffee.GrindUnit,
			&coffee.CreatedAt,
			&coffee.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		coffees = append(coffees, &coffee)
	}

	return coffees, nil
}

// Post Coffee
func (c *Coffee) CreateCoffee(coffee Coffee) (*Coffee, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)

	defer cancel()

	//goland:noinspection ALL
	query := `
				INSERT INTO coffees
					(name,roast,image,region,price,grind_unit,created_atupdated_at) 
				VALUES ($1,$2,$3,$4,$5,$6,$7,$8) returning *
			`

	// CHECK:
	_, err := db.ExecContext(
		ctx, query, coffee.Name, coffee.Roast, coffee.Image, coffee.Region, coffee.Price, coffee.GrindUnit, time.Now(), time.Now(),
	)

	// We are using server time to generate CreatedAt,UpdatedAt values

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	// &coffee his the information we are storing in the table.
	// if we reach here means that everything went well and information should be stored in the table
	return &coffee, nil
}

// Get one coffee by ID
func (c *Coffee) GetCoffeeByID(id int) (*Coffee, error) {
	// Return coffee pointer &coffee by id of type Coffee and an error
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)

	defer cancel()

	var coffee Coffee

	//goland:noinspection SqlNoDataSourceInspection
	query := `SELECT * FROM coffees WHERE id = $1`

	row := db.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&coffee.ID,
		&coffee.Name,
		&coffee.Roast,
		&coffee.Image,
		&coffee.Region,
		&coffee.Price,
		&coffee.GrindUnit,
		&coffee.CreatedAt,
		&coffee.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &coffee, nil
}

// Get one coffee by ID
func (c *Coffee) UpdateCoffeeByID(id int, body Coffee) (*Coffee, error) {
	// Return coffee pointer &coffee by id of type Coffee and an error
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)

	defer cancel()

	//goland:noinspection SqlNoDataSourceInspection
	query := `
				UPDATE coffees
				SET 
					name = $1,
					roast = $2,
					image = $3,
					region = $4,
					price = $5, 
					grind_unit = $6,
					updated_at = $7
				WHERE id = $8
				returning *
			`

	_, err := db.ExecContext(
		ctx,
		query,
		body.Name,
		body.Roast,
		body.Image,
		body.Region,
		body.Price,
		body.GrindUnit,
		time.Now(),
		id,
	)

	if err != nil {
		return nil, err
	}

	// You can have the body returned from sql by decoding it and returning it too.
	// At this point, it means everything is successful and we do not need to return that
	return &body, nil
}

// Get one coffee by ID
func (c *Coffee) DeleteCoffeeByID(id int) error {
	// Return coffee pointer &coffee by id of type Coffee and an error
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)

	defer cancel() // cancel db connection

	//goland:noinspection SqlNoDataSourceInspection
	query := `DELETE FROM coffees WHERE id = $1`

	_, err := db.ExecContext(ctx, query, id)

	if err != nil {
		return err
	}

	return nil
}
