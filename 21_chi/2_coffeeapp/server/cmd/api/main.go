package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"server/db"
	"server/router"
	"server/services"
	"strconv"

	"github.com/joho/godotenv"
)

type Config struct {
	Port string
}

type Application struct {
	Config Config
	Models services.Models
}

// nodemon --exec go run main.go --signal SIGTERM

func (app *Application) Serve() error {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	port := os.Getenv("PORT")
	fmt.Printf("API is listening on port  %v ...!\n", port)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: router.Routes(),
	}

	return server.ListenAndServe()
}

func main() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal("Error loading .env files")
	}

	config := Config{
		Port: os.Getenv("PORT"),
	}

	// Connection to db
	dbHost := os.Getenv("HOST")
	dbUser := os.Getenv("USER")
	dbPort, _ := strconv.Atoi(os.Getenv("DB_PORT"))
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s", dbHost, dbPort, dbUser, dbPassword, dbName)
	dbConnection, err := db.ConnectPostgres(dsn)

	if err != nil {
		log.Fatal("Cannot connect to database")
	}

	defer dbConnection.DB.Close() // run all process then defer dbConnection Close()

	app := &Application{
		Config: config,
		Models: services.NewConnections(dbConnection.DB),
	}
	err = app.Serve()
	if err != nil {
		log.Fatal(err)
	}
}
