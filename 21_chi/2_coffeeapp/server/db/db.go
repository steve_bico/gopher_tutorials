package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
)

type DB struct {
	DB *sql.DB
}

var dbConn = &DB{}

const maxOpenDbConn = 10
const maxIdleDbConn = 5
const maxDbLifetime = 5 * time.Minute

func ConnectPostgres(dsn string) (*DB, error) {
	database, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	database.SetMaxOpenConns(maxOpenDbConn)
	database.SetConnMaxIdleTime(maxIdleDbConn)
	database.SetConnMaxLifetime(maxDbLifetime)

	err = testDB(database)

	if err != nil {
		return nil, err
	}

	dbConn.DB = database

	return dbConn, nil
}

func testDB(d *sql.DB) error {
	err := d.Ping()
	if err != nil {
		fmt.Printf("Error %v", err)
		return err
	}

	fmt.Println("*** Pinged database successfully ***")
	return nil
}
