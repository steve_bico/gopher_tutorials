
CREATE TABLE IF NOT EXISTS coffees (
    "id" SERIAL PRIMARY KEY NOT NULL,
    "name" VARCHAR NOT NULL,
    "roast" VARCHAR NOT NULL,
    "region" VARCHAR NOT NULL,
    "image" VARCHAR NOT NULL,
    "price" FLOAT NOT NULL,
    "grind_unit" INT NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);