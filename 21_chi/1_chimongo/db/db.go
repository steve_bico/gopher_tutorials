package db

import (
	"context"
	"log"

	"github.com/redis/go-redis/v9"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func ConnectDB(ctx context.Context, dbURL string) (*mongo.Client, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(dbURL))
	err = client.Ping(ctx, readpref.Primary())

	if err != nil {
		return nil, err
	}

	log.Println("Connected to mongo")

	return client, nil
}

func ConnectRedisClient(address string, password string, db int, ctx context.Context) *redis.Client {
	redisDB := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       db,
	})

	pong, err := redisDB.Ping(ctx).Result()

	if err != nil {
		log.Fatal(err)
	}

	log.Println(pong, "Connected redis!")

	return redisDB
}
