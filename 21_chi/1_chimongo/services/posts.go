package services

import (
	"context"
	"github.com/redis/go-redis/v9"
	"go.mongodb.org/mongo-driver/mongo"
)

type Post struct {
	Ctx context.Context
	Col *mongo.Collection
	Rdc *redis.Client
}

func CreatePostService(ctx context.Context, col *mongo.Collection, rdc *redis.Client) *Post {
	return &Post{
		Ctx: ctx,
		Col: col,
		Rdc: rdc,
	}
}

func (s *Post) DBInsertPost() error {
	//s.Rdc.Set(s.Ctx, s., value interface, expiration time.Duration)
	return nil
}
