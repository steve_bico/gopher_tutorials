package main

import (
	"fmt"
	"gochimongo/db"
	"gochimongo/routers"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"context"

	"github.com/joho/godotenv"
)

// nodemon --exec go run main.go --signal SIGTERM

var (
	port  string
	env   string
	dbURL string
)

func init() {
	// load the .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(err)
	}

	log.Println("Env loaded...!")
	port = os.Getenv("PORT")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	dbURL = os.Getenv("MONGOURL")

	_, err = db.ConnectDB(ctx, dbURL)

	if err != nil {
		log.Fatal(err)
	}

	// Redis Configs
	redisAdd := os.Getenv("REDIS_ADD")
	redisPassword := os.Getenv("REDIS_PASSWORD")
	rdsDB, _ := strconv.Atoi(os.Getenv("REDIS_D"))

	rdb := db.ConnectRedisClient(redisAdd, redisPassword, rdsDB, ctx)
	_ = rdb

}

func main() {
	router := routers.InitRoutes()

	server := http.Server{
		Addr:              fmt.Sprintf(":%s", port),
		Handler:           router,
		IdleTimeout:       30 * time.Second,
		ReadHeaderTimeout: 5 * time.Second,
		ReadTimeout:       10 * time.Second,
		WriteTimeout:      10 * time.Second,
	}

	log.Fatal(server.ListenAndServe())
}
