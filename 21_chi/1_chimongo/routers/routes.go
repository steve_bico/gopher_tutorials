package routers

import (
	"gochimongo/handlers"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func InitRoutes() http.Handler {
	mux := chi.NewRouter()
	mux.Get("/", handlers.HomeHandler)
	return mux
}
