package calculator_test

import (
	"log"
	"os"
	"testing"

	"github.com/bicosteve/tdd/pkg/calculator"
)

func TestMain(m *testing.M) {
	//set up
	setup()

	//run the test
	e := m.Run()

	//Clean up the statements
	teardown()

	//report the exit code
	os.Exit(e)
}

func TestAdd(t *testing.T) {

	// defer func() {
	// 	log.Println("Deferred tearing down.")
	// }()

	// Arrange
	e := calculator.Engine{}
	// x, y := 2.5, 3.5

	actAssert := func(x, y, want float64) {
		// Act - invokes the add method
		got := e.Add(x, y)

		// Assert - compare found value from the expected result
		if got != want {
			t.Errorf("Add(%.2f, %.2f) incorrect, got: %.2f, want: %.2f", x, y, got, want)

		}

		// Red - fail the test with incorrect values
		// Green - run the test with correct values
		// Refactor - cleans up the code by extracting values like x, y := 2.5, 3.5

	}

	// subtests
	t.Run("positive input", func(t *testing.T) {
		x, y := 2.5, 3.5
		want := 6.0
		actAssert(x, y, want)
	})

	// subtests
	t.Run("negative input", func(t *testing.T) {
		x, y := -2.5, -3.5
		want := -6.0
		actAssert(x, y, want)
	})

}

func setup() {
	log.Println("Tearing down")
}

func teardown() {
	log.Println("Setting up.")
}
