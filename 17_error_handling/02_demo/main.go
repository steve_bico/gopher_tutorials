package main

import (
	"errors"
	"fmt"
)

type Stuff struct {
	values []int
}

func (s *Stuff) Get(index int) (int, error) {
	if index > len(s.values) {
		return 0, errors.New(fmt.Sprintf("No element index at index %v", index))
	} else {
		return s.values[index], nil
	}

}

func main() {

	stuff := Stuff{values: []int{2, 5, 6, 7, 50, 23, 45}}

	value, err := stuff.Get(10)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(value)
	// returns no element at index 10
}
