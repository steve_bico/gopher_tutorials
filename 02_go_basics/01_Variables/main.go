package main

import (
	"fmt"
)

func main() {
	fmt.Println("Howdy")

	// 1. VARIABLES - variables are names in memory where value of specific type is stored
	// declared variable must be used or we will get an error
	// go's variable belongs and is created at runtime
	// _ blank identifier mutes the compile time error returned of unused variables
	// use var keyword to declare variables
	// can also use short declaration :=

	var x int = 7
	var s1 string

	s1 = "my first string"

	// using (:=) short declaration operator
	// only work on blocked scope
	// cannot use := for already declared variables
	// must only be used on new variables
	age := 30
	myAge := 45
	hisAge := 50

	// multiple statement on the line

	fmt.Println("My age is ", myAge)
	fmt.Println("His age is ", age)

	fmt.Println("Its value is ", x)
	fmt.Println("Your string is ", s1)

	// mute the compile error of unused variable by using _
	_ = hisAge // this will mute the compile error

	// 2. DECLARING MULTIPLE VARIABLES
	car, cost := "Audi", 50_000_000
	fmt.Println(car, cost)

	//can use multiple declarations if there is a new variable in the declaration
	car, year := "Merc", 1990 // this works because new declaration is year

	fmt.Println(car, year)

	var opened = false
	opened, file := true, "main.txt"

	_, _ = opened, file // mutes compile error

	//3. MULTIPLE DECLARATIONS FOR EMPHASIS
	var (
		salary    float64
		firstName string
		adult     bool
	)

	var a, b, c int

	fmt.Println(a, b, c)
	// returns 0,0,0

	fmt.Println(salary, firstName, adult)

	// 4. Multiple Assignment

	var i, j int
	i, j = 5, 8

	_, _ = i, j

	//swapping variables
	j, i = i, j

	fmt.Println(i, j) // returns 8, 5

	//
	sum := 5 + 2.4

	fmt.Println(sum)

	//5. TYPES AND ZERO VALUES
	// go does type checking at compile time
	// each variable should have a type or be infered if not provided
	// cannot reasign a type one variable to another variable type
	// to make it work, you have to convert a type to another
	// typing prevents bugs as early as possible
	// a variable of one type cannot be reasigned to variable of another type

	var e = 5
	var d = 4.5

	e = int(d)
	fmt.Println(e)

	// zero values
	// zero value ensures a variable must have a value
	// numeric types = 0
	// boolean types = false
	// string type = "" empty string
	// pointer type = nil

	var value int
	var price float64
	var name string
	var turn bool

	fmt.Println(value, price, name, turn)
	// returns 0, 0, "" false

	// use // for inline comments
	// can also use /**/
	// this will comment out an entire block of code

	// NAMING CONVENTIONS
	// names start with letters or underscore (_)
	// go keywords cannot be used as names
	// upper case first letter will be exported and be used in other packages
	// example is Max := 100
	// use mixCap/camel case to write variables with two words
	// eg writeToDB := true
	//

	/*
		fmt PACKAGE
		-> implements formatted I/O with functions
		-> has Println
		-> expression can be used as Println functions
		a,b := 4,6
		fmt.Println("Sum: ", a+b, "Mean value is ", (a+b)/2)

		//Printf - this is used for formatting text
		// %d means replace the printed value with an integer
		fmt.Printf("Your age is %d \n",21) // will replace %d with integer value
		fmt.Printf("Your age is %d and your weight is %f", 45, 73.5)
		fmt.Printf("He says: \"Hello Go\"\n")

		figure := "Circle"
		radius := 5
		pi := 3.142


		fmt.Printf("Radius is %d\n", radius)
		fmt.Printf("Radius is %+d\n", radius) will print (+) sign too
		fmt.Printf("The figure is ")
		fmt.Printf("PI constant is %f\n", pi)
		fmt.Printf("The diameter of %s with radius %f is %f ",figure,radius, float64(radius)*2)

		%q for the quoted strings
		fmt.Printf("This is %q", figure)


		%v - can be used to print any type of value
		fmt.Printf("The diameter is %v",float64(radius)*2)

		%T -> is used to see the type of a variable
		fmt.Printf("The figure is of type %T\n",figure) // string
		fmt.Printf("The radius is of type %T\n",radius) // integer


		%t ->  is used to format a boolean value as true or false
		closed := true
		fmt.Printf("The file is closed %t\n",closed)
		will print file closed true

		%b - converts decimal integer to base 2 or binary
		fmt.Printf("%b \n", 55)
		prints 55 in base 2

		x := 3.4
		y := 6.9

		fmt.Printf("X * Y  is = %f\n", x * y)
		fmt.Printf("X * Y  is = %.3f\n", x * y) // to print to 3 decimal places

	*/

	/*
	 7. CONSTANTS
	 -constant is used to represent fixed/unchanging values
	 -constants are used to avoid possible errors
	 -all basic literals are in fact unnamed constants
	 -constant belongs to the compile time and is created at compile time.  It's value cannot be changed while the program runs
	 -go cannot detect errors at runtime but errors on constant can be detected at run time
	 -you can declare constants that store numbers, strings or booleans





	*/

	const days int = 7
	// program can be run without error
	// constants must be initialized when declared
	// it is mandatory to assign value in a constant
	const pi float64 = 3.14
	const secondInHour = 3600

	duration := 234

	fmt.Printf("Duration in seconds %v\n", duration*secondInHour)

	// variable errors are detected at runtime
	// constant errors are detected at compile time
	// compile time errors are detected early

	//declaring multiple constants in one line
	const n, m int = 5, 6
	const nOne, mOne = 7, 8
	// can use both type inference and explicit typing

	//more on multiple constants
	const (
		minOne   = -500
		minTwo   = -300
		minThree = 100
	)
	// in a grouped constant, if the constants are not declared, they will repeat the first constants

	fmt.Println(minOne, minTwo, minThree)

	// constant rules
	// 1. cannot be changed eg temp = 20, temp =40
	const tempt = 20
	//tempt = 30 // throws an error
	// 2. you cannot initiate a constant at runtime
	//const power = math.Pow(2, 4)
	// 3. you cannot use a variable to initialize a constant
	// t := 5
	// const tc = t
	// 4. you can use build in methods to declare constants

	const lengthOne = len("Hello")

	// Untyped Constants
	// can be declared with or without its type
	// constants without types behave different from typed constants
	//

	const ab float64 = 5.2 //typed constants
	const bc = 6.6         // untyped constants

	// constant expressions
	// always evaluated at compile time
	const cb float64 = ab * bc
	const str = "Hello " + "there"
	const dr = 5 > 40
	fmt.Println(dr) // returns false

	// ints and floats cannot be multiplied
	// you can multiple untyped constants of different types but not typed constants of different types

	const xy = 5
	const yx = 2.5 * 5 // this is possible and returns float64

	var ii int = xy     // xy becomes int
	var jj float64 = xy // xy becomes float64

	_, _ = ii, jj

	// default types
	// when untyped constant is used in a context that requires type, the type will be infered
	//

	const r = 5 // untyped constant
	var rr = r  // rr is type int
	_ = rr

	// IOTA
	/*
		-> within a constant declaration, the predeclared identifier IOTA represents successive untyped integer constants
		-> Its value is the index of the respective constSpec in that constant declaration, starting at zero.
		-> It can also be used to construct a set of related constants:

		const (
			c0 = iota  // c0 == 0
			c1 = iota  // c1 == 1
			c2 = iota  // c2 == 2
		)


		const (
			a = 1 << iota
			b = 1 << iota
			c = 3
			d = 1 << iota
		)

		func main(){
			const (
				c1 = iota
				c2 = iota
				c3 = iota
			)

			fmt.Println(c1, c2, c3) -> will return 0, 1, 2
			increments by 1 automatically

			const (
				North = iota
				East
				South
				West
			)

			fmt.Println(East) -> 1
			fmt.Println(South) -> 2
			fmt.Println(West) -> 3
		}



	*/

}
