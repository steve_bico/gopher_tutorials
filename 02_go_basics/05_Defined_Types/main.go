package main

import "fmt"

type kilometre float64
type mile float64

func main() {
	fmt.Println("This is defined type section")
	// defined types are created by programmer from another existing type/source types
	// must have a name and can have its own methods
	// source type provides representation, operations, and size
	// it is a type different from source type though they share representation and operations
	// to perform operations on source type with defined types, they must be converted to align with each other's type.
	// there is no type hierachy in go
	// use keyword type then name then source type

	type age int        // type age underlying/source type int
	type oldAge age     // type oldAge underlying type age
	type veryOldAge int // type veryOldAge source type int

	// WHY?
	// we can attach methods to newly defined types
	// type safety: we must convert one type into another to perform operations with them
	// readability: when we defined a new type let's say type usd float64 we know the new type represents the US Dollar not only floats

	// EXAMPLE
	type speed uint
	var sOne speed = 10
	var sTwo speed = 20

	var x uint // x and sOne are different types

	x = uint(sOne) // converting x to sOne type

	_ = x

	fmt.Println(sTwo - sOne)

	//var sThree speed

	var parisToLondon kilometre = 465
	var distanceInMile mile

	//calculate distance in miles between paris and london

	distanceInMile = mile(parisToLondon) / 0.621 // convert parisToLondon to mile type

	fmt.Println(distanceInMile)

	// ALIASES
	// alias declaration has form of type T1 = T2 as opposed to standard definition which is type T1 T2
	// aliase declaration binds an identifier to the given type.  It is the same type with a new name.
	// types with different names are different types but there is exception to this rule and that is aliased types
	// byte and uint8 are aliases or the same type with different names. The same is applicable to run and int32 because rune is alias of int32
	// aliases can be used together in operatons without type conversions we've seen at the defined types
	// NB You should use aliases with caution, they are not for everyday use

	var a uint8 = 10
	var b byte

	b = a
	_ = b

	type second = uint // this aliases

	var hour second = 36000 // hour is of type second

	fmt.Printf("Minutes in hours: %v \n", hour/60)

	//glpat-ssYW7uv3uKxzXnSSuFoD

}
