package main

// a type determines the set of values together with operations and methods specific to those values
// there are predeclared types, introduced types with type declarations and composite types:
// arrays, slice, map, struct, pointer, function, interface and channel types
//

/*

1. PREDECLARED, BUILT IN TYPES

a) Numeric types
 -> int8, int16, int32, int64
 -> uint8, uint16, uint32, uint64 represent unassigned positive integers
 -> uint is an alias for uint32 or uint64 based on platform
 -> int is an alias for int32 or int64 based on platform
 -> float32, float64: zero before the decimal separator can be ommited (-.4, -.7)
 -> complex64, complex128
 -> byte(alias for uint8)
 -> run(alias for int32)


 func main(){
	var i1 int8 = 100
	fmt.Println("%T\n",i1) -> int8

	var i2 uint16 = 65535
	fmt.Println("%T\n",i2) -> uint16

	// FLOAT
	var f1, f2, f3 float64 = 1.1, -.2, 5.

	// COMPLEX NUMBER

	// BYTE AND RUNE
	are used to distinguish character from integer values
	var r rune = 'f'
	fmt.Println("%T",r) -> int32
	fmt.Println(r) -> 102 the asci code for f
 }

 b) BOOL TYPE
 -> are true and false

 c) STRING TYPE
 -> unicode chars written in double quotes
 -> string value is possible empty sequence of bytes
 ->

 c) ARRAY TYPE
 -> an array is numbered sequence of elements of a single type, called the element type
 -> an array has a fixed length (we specify how many items are in array when it is declared) but slice has dynamic length
 -> a slice can shrink or grow
 ->

 func main(){
	//array
	var numbers = [4] int{4,5,-6,100} -> initializes an array with given type int values
	fmt.Println("%T\n", numbers)

	//slice
	var cities = []string{"london","tokyo","new york"}
	fmt.Println("%T\n", cities) -> returns slice of string
 }

 d) MAP TYPE
 -> is an unordered group of elements of one type, indexed by a set of unique keys of another type
 -> a map in go is similar to python dictionary
 ->

 balances := map[string]float64{
	"USD":34.55,
	"kes":45.66,
	"UGX":55.66,
	"TZSH":56.77
 }

 e) STRUCT TYPE
 -> is a sequence of named elements, called fields, each of which has a name and a type
 -> a struct can be compared to a class in OOP
 ->

 type Car struct{
	brand string
	print int
 }


 // struct
 type Person struct{
	name string
	age int
 }

 var you Person

 fmt.Println("%T\n",you) -> type Person


 f) POINTER TYPE
 -> is a variable that stores the memory addres of another variable
 -> the value of an unititialized pionter is nil
 ->

 var x int = 2
 ptr := &x
 fmt.Printf("Ptr is of type %T with the value of %v\n",ptr, ptr) -> type pointer *int with value of 0x0006


 g) FUNCTION TYPE
 func f(){}

 fmt.Printf("%T\n",f) -> returns type func()

*/
