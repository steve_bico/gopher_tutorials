package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("This is type convertion section")
	// it is converting not
	// can be explicit or implied

	var x = 3   // this type int
	var y = 3.2 // float type

	//x = x * y // returns compile time error. int cannot be multiplied by float

	// you have to convert int to float or vice versa

	x *= int(y) // converting float to int

	fmt.Println(x)

	x = int(float64(x) * y)

	// multiply floats then convert to int

	y = float64(x) * y

	//converts x to float then multiplie by float64

	var a = 5
	var b int64 = 5

	// a and b are different types
	// cannot a + b -> returns an error

	xb := int(b) + a

	fmt.Printf("%T", xb) // returns value of int

	// CONVERTING NUMBERS  TO STRINGS
	//s := string(99) // returns asci of 99 which is c

	//fmt.Println(s) // -> returns C which is asci for 99

	// cannot convert floats to string

	//sf := string(8.9)

	//fmt.Println(sf)//

	var myStr = fmt.Sprintf("%f", 55.6)

	fmt.Println(myStr) // converst float to string

	// to convert a string to an int

	var myStrOne = fmt.Sprintf("%d", 99)

	fmt.Println(myStrOne) // converts 99 to string '99'

	// CONVERTING STRING TO NUMBERS
	sOne := "3.2456"

	fmt.Printf("%T\n", sOne) // type string

	var fOne, err = strconv.ParseFloat(sOne, 64) // first arg is the string to convert, second is precision here 64
	_ = err

	fmt.Println(fOne) // returns 3.2456

	i, err := strconv.Atoi("-60")
	_ = err
	s2 := strconv.Itoa(30)

	fmt.Printf("i type is %T, s2 value is %v", i, 1)
	// returns i type is int and value is -50
	fmt.Printf("s type is %T, s value is %q", s2, s2)
	// returns s2 type is string and s2 value is '30'

}
