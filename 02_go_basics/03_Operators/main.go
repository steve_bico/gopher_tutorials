package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("This is is operator section")
	// Operator -> a symbol that operates on values
	// there are many types
	// 1. Arithmetic & Bitwise Operators: +, -, *, /, %, &, |, ^, <<, >>: for common math operations
	a, b := 4, 5
	r := (a + b) / (a - b) * 2
	fmt.Printf("The value if %v is ", r) // -> returns 6

	r = 9 % b
	fmt.Println(r) // -> returns 4

	// 2. Assignement Operators: =,+=, -=, *=, /=, %= :
	// used to assign values to variables
	c, d := 3, 4

	//increment assigment
	c += b
	fmt.Println(c) // increases c with value of b

	d -= a
	fmt.Println(d) // decrease d by value of a

	// multiplication assignment
	d *= 5
	fmt.Println(d) // multiplies d by 5

	//division assigment
	d /= 5
	fmt.Println(d) // divides d by 5 and assigmes the value to d

	// 3. Increment & Decrement statements: ++, --
	// increments operants by one or decrease by one
	x := 1
	x++
	fmt.Println(x) // increases x by 1 and returns 2

	x--
	fmt.Println(x)

	// 4. Comparison operators: ==, !=, <, >, <=, >=
	// compares tow operands and yields a boolean
	// == equal, != not equal

	// 5. Logical Operators: &&, ||, !
	// logical operators apply to boolean values and yield a result of the same type as the operands
	// && conditional and: returns true if all values are true
	e, f := 5, 10
	fmt.Println(e > 1 && f > 100) // returns false
	fmt.Println(e < 10 || f > 9)  // returns true

	fmt.Println(!(e < 0))                // will return false because it has been negated.
	fmt.Println(!(f == 1) || (e == 100)) // returns true

	// || conditional or : returns true if one value is true
	// ! not/logical negation

	// 6. Operators for Pointers (&) and channels (<-)

	// OVERFLOWS & UNDERFLOWS
	var xy uint8 = 255

	xy++

	fmt.Println(xy) // -> returns 0 because it is an overflow and returns its minimum value of uint8

	//ab := int8(255 + 1) // returns invalid convertion

	var ba int8 = 127
	fmt.Printf("%d\n", ba+1) // returns -128 because of an overflow and returns the minimum value

	ba = -128
	ba--

	fmt.Println(ba) // returns 127 which is its maximum value

	// FLOAT OVERFLOW
	fa := float32(math.MaxFloat32)
	fmt.Println(fa) //returns 3.4

	fa *= 1.5

	fmt.Println(fa) // returns +Inf

	// use package big for big numbers

}
