package main

import "fmt"

func main() {
	type Contact struct {
		email, address string
		phone          int
	}

	type Employee struct {
		name         string
		salary       int
		contanctInfo Contact
	}

	// Employee struct has embedded Contact struct

	john := Employee{
		name:   "John Keller",
		salary: 20_000,
		contanctInfo: Contact{
			email:   "jkeller@company.com",
			address: "Box 64 Sondu",
			phone:   00123474573,
		},
	}

	fmt.Println(john)

	// Accessing field
	fmt.Printf("Employee's email is %s\n", john.contanctInfo.email)
	// accessing data in embedded struct

	// Updating Field
	john.contanctInfo.email = "new_email@company.com"
	fmt.Printf("Employee's new email is %s\n", john.contanctInfo.email)

	myContact := Contact{email: "bix@bix.com", address: "Box 64 Kadongo", phone: 213456789}
	fmt.Println(myContact)

}
