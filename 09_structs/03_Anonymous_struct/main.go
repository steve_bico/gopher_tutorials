package main

import "fmt"

func main() {

	//anonymous struct are defined like this

	diana := struct {
		firstname, lastname string
		age                 int
	}{
		firstname: "Diana",
		lastname:  "Muller",
		age:       30,
	}

	//diana is anonymous struct

	fmt.Printf("%#v\n", diana)
	fmt.Printf("Diana's age is %d\n", diana.age)
	fmt.Printf("Diana's last name is %s\n", diana.lastname)

	// ANONYMOUS FIELDS
	// anonymous fields are defined by string, float64, bool
	//
	type Book struct {
		string
		float64
		bool
	}

	b1 := Book{"1984 By George Orwell", 20.55, false}
	fmt.Println(b1)

	// getting values of anonymous fields
	// use the field types to get the values
	fmt.Println(b1.string)
	fmt.Println(b1.float64)
	fmt.Println(b1.bool)

	// can mix anonymous fields with named fields
	type Employee struct {
		name   string
		salary int
		bool
	}

	employeeOne := Employee{name: "James Dan", salary: 20_000, bool: false}

	fmt.Printf("%#v\n", employeeOne)
}
