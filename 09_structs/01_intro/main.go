package main

import (
	"fmt"
	"time"
)

type user struct {
	firstName string
	lastName  string
	createdAt time.Time
}

func main() {
	// intro
	title, author, year := "Devine Comedy", "Dante Algheri", 1320
	//titleTwo, authorTwo, yearTwo := "Macbeth", "Shakespear", 1606
	fmt.Println("The book details are ", title, author, year)

	appUser := user{
		firstName: "Bond",
		lastName:  "James",
		createdAt: time.Date(1997, time.December, 15, 10, 44, 45, 57, &time.Location{}),
	}

	var dan user

	dan = user{
		firstName: "Dan",
		lastName:  "Tom",
		createdAt: time.Now(),
	}

	mike := user{}
	_ = mike // null value of user struct
	// omitting fields in the type of struct will be a null value of that field

	// outputUserDetails(dan)
	outputUserDetails(&dan)

	fmt.Println(appUser)
	fmt.Println(dan)

	// Creating struct
	type Book struct {
		title  string
		author string
		year   int
	}

	// Book is struct type with fields of title, author, year

	type BookOne struct {
		title, author string
		pages, year   int
	}

	myBook := Book{"Devine Comedy", "Dante Algheri", 1320}
	// this a struct literal
	// struct is the myBook which hold type of variable Book
	fmt.Println("My book is ", myBook)

	myBookTwo := BookOne{"Walenisi", "Wala bin Wala", 250, 1997}
	// this is a struct literal
	// struct is the variable which hold the type of BookOne
	fmt.Println("My second book is ", myBookTwo)

	// Recommended way
	bestBook := Book{title: "Animal Farm", author: "George Orwell", year: 1945}
	// this is the recommended way to initialize a struct and maps field with their correct values
	fmt.Println("My best book is ", bestBook)

	// the fields which have not been initialized return 0

	aBook := Book{title: "Some Random Boo"}
	fmt.Println("Some random book", aBook)
	// will print author will be "" and year to have value of 0
}

func outputUserDetails(u *user) {
	// u *user is a pointer to type user
	// dereference the value by using & when passed in the function
	fmt.Printf("Formatted name is %s %s \n", (*u).firstName, u.lastName)
	// (*u).firstName is the long cut for dereferencing
	// u.lastName is the short cut also allowed by go
	// Dan Tom

}

/*
struct is a sequence of named elements, called fields. Each of them has a name and a type
strcut are sort of classes in oop
struct fields are like instance attributes when we define class in OOP
Go does not have classes but rather structs
struct is a schema containing blueprint a data structure will hold
It is not allowed to change the name or the type at run time.
You cannot remove or add fields in struct at run time

*/
