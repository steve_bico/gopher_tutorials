package main

import "fmt"

type Bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// This is a blue print of a Bill
// It is a custom type of a bill and all the bills will take this structure
// It is sort of object in OOP

// Make new bill function
func newBill(name string) Bill {
	var billOne = Bill{
		name:  name,
		items: map[string]float64{"pie": 5.55, "cake": 4.55},
		tip:   0,
	}

	_ = billOne

	bill := Bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	return bill
}

// Receiver function
// func (bill Bill) -> is the receiver function of Bill and is limited to only Bill type
func (bill Bill) formatBill() string {
	formatedString := "Bill breakdown \n"
	total := 0.0

	// List items
	for key, value := range bill.items {
		formatedString += fmt.Sprintf(" %v ... $%v \n", key+":", value)
		total += value
	}

	formatedString += fmt.Sprintf(" %v .... $%0.2f", " total: ", total)

	return formatedString
}
