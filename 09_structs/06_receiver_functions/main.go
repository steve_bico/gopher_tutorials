package main

import "fmt"

func main() {
	// Struct: Is a blueprint of data.
	// Loosely translate as an object in OOP language

	myBill := newBill("Mario's Bill")

	fmt.Println(myBill.formatBill())
}
