package main

import (
	"errors"
	"fmt"
)

type User struct {
	firstName string
	lastName  string
	age       int
}

// Method
// passing u User means the outputUserDetails function has access to the
// User struct fields
// (u User) -> is called a receiver and the method is called a receiver function
func (u User) outputUserDetails() {
	fmt.Printf("Your name is %s, %s and age is %d \n", u.firstName, u.lastName, u.age)

}

func (u *User) changeAge(number int) {
	u.age += number
}

func (u *User) modifyDetails(firstName string, lastName string, age int) *User {

	user := &User{
		firstName: firstName,
		lastName:  lastName,
		age:       age,
	}

	return user
}

// NB: Methods which modify the struct should have receivers with pointer of the type of struct in this case u *User

// Constructor
func newUser(firstName, lastName string, age int) (*User, error) {
	//This method can now be used to always initial a new user
	if firstName == "" || lastName == "" || age == 0 {
		return nil, errors.New("All fields are required")

	}
	return &User{
		firstName: firstName,
		lastName:  lastName,
		age:       age,
	}, nil
}

func main() {
	fmt.Println("This is method and custom types section")

	// function attached to struct is called a method
	//

	dan := User{
		firstName: "Dan",
		lastName:  "Deen",
		age:       25,
	}

	dan.changeAge(5) // modifying age with u *User

	dan.outputUserDetails()

	dan.modifyDetails("Seth", "Oloo", 50)
	fmt.Println(dan.modifyDetails("Seth", "Oloo", 50))

	mike, err := newUser("Mike", "Deen", 67)
	_ = err
	_ = mike

	var ross *User // ross stores a pointer to a user object
	ross, err = newUser("mike", "rose", 27)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(ross)
}
