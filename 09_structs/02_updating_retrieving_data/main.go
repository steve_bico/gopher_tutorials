package main

import "fmt"

func main() {
	type Book struct {
		title, author string
		copies, year  int
	}

	lastBook := Book{title: "Anna Karenina"}

	//Retrieve value

	fmt.Printf("The title is %v\n", lastBook.title)
	// returns the value of the books title
	// use . notation

	// for a field that does not exist it raises an error of undefined
	// page := lastBook.title - lastBoo.page undefined (type book has no field or method pages)
	// struct are fixed at compile time
	// cannot add or remove fields at run time

	// To Update
	lastBook.author = "Leo Tolstoy"
	lastBook.copies = 250
	lastBook.year = 2005

	// this updates the lastBook
	fmt.Printf("%+v\n", lastBook)

	// Comparing structs
	// struct values are compared with == and two are equal if the structs fields are the same
	aBook := Book{title: "007", author: "James Bond", copies: 300, year: 2005}

	fmt.Println(lastBook == aBook)
	// returns false
	// the values of title, author, copies and year are not the same
	// can only return true if the values of the fields are the same

	// CREATING A COPY OF STRUCTS
	myBook := aBook
	// myBook is a copy of aBook
	// if myBook is modified, it will not affect aBook
	myBook.title = "My Favourite Book"

	fmt.Println(myBook)
	fmt.Println(aBook)
	// the values will be different

}
