package main

import "fmt"

type Car struct {
	Make  string
	Model string
	Year  int
	Price int
}

func (c Car) GetPrice() int {
	return c.Price
}

func (c Car) Info() string {
	return fmt.Sprintf("name: %s %s, price: %d", c.Make, c.Model, c.Price)
}

func (c *Car) SetNewPrice(price int) {
	c.Price = price
}

type House struct {
	Address string
	Storeys int
	Price   int
}

type Property interface {
	GetPrice() int
	Info() string
	SetNewPrice(price int)
}

func (h House) GetPrice() int {
	return h.Price
}
func (h House) Info() string {
	return fmt.Sprintf("name: %s, price: %d", h.Address, h.Price)
}

func (h *House) SetNewPrice(price int) {
	h.Price = price
}

type Boat struct {
	Name  string
	Price int
}

func PrintPrice(p Property) {
	fmt.Printf("the price of the property is : %v\n", p.GetPrice())
}

func SendPropertyInfoToGovernment(p Property) {
	// logic to send the info to the government
	fmt.Printf("sent [%+v] to the government\n", p.Info())
}

func printSomething(arg any) {

}

func getPrice(p Property) int {
	return p.GetPrice()
}

// every property (house, boat and car) have the following methods: buy, sell, getPrice and you want to enforce this
func main() {
	var car = Car{
		Model: "Corolla",
		Make:  "Toyota",
		Year:  2007,
		Price: 2_000_000,
	}

	printSomething(9)
	printSomething("jkjhsfdk")

	var house = House{
		Address: "4354 Nairobi",
		Storeys: 4,
		Price:   1_000_000,
	}

	PrintPrice(&car)
	PrintPrice(&house)

	SendPropertyInfoToGovernment(&car)
	SendPropertyInfoToGovernment(&house)

	fmt.Println(getPrice(&car))
	fmt.Println(getPrice(&house))

}
