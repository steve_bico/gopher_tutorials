package main

import (
	"fmt"
	"math"
)

type shape interface {
	calculateArea()
	calculatePerimeter()
	getArea() float64
	getPerimeter() float64
}

type circle struct {
	radius    float64
	area      float64
	perimeter float64
}

func (c *circle) calculateArea() {
	c.area = math.Pi * c.radius * c.radius
}

func (c *circle) calculatePerimeter() {
	c.perimeter = 2 * math.Pi * c.radius
}

func (c circle) getArea() float64 {
	return c.area
}

func (c circle) getPerimeter() float64 {
	return c.perimeter
}

type rectangle struct {
	height    float64
	width     float64
	area      float64
	perimeter float64
}

func (r *rectangle) calculateArea() {
	r.area = r.height * r.width
}

func (r *rectangle) calculatePerimeter() {
	r.perimeter = 2 * (r.width + r.height)
}

func (r rectangle) getArea() float64 {
	return r.area
}

func (r rectangle) getPerimeter() float64 {
	return r.perimeter
}

// for the shapes above, each has area and perimeter, and we want to maybe send the area and perimeter to the government

// func SendCircleDataToGovernment(c circle) {
// 	c.calculateArea()
// 	c.calculatePerimeter()
// 	fmt.Printf("The area is %.2f and perimeter is %.2f\n", c.area, c.perimeter)
// }

// func SendRectDataToGovernment(r rectangle) {
// 	r.calculateArea()
// 	r.calculatePerimeter()
// 	fmt.Printf("The area is %.2f and perimeter is %.2f\n", r.area, r.perimeter)
// }

type square struct {
	length    float64
	area      float64
	perimeter float64
}

func SendDataToGovernment(s shape) {
	s.calculateArea()
	s.calculatePerimeter()
	fmt.Printf("The area is %.2f and perimeter is %.2f\n", s.getArea(), s.getPerimeter())
}

func SendDataToGovernmentWithoutInterfaces(area, perimeter float64) {
	fmt.Printf("The area is %.2f and perimeter is %.2f\n", area, perimeter)

}

func GetSumOfAreaAndPerimeter(s shape) float64 {
	s.calculateArea()
	s.calculatePerimeter()

	return s.getArea() + s.getPerimeter()
}

func main() {

	var c = circle{
		radius: 7,
	}
	var r = rectangle{
		width:  10,
		height: 5,
	}

	// SendCircleDataToGovernment(c)
	// SendRectDataToGovernment(r)
	SendDataToGovernment(&c)
	SendDataToGovernment(&r)

	SendDataToGovernmentWithoutInterfaces(c.getArea(), c.getPerimeter())

	sumOfAreaAndPerimeterForCircle := GetSumOfAreaAndPerimeter(&c)
	fmt.Printf("sum or area and perimeter of circle is %.2f\n", sumOfAreaAndPerimeterForCircle)
}
