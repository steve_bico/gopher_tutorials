package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	// defer statement - postpones the execution till sorrounding function returns
	// ensures a program or a function is called later usually for purpose of clean up
	defer f1() // this is called last
	f2()

	fmt.Println("Just a string after defering f1() and calling f2()")

	defer f3() // this will be executed first in defer then f1() will be executed last

	// NB f2() will be executed first
	// fmt.Println() will be executed second
	// f3() will be executed third
	// fi() will be executed last

	file, err := os.Open("main.go")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// code that works with the file will be executed before the file is closed

}

func f1() {
	fmt.Println("this is function f1()")
}

func f2() {
	fmt.Println("This is function f2()")
}

func f3() {
	fmt.Println("This is function f3()")
}
