package main

import (
	"fmt"
	"strings"
)

func getInitials(n string) (string, string) {
	s := strings.ToUpper(n)
	// to string n to uppercase

	// split s into slice
	names := strings.Split(s, " ")

	// Create another slice for initials
	var initials []string

	for _, value := range names {
		initials = append(initials, value[:1])
	}

	if len(initials) < 1 {
		return initials[0], "_" // if the length is less than one, return the first then _
	}

	return initials[0], initials[1]
}

func main() {
	firstName, secondName := getInitials("tifa lockhart")
	fmt.Println(firstName)
	fmt.Println(secondName)

	firstNameOne, secondNameOne := getInitials("James")
	fmt.Println(firstNameOne)
	fmt.Println(secondNameOne)

}
