package main

import (
	"fmt"
	"math"
)

// function is small piece of code dedicated to perform a particular task
// within the same package function names must be unique
// a function can return multiple values
// go does not support function overloading

func main() {
	great() // function body will be executed

	sayGreeting("James")
	sayBye("James")

	cycleNames([]string{"Cloud", "James", "Jude"}, sayGreeting)
	// passing slice and function where function is passed by reference
	cycleNames([]string{"Cloud", "James", "Jude"}, sayBye)

	areaOne := cycleArea(8)
	areaTwo := cycleArea(9)

	fmt.Printf("Cycle one is %0.2f", areaOne)
	fmt.Printf("Cycle two is %0.2f", areaTwo)
}

func great() {
	fmt.Println("Look, I am a greet() function ")
}

//

func sayGreeting(name string) {
	fmt.Printf("Good morning %v \n", name)
}

func sayBye(name string) {
	fmt.Printf("Goodbye %v \n", name)
}

// Taking multiple args
func cycleNames(names []string, f func(string)) {
	for _, v := range names {
		f(v)
	}

}

// Returning values from function
func cycleArea(r float64) float64 {
	return math.Pi * r * r
}
