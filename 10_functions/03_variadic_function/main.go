package main

import (
	"fmt"
	"strings"
)

// variadic functions - take variable number of args, ... infront of function makes it variadic
// the function may be called with 0 or more args
//
// if it takes many params onlny the last param will have a type

func main() {
	//add(1, 2, 3, 4) // variadic function
	// returns an int slice

	nums := []int{2, 4}

	nums = append(nums, 3, 6, 7) // append is a variadic function can be called with variable number of args
	//fmt.Println(nums)

	//add(nums...) // it is possible to pass in a slice in variadic function

	//f1(nums...)
	//fmt.Println(nums)
	// will change the 0 index of nums to 50

	sum, product := sumProduct(8.9, 5.6, 9.8, 7.4, 6.5)
	fmt.Println(sum)
	fmt.Println(product)

	info := personInfo(40, "Wolfgang", "Amadeus", "Mozart")

	fmt.Println(info)
	// Age 40, Full name Wolfgang Amadeus Mozart

}

// 1. When a number of params are unknown
// ... will allow for any amount of params but should include the type
// ... string, ... int, ... float64
func add(a ...int) {
	fmt.Printf("%T\n", a)
	// returns [] slice
	fmt.Printf("%#v\n", a)
	// returns an int slice []int{1,2,3,4}
}

// 2. When there is no args
// returns an int slice with value nil

func f1(a ...int) {
	a[0] = 50
}

// sum

func sumProduct(a ...float64) (float64, float64) {
	sum := 0.
	product := 1.

	for _, v := range a {
		sum += v
		product *= v
	}

	return sum, product
}

// mixing variadic and non variadic params
func personInfo(age int, names ...string) string {
	fullName := strings.Join(names, " ")

	returnString := fmt.Sprintf("Age %d, full name is %s ", age, fullName)

	return returnString
	//
}
