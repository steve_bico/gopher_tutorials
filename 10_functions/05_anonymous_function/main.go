package main

import "fmt"

func main() {
	// anonimous function
	func(msg string) {
		fmt.Println(msg)
	}("I am an anomymous value")

	a := increment(10)
	fmt.Printf("%T\n", a)
	// returns func() type int
	a()              // alble to call a()
	fmt.Println(a()) // returns 12
}

func increment(x int) func() int {
	return func() int {
		x++
		return x
	}

}
