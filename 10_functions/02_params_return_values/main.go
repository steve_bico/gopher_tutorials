package main

import (
	"fmt"
	"math"
)

func main() {
	add(50, 60)
	divide(50, 60, 2)
	p := power(4)
	fmt.Println(p)
	a, b := f5(10, 4)
	fmt.Println(a, b)
	c, _ := f5(4, 5) // hide error for second return value with an _
	fmt.Println(c)
	mySum := sum(5, 5)
	fmt.Println(mySum)
	// returns 0 initial s value
	// returns 10 the return value of s
}

func add(a int, b int) {
	// function has two args a, b
	// they are local to this function
	fmt.Println("Sum of a + b = ", a+b)
}

// short hand parameter notatiton
func divide(a, b, c int) {
	// only assign type to the last arg
	fmt.Println("The division of a + b by c is ", (a+b)/c)
}

// return value
func power(a float64) float64 {
	return math.Pow(a, 2)
	// returns a^2
	// any statement below return statement is never executed
}

// returning multiple values
func f5(a, b int) (int, int) {
	return a + b, a * b
}

// named return values
func sum(a, b int) (s int) {
	fmt.Println(s) // s -> 0

	s = a + b

	return
	// will automatically return s
	// make return statement should only be used in short function

}
