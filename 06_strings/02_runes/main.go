package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func main() {
	fmt.Println("This is rune and byte section")
	var1, var2 := 'A', 'B'
	// these are runes
	fmt.Printf("Type %T, Value %d\n", var1, var2)
	// Type - int32, value - 66

	str := "Tara"

	fmt.Println(len(str))
	fmt.Println("Byte (not rune) at position 1: ", str[0])

	for i := 0; i < len(str); i++ {
		fmt.Printf("%c\n", str[i])
		// loops through the string and prints each character
	}

	fmt.Println("\n", strings.Repeat("#", 5))

	for i := 0; i < len(str); {
		r, size := utf8.DecodeRuneInString(str[i:])
		fmt.Printf("%c\n", r)
		i += size

	}

	fmt.Println("\n", strings.Repeat("#", 5))

	for _, r := range str {
		fmt.Printf("%c\n", r)
	}

}

/*
- go has two additional integer called byte and rune that are aliases of uint8 and uint32
- rune and bytes are used t distinguish characters from integer values
- go does not have char data type like java uses byte and rune to represent character values
- characters in go are expressed by enclosing then in single quote '' eg 'A'
- rune literals like 'a','b','c' are represented using unicode points
- character encoding scheme ASCII which is a unicode subset comprises 128 code points
- string is a series of byte vlaues. String is a slice of bytes and any byte slcie can be encoded in a string value
- go terminology for code points is runes.
- rune represent a single unicode character

*/
