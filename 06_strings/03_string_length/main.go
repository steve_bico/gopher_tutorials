package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	fmt.Println("This is byte length section")

	s1 := "Golang"
	s2 := "Codruta"

	fmt.Println(len(s1)) // contains 6 byte
	fmt.Println(len(s2)) // contains 7 byte

	// finding the length using utf* runeCountInString

	n := utf8.RuneCountInString(s1) // returns 6
	m := utf8.RuneCountInString(s2) // returns 7

	fmt.Println(n, m)
}
