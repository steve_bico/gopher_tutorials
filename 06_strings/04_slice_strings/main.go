package main

import "fmt"

func main() {
	fmt.Println("Slicing String")
	// returns part of the string
	// uses backng array methos

	s1 := "This is the string to be sliced"
	fmt.Println(s1[2:5])
	// returns index to to index 5 exclusive - 'is'
	// returns bytes not rune
}
