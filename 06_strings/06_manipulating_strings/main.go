package main

import (
	"fmt"
	"strings"
)

func main() {
	// 1. Repeat function
	// repeats the string with the copy specified

	p := fmt.Println

	myStr := strings.Repeat("Ab \n", 4)

	p(myStr) // repeats Ab 4 times

	// 2. Replace

	myStrOne := strings.Replace("198.888.44.55", ".", ",", 2)
	p(myStrOne) // 198,888,44.55 if the arg is -1 replaces all occurance of to be replaced

	// 3. ReplaceAll

	myStrTwo := strings.ReplaceAll("123.4444.555.666", ".", ":")
	p(myStrTwo) // 123:4444:555:666

	// 4. Split - spits the string and returns a slice of the string with
	str := strings.Split("a,b,c,d,f,g,h", ",")
	fmt.Printf("%T\n", str) // return slice of string
	fmt.Printf("%#v\n", str)

	// if the spit a is empty string, it will split the string on each literal
	strOne := strings.Split("This is my home", "")
	fmt.Printf("%#v\n", strOne)
	// returns []string{"T", "h", "i", "s", " ", "i", "s", " ", "m", "y", " ", "h", "o", "m", "e"}
	// "" empty string returns each character

	strTwo := strings.Split("This is my home", " ")
	fmt.Printf("%#v\n", strTwo)
	// returns []string{"This", "is", "my", "home"}
	// " " returns each word

	// 5. Join
	s := []string{"I", "Learn", "Go"}
	myStrThree := strings.Join(s, " ")
	myStrFour := strings.Join(s, "-")

	fmt.Println(myStrThree)
	// I Learn Go
	// the separator can be anything

	fmt.Println(myStrFour)
	// I-Learn-Go

	// 6. Fields
	sOne := "Orange Green \n Blue Yellow"
	fields := strings.Fields(sOne)

	fmt.Printf("%T\n", fields)  // [] string
	fmt.Printf("%#v\n", fields) // []string{"Orange", "Green", "Blue", "Yellow"}

	// 7. TrimSpace - removes white spaces and leading tabs
	sTwo := strings.TrimSpace(" \t Good buy Windows, Welcome Linux   ")
	fmt.Printf("%q\n", sTwo) // "Good buy Windows, Welcome Linux" removes leading and trailing white space

	// 8. Trim - removes the specified characters in the second args
	// removes leading and trailing specified characters in a string

	sThree := strings.Trim("...Hello Gophers!!!!!?", ".!?")
	fmt.Printf("%q\n", sThree)
	fmt.Printf("%v\n", sThree)

}
