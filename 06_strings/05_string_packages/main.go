package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("String packages section")
	// these are string packages not methods on the string

	// 1. Contains -> whether a substring is in string
	p := fmt.Println
	result := strings.Contains("I love Go programming", "love")
	resultOne := strings.Contains("I love python programming", "Go")

	p(result)    // returns true
	p(resultOne) // returns false

	// 2. ContainsAny
	resultThree := strings.ContainsAny("success", "xy")
	resultFour := strings.ContainsAny("success", "sa")
	p(resultThree) // returns false
	p(resultFour)  // returns true

	// 3. ContainsRune -
	// runes are enclosed in single quote ''

	resultFive := strings.ContainsRune("This is my home", 'i')
	p(resultFive) // returns true

	// 4. Count -> counts the number of instances of a character in the string
	// count with empty search type returns the length of the string + 1
	// n := strings.Count("Five","") returns 5
	resultSix := strings.Count("This is a good home", "a")
	p(resultSix) // returns 1

	// 5. toUpper/toLower
	// converts to upper and lower case and the returned strings is a new string
	p(strings.ToLower("GO PYTHON AND JAVA"))               // go python and java
	p(strings.ToUpper("this is a good day to keep a run")) // THIS IS A GOOD DAY TO KEEP A RUN

	// 6. Comparing Strings
	p("go" == "go")
	// true . does not work well with cases

	p(strings.ToLower("Go") == strings.ToLower("go"))
	// returns true
	// not efficient
	// traverses each string before convertion

	// use efficient conversion string
	p(strings.EqualFold("GO", "go"))
	// returns true
	// recommended way to compare strings

}
