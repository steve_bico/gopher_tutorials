package main

import "fmt"

func main() {
	fmt.Println("This is String section")
	// are defined in doubled quotes
	// are utf8 encoded
	stringOne := "Hi there"
	fmt.Println(stringOne)

	//using escape in string to quote a string
	fmt.Println("He says, \"Hello!\"") // \"Hello" is an escaped string

	//using double quote inside ``

	fmt.Println(`He says "Hello!"`)

	//raw string
	stringTwo := `I like go`
	fmt.Println(stringTwo)

	//printing in a new line
	fmt.Println("The price is 1000 \n Brand: Nike")
	fmt.Println(`
	The price is 1000
	Brand is Nike
	`)

	// escaping strings
	fmt.Println(`C:\Users\Bix`)
	fmt.Println("C:\\Users\\Bix")
	// the above two are the same sentence

	//Concatening strings
	// strings are immutable in go
	// when concatenated, go creates another string

	var stringThree = "I love " + "Go " + "Programming"

	fmt.Println(stringThree + "!")

	// getting element using indexing

	fmt.Println("The element at index 0 is ", stringThree[0])
	//returns a letter

	//immutability of string
	//stringThree[0] = "Y" -> returns unassignable operand error

	//printf function use %s, %q
	// %s is for Printing the string
	// %q is for quoting the string
	fmt.Printf("%s\n", stringThree) // printing the string
	fmt.Printf("%q\n", stringThree) // quoting the string

}
