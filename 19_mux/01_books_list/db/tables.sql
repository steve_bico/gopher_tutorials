SHOW DATABASES;

CREATE DATABASE booksDB

USE booksDB;

SHOW TABLES;

CREATE TABLE books(
    id int NOT NULL AUTO_INCREMENT, 
    title varchar(255),
    author varchar(255),
    year varchar(255),
    PRIMARY KEY (id)
);

INSERT INTO books(title, author, year) VALUES 
    ('Python For Beginners','Luke Shaw','2020'),
    ('Golang Explained','Brad Lee','2019'),
    ('Modern Javascript','James Joe','2021');

//dbName booksDB
