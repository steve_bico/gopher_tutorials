package bookRepository

import (
	"bookslist/models"
	"database/sql"
	"log"
)

type BookRepository struct{}

func (b BookRepository) GetBooks(db *sql.DB, book models.Book, books []models.Book) []models.Book {
	rows, err := db.Query("SELECT * FROM books")

	if err != nil {
		log.Fatal(err.Error())
	}

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)

		if err != nil {
			log.Fatal(err.Error())
		}

		books = append(books, book)
	}

	return books
}

func (b BookRepository) GetBook(db *sql.DB, book models.Book, id int) models.Book {
	rows := db.QueryRow("SELECT * FROM books WHERE id = ? ", id)

	err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)

	if err != nil {
		panic(err.Error())
	}

	return book
}

func (b BookRepository) AddBook(db *sql.DB, book models.Book) (models.Book, int64) {
	addBookQuery := "INSERT INTO books (title,author,year) VALUES (?,?,?)"
	rows, err := db.Exec(addBookQuery, book.Title, book.Author, book.Year)

	if err != nil {
		panic(err.Error())
	}

	id, err := rows.LastInsertId()

	if err != nil {
		panic(err.Error())
	}

	return book, id
}

func (b BookRepository) UpdateBook(db *sql.DB, book models.Book, id int) models.Book {
	updateQuery := "UPDATE books SET title = ?, author = ?, year = ? WHERE id = ?"
	result, err := db.Exec(updateQuery, &book.Title, &book.Author, &book.Year, id)

	if err != nil {
		panic(err.Error())
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		panic(err.Error())
	}

	_ = rowsUpdated
	book.ID = int64(id)

	return book

}

func (b BookRepository) RemoveBook(db *sql.DB, id int) int64 {
	result, err := db.Exec("DELETE FROM books WHERE id = ?", id)

	if err != nil {
		panic(err.Error())
	}

	rowsDeleted, err := result.RowsAffected()

	if err != nil {
		panic(err.Error())
	}

	return rowsDeleted
}
