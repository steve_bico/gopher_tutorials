package controllers

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"

	dbconfig "bookslist/db"
)

// var db *sql.DB

/*
End points to test
1. GetBooks
2. GetABook
3. AddBook
4. UpdateABook
5. DeleteBook

*/
// Get Books Test Case
func TestGetBooks(t *testing.T) {

	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/books", nil)

	controllers := Controller{}

	db := dbconfig.ConnectDB()

	if err != nil {
		t.Fatal(err)
	}

	// record response received
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(controllers.GetBooks(db))

	handler.ServeHTTP(rr, req)

	status := rr.Code

	if status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `[
		{
			"id": 1,
			"title": "Python The Hard Way",
			"author": "Luke Shaw",
			"year": "2021"
		},
		{
			"id": 3,
			"title": "Modern Javascript",
			"author": "James Joe",
			"year": "2021"
		},
		{
			"id": 4,
			"title": "Java For Everyday",
			"author": "Yue Jong",
			"year": "2021"
		},
		{
			"id": 5,
			"title": "Java For Dummies",
			"author": "George Russel",
			"year": "2012"
		},
		{
			"id": 6,
			"title": "Java For Dummies",
			"author": "George Russel",
			"year": "2012"
		},
		{
			"id": 7,
			"title": "Project Management 101",
			"author": "George Russel",
			"year": "2011"
		},
		{
			"id": 9,
			"title": "Hard Way; The Only Way",
			"author": "George Deen",
			"year": "2015"
		},
		{
			"id": 10,
			"title": "Hard Way; The Only Way New Test",
			"author": "George Deen",
			"year": "2015"
		},
		{
			"id": 11,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 12,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 13,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 14,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 15,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 16,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 17,
			"title": "Test New Book",
			"author": "Bico Steve",
			"year": "2023"
		},
		{
			"id": 19,
			"title": "Test New Book",
			"author": "Bico Rapando",
			"year": "2023"
		}
	]`

	if rr.Body.String() != expected {
		t.Errorf("Handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}

}
