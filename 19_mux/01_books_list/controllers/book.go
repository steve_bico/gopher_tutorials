package controllers

import (
	dbconfig "bookslist/db"
	"bookslist/models"
	bookRepository "bookslist/repository/book"

	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Controller struct{}

func (c Controller) GetBooks(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		books := []models.Book{}
		bookRepo := bookRepository.BookRepository{}

		db := dbconfig.ConnectDB()

		books = bookRepo.GetBooks(db, book, books)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(books)

	}
}

func (c Controller) GetABook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var book models.Book
		params := mux.Vars(r)
		bookID := params["book_id"]

		errMap := make(map[string]string)

		id, err := strconv.Atoi(bookID)

		if err != nil {
			errMap["msg"] = "Book id can only be numbers!"
			json.NewEncoder(w).Encode(errMap)
			w.WriteHeader(400)
			return
		}

		db := dbconfig.ConnectDB()
		bookRepo := bookRepository.BookRepository{}

		book = bookRepo.GetBook(db, book, id)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(book)

	}

}

func (c Controller) AddBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		// var bookID int

		json.NewDecoder(r.Body).Decode((&book))

		db := dbconfig.ConnectDB()

		bookRepo := bookRepository.BookRepository{}
		book, id := bookRepo.AddBook(db, book)

		book.ID = int64(id)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(book)

		// addBookQuery := "INSERT INTO books (title,author,year) VALUES (?,?,?)"
		// rows, err := db.Exec(addBookQuery, book.Title, book.Author, book.Year)
		// rows.LastInsertId()
		// rows.RowsAffected()
		// // query - 0, or multiple things
		// // queryrow - one thing
		// // exec - everything else

		// if err != nil {
		// 	w.WriteHeader(http.StatusBadRequest)
		// 	json.NewEncoder(w).Encode(err)

		// }

	}
}

func (c Controller) UpdateABook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		bookID := mux.Vars(r)["book_id"]

		id, err := strconv.Atoi(bookID)

		if err != nil {
			panic(err.Error())
		}

		json.NewDecoder(r.Body).Decode(&book)

		db := dbconfig.ConnectDB()
		bookRepo := bookRepository.BookRepository{}
		book = bookRepo.UpdateBook(db, book, id)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(book)

	}
}

func (c Controller) DeleteABook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		bookID := mux.Vars(r)["book_id"]

		id, err := strconv.Atoi(bookID)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(err.Error())
		}

		json.NewDecoder(r.Body).Decode(&book)

		db := dbconfig.ConnectDB()
		bookRepo := bookRepository.BookRepository{}
		rowsDeleted := bookRepo.RemoveBook(db, id)

		// result, err := db.Exec("DELETE FROM books WHERE id = ?", id)

		// _ = result

		// if err != nil {
		// 	w.WriteHeader(500)
		// 	json.NewEncoder(w).Encode(err.Error())
		// }

		// rowsDeleted, err = result.RowsAffected()

		if err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(err.Error())

		}

		json.NewEncoder(w).Encode(rowsDeleted)

		// nodemon --exec go run main.go --signal SIGTERM
		// air
	}
}
