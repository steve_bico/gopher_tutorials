package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"bookslist/controllers"
	dbconfig "bookslist/db"

	"github.com/gorilla/mux"
)

var db *sql.DB

func main() {

	db = dbconfig.ConnectDB()
	router := mux.NewRouter()
	controllers := controllers.Controller{}

	router.HandleFunc("/books", controllers.GetBooks(db)).Methods("GET")
	router.HandleFunc("/books/{book_id}", controllers.GetABook(db)).Methods("GET")
	router.HandleFunc("/books", controllers.AddBook(db)).Methods("POST")
	router.HandleFunc("/books/{book_id}", controllers.UpdateABook(db)).Methods("PUT")
	router.HandleFunc("/books/{book_id}", controllers.DeleteABook(db)).Methods("DELETE")

	fmt.Println("Golang server running on port 8000...")
	log.Fatal(http.ListenAndServe(":8000", router))
}
