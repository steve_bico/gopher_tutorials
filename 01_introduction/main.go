// GO APPLICATION STRUCTURE
// each go source file begins with package declaration
// it shows what package the file is part of
// this case starts with package main

package main

//executable package that generate a file that can be run
// must also have a function called main inside
// it is followed by any import statements
// functions and variables follow in any order

import "fmt"

const secondsInHour = 3600

func main() {
	//func main is the entry point of the program
	// fmt -> format
	// used for formating strings
	fmt.Println("Hello Go World")
	distance := 60.8

	fmt.Printf("The distance in miles is %f \n", distance*0.62137)

}

// us go run main.go -> compiles and immediately run go program
// go build -> compiles the application and produces an executable file
// it compiles the files in the current directory and produce executable file with the name of the current working dir
// go build -o app will produce an executable file called app
// go build -o <name> will give the file a new name <name> for the file
// use gofmt -w main.go to format the go file in go standard main
