package main

import (
	"fmt"
	"math"
)

type Rectangle struct {
	width, height float64
}

func (r Rectangle) area() float64 {
	return r.height * r.width
}

func (r Rectangle) perimeter() float64 {
	return (2 * r.height) + (2 * r.width)
}

type Circle struct {
	radius float64
}

func (c Circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}

func (c Circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

// declaring interface
type Shape interface {
	// containes signature of a methods
	// but not implementations that is names and params and return type
	// this case area(), perimeter
	area() float64
	perimeter() float64
}

func printShape(s Shape) {
	fmt.Printf("Shape %#v\n", s)
	fmt.Printf("The area is %v\n", s.area())
	fmt.Printf("The perimeter is %v\n", s.perimeter())
}

func main() {
	fmt.Println("Interfaces Section")
	// they define behavior of objects
	//
	// defining shapes

	circleOne := Circle{radius: 7}
	rectangleOne := Rectangle{height: 20, width: 5}

	// any value that implements the interace can be passed in the method that implements the interface
	//
	printShape(circleOne)
	printShape(rectangleOne)

}
