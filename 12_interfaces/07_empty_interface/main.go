package main

import "fmt"

type Empty interface{}

type Person struct {
	info interface{}
}

func main() {
	// empty interface has no method
	// can represent any value
	// may hold values of any type
	// used by code which handle values of unknown type
	//

	var empty interface{}

	empty = 5

	fmt.Println(empty)

	empty = "Go"

	fmt.Println(empty) // can store string value

	empty = []int{4, 6, 8}
	fmt.Println(empty) // stores a slice

	// checking length of the slice
	// use type assertion
	fmt.Println(len(empty.([]int))) // this type assertion

	you := Person{}

	you.info = "Your name "

	fmt.Println(you.info)

	you.info = 40

	fmt.Println(you.info)

	you.info = []float64{5.6, 7.0, 9.6}
	fmt.Println(you.info)

	// NB: Use empty interfaces if it is really necessary
}
