package main

import (
	"fmt"
)

type Rectangle struct {
	width, length float64
}

func (r Rectangle) area() float64 {
	return r.length * r.width
}

func (r Rectangle) perimeter() float64 {
	return (2 * r.length) + (2 * r.width)
}

type Shape interface {
	area() float64
	perimeter() float64
}

func printShape(s Shape) {
	fmt.Printf("Shape %#v\n", s)
	fmt.Printf("The area is %v\n", s.area())
	fmt.Printf("The perimeter is %v\n", s.perimeter())
}

func main() {
	fmt.Println("This is polymorphism section")

	var s Shape
	fmt.Printf("%T\n", s)
	// zero value of interface type is nil
	// returns <nil>

	window := Rectangle{length: 100, width: 20}
	s = window
	printShape(s)
	// prints the area and perimeter

	fmt.Printf("Type of s is %T\n", s)
	// returns main.Rectangle

	room := Rectangle{length: 250, width: 50}
	s = room

	fmt.Printf("The type of s is %T\n", s)
	// the type will be Rectangle which is the concrete type set at run time.
	// this could change if the shape was circle and the type would be a Circle.
	// this takes the form of polymorphism meaning taking many forms.
	// interfaces have dynamic types that change at run time.

}
