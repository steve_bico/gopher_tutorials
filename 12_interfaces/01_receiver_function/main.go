package main

import (
	"fmt"
	"time"
)

func main() {
	const day = 24 * time.Hour

	fmt.Printf("%T\n", day)
	// returns time.Duration

	seconds := day.Seconds()
	// this a receiver function
	// named types can have functions also called receiver functions called on them
	//

	fmt.Printf("%T\n", seconds)
	fmt.Printf("Seconds in a day %v\n", seconds)
	// Seconds in a day 86400

	// user defined named type

	friends := names{"Dan", "James", "June"}
	friends.print()

	// can also be called in this format
	//names.print(friends)
	// prints the names in friend

	friends.addNames("Joel", "Ken", "Jimmy", "Mike", "Burrows")

}

type names []string

func (n names) print() {
	// print() is the receiver function
	for i, name := range n {
		fmt.Println(i, name)
	}
}

func (n names) addNames(newNames ...string) {
	// adding names
	n = append(n, newNames...)
	fmt.Println(n)

}
