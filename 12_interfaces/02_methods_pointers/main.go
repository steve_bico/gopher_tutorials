package main

import "fmt"

type Car struct {
	brand string
	price int
}

func changeCar(c Car, newBrand string, newPrice int) {
	c.price = newPrice
	c.brand = newBrand
}

func (c Car) changeCarOne(newBrand string, newPrice int) {
	c.brand = newBrand
	c.price = newPrice
}

// using pointer receiver
func (c *Car) changeCarTwo(newBrand string, newPrice int) {
	c.brand = newBrand
	(*c).price = newPrice
}

func main() {
	myCar := Car{brand: "Audi", price: 50_000}
	changeCar(myCar, "BMW", 100_000)
	fmt.Println(myCar)
	// there was no change of name of car and price

	// Using Value Receiver
	myCar.changeCarOne("BMW", 90_000)
	fmt.Println(myCar)
	// no change

	// Using a pointer receiver
	//(&myCar).changeCarTwo("BMW", 90_000) - compiler will assist in this in if the & is not available
	myCar.changeCarTwo("BMW", 90_000)
	fmt.Println(myCar)
	// the values were changed

	var yourCar *Car
	yourCar = &myCar

	yourCar.changeCarTwo("Benz", 40_000)
	fmt.Println(*yourCar)

	// Valid Ways of Calling Methods On Pointer Type

	// 1. without dereferencing
	yourCar.changeCarTwo("VW", 40_000)
	fmt.Println(yourCar)

	// 2. with dereferencing
	(*yourCar).changeCarTwo("BMW", 50_000)
	fmt.Println(yourCar)

}
