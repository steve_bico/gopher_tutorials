package main

import "fmt"

type bot interface {
	getGreeting() string
}

type englishBot struct{}
type spanishBot struct{}

func (englishBot) getGreeting() string {
	// Very custom logic for generating english greeting
	return "Hi, there"
}

func (spanishBot) getGreeting() string {
	// Very custom logic for generating english greeting
	return "Hi, there"
}

func printGreeting(b bot) {
	fmt.Println(b.getGreeting())
}

// NB: Interfaces are used to define method set eg bot interface has getGreeting()string
