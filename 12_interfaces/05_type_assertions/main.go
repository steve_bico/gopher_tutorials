package main

import (
	"fmt"
	"log"
	"math"
)

type Rectangle struct {
	length, width, height float64
}

func (r Rectangle) area() float64 {
	return r.length * r.width
}

func (r Rectangle) perimeter() float64 {
	return (2 * r.length) + (2 * r.width)
}

func (r Rectangle) volume() float64 {
	return r.length * r.width * r.height
}

type Circle struct {
	radius float64
}

func (c Circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}

func (c Circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

type Shape interface {
	area() float64
	perimeter() float64
}

func printShape(s Shape) {
	fmt.Printf("Shape %#v\n", s)
	fmt.Printf("The area is %v\n", s.area())
	fmt.Printf("The perimeter is %v\n", s.perimeter())
}

func main() {
	// Type assertion provides access to interface concrete values

	var s Shape = Rectangle{length: 10, height: 20, width: 5}
	fmt.Printf("The type is %T\n", s)
	// type is a Rectangle
	// even though is is type Rectangle, we cannot access Rectangle methods in s.
	// s.volume() - returns an error
	// interface hides its methods they decouple one part of the code from another

	// use this to access methods on the Struct
	s.(Rectangle).volume()
	// this returns no error
	// this is type assertion
	// returns two values, boolean value and underlying.  Boolean reports whether the assertion exceeded.

	window, ok := s.(Rectangle)
	// ok - true if assertion is succefful or false otherwise
	// is success window, holds dynamic value is it fails it holds zero for the rectangle type

	if ok == true {
		fmt.Printf("Windows volume is %v  cubic centimetre \n", window.volume())
	} else {
		log.Fatal("Something went wrong")
	}

	// Type Switches
	// type switch look like regular switch but the types are the structs
	// they are compared against the valuee held

	s = Circle{radius: 5.0}

	switch value := s.(type) {
	case Rectangle:
		fmt.Printf("%#v has a Rectangle of type \n", value)
	case Circle:
		fmt.Printf("%#v has a Circle of type \n", value)
		// thsi will be printed
	}

}
