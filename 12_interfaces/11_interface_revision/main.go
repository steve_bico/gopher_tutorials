package _1_interface_revision

import (
	"fmt"
	"math"
)

type Square struct {
	length float64
}

type Circle struct {
	radius float64
}

// Interface, groups types together based on their methods

type Shape interface {
	area() float64
	perimeter() float64

	//NB: This means, in order to be considered as part of interface Shape,
	// 1. Shape must implement area()
	// 2. Shape must implement perimeter()

	//NB: Interfaces assist us in;
	// 1. Group structs methods together
	// 2.
}

// Using interfaces
func printShapeInfo(s Shape) {
	fmt.Printf("The shape area is %0.2f and the shape perimeter is %0.2f \n", s.area(), s.perimeter())
}

// Square methods
func (s Square) area() float64 {
	return s.length * s.length
}

func (s Square) perimeter() float64 {
	return 4 * s.length
}

// Circle methods
func (c Circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (c Circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

// NB: When to use a pointer receiver and when not to

func main() {
	shapes := []Shape{
		Square{length: 5.5},
		Circle{radius: 3.5},
		Square{length: 2.5},
		Circle{radius: 14.5},
	}

	for _, value := range shapes {
		printShapeInfo(value)
		fmt.Println("===============")
	}

}
