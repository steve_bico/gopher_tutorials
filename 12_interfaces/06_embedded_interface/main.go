package main

import (
	"math"
)

type Shape interface {
	getArea() float64
}

type Object interface {
	getVolume() float64
}

// EMBEDDED INTERFACE IMPLIMENTATION
// Geometry interface is embedding Shape and Object interfaces
// when we add an interface into another(embedd), we add all its method with it in the interface
type Geometry interface {
	Shape
	Object
	getColor() string
}

type Cube struct {
	edge  float64
	color string
}

// cube receiver function for area
func (c Cube) geArea() float64 {
	return 6 * (c.edge * c.edge)
}

// cube receiver function for volume
func (c Cube) getVolume() float64 {
	return math.Pow(c.edge, 3)
}

// cube receiver function for perimeter
func (c Cube) getPerimeter() float64 {
	return c.edge * 6
}

// cube receiver function getColor()
func (c Cube) getColor() string {
	return c.color
}

func measure(g Geometry) (float64, float64) {
	a := g.getArea()
	v := g.getVolume()

	return a, v
}

func main() {

	c := Cube{edge: 3}
	_ = c
	// a, v := measure(c)
	// return a, v

}
