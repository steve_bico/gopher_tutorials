package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
)

type Saver interface {
	// contract that guarantees that a value mostly struct have certain methods
	// whichever struct implements this interface has these methods
	// they do not implement the logic but a certain methods on type implementing it exists.
	// also has return values and types of params
	Save() error
}

type Todo struct {
	Content string `json:"content"`
}

func (t Todo) Display() {
	fmt.Printf("The note title contain the following %v as content \n", t.Content)
}

func (t Todo) Save() error {
	fileName := "todo.json"

	json, err := json.Marshal(t)

	if err != nil {
		return err
	}

	return os.WriteFile(fileName, json, 0644)
}

func New(content string) (Todo, error) {
	if content == "" {
		return Todo{}, errors.New("Invalid input")
	}

	return Todo{
		Content: content,
	}, nil
}

func getTodo() string {
	text := "This is my text"
	return text
}

func saveData(data Saver) error {
	err := data.Save()
	if err != nil {
		return err
	}

	return nil
}

// empty interface interface{}
// can be accepted as param and also as return value
func printSomething(value interface{}) {
	// interface{} accepts any type of value
	// string, slice etc
	// you can check the value supplied by using special .type() on the value
	// use the (.) on the received value then in parenthesis type value.(type)
	// use this with switch case
	switch value.(type) {
	case int:
		fmt.Println("Interger ", value)
	case string:
		fmt.Println("String ", value)
	default:
		fmt.Println("Not known")

	}

	// Alternative to extracting values from interface{}
	typedValue, ok := value.(int)
	// ok will be true if the value is type int
	// typedValue will be the actual value
	if ok {
		// at this point you know the value is an integer
		typedValue += 1
		return
	}

	valueString, ok := value.(string)
	if ok {
		valueString += " Your string value "
		return
	}

}

func main() {
	fmt.Println("This is interface section")
	todo, err := New("Learn Go")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = saveData(todo)
	if err != nil {
		fmt.Println(err)
	}
	//
}
