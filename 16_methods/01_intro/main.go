package main

import (
	"os/user"
	"time"

	"github.com/Rhymond/go-money"
)

func main() {
	// Method - this is a function with a receiver
	// Receiver - receiver of method is a special parameter
	// Receiver is listed before the method name
	// Method can only have one receiver.
	// Receiver has a type T or T*
	// When a receiver has a type T, we say it is a 'Value Receiver'
	// When a receiver has a type T* , we say it is a 'Pointer Receiver'
	// We say that the base type is T
	// func (c *Cart) TotalPrice()(*money.Money,error){}
	// func (c *Cart) - receiver
	// Base Type: Cart
	// Receiver Type: *Cart
	// TotalPrice() - method name
	//

}

type Item struct{ ID string }
type Cart struct {
	ID        string
	createdAt time.Time
	updatedAt time.Time
	lockedAt  time.Time
	user.User
	items       []Item
	currenyCode string
	isLocked    bool
}

func (c *Cart) TotalPrice() (*money.Money, error) {
	return nil, nil
}

func (c *Cart) Lock() error {
	return nil
}
