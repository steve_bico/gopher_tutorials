package main

import (
	"fmt"
	"reflect"
)

type Sayer interface {
	Say() string
}

type Cat struct{}
type Dog struct{}
type Horse struct{}

func (c Cat) Say() string {
	return "meaow"
}

func (d Dog) Say() string {
	return "woof"
}

func (h Horse) Say() string {
	return "neigh"
}

func MakeCatTalk(c Cat) {
	fmt.Println("Cat says: ", c.Say())
}

// Takes the type Sayer interface
// when called, you can pass any type that implements Sayer interface instead of Sayer
// You will still be able to access the methods of the passed type
// because it implements Sayer()
// eg MakeTalk(c) where c := Cat{}
func MakeTalk(s Sayer) string {
	//fmt.Println(reflect.TypeOf(s).Name(), "says:", s.Say())
	return reflect.TypeOf(s).Name() + " says " + s.Say()
}

func main() {
	c := Cat{}
	d := Dog{}

	animals := []Sayer{c, d}
	animals = append(animals, Horse{})
	// for _, a := range animals {
	// 	// fmt.Println(reflect.TypeOf(a).Name(), "says:", a.Say())
	// }

	//MakeCatTalk(c)

	// Applying interface
	//MakeTalk(animals)
	// MakeTalk(animals[0])
	// MakeTalk(animals[1])
	// MakeTalk(animals[2])
	//MakeTalk(c)
	fmt.Println(MakeTalk(c))

}
