package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/IBM/sarama"
)

func main() {
	topic := "comments"
	partition := int32(0)
	worker, err := connectConsumer([]string{"localhost:19092"})
	if err != nil {
		panic(err)
	}

	defer worker.Close()

	consumer, err := worker.ConsumePartition(topic, partition, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	defer consumer.Close()
	fmt.Println("consumer has started")

	// Listens to termination message from OS and notifies
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// msges to be consumed from the queue
	msgCount := 0

	// doneChan for notifying when done consuming from the channel
	doneChan := make(chan struct{})

	// goroutine for tasks
	go func() {
		for {
			select {
			case err := <-consumer.Errors():
				fmt.Println(err)

			case msg := <-consumer.Messages():
				msgCount++
				fmt.Printf("Received message count: %d | Topic (%s) | Message(%s) | Key (%s)\n", msgCount, string(msg.Topic), string(msg.Value), string(msg.Key))
				// if string(msg.Key) == "comments-key" {
				// 	msgCount++
				// 	fmt.Printf("Received message count: %d | Topic (%s) | Message(%s)\n", msgCount, string(msg.Topic), string(msg.Value))
				// }
			case <-sigchan:
				fmt.Println("Interruption detected")
				doneChan <- struct{}{}
			}
		}
	}()

	<-doneChan
	fmt.Println("Processed")
	if err := worker.Close(); err != nil {
		panic(err)
	}

}

func connectConsumer(brokerUrls []string) (sarama.Consumer, error) {
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	config.Consumer.MaxWaitTime = 500 * time.Millisecond

	connection, err := sarama.NewConsumer(brokerUrls, config)
	if err != nil {
		return nil, err
	}

	return connection, nil
}
